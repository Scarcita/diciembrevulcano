import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup'
import { Dots } from 'react-activity';
import "react-activity/dist/Dots.css";


export default function ModalNewUsers(props) {


    const [showModal, setShowModal] = useState(false);
    const [isLoading, setIsLoading] = useState(false)


    
    const [name, setName] = useState('')
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [passwordRepeat, setPasswordRepeat] = useState('')
    const [phone, setPhone] = useState('')
    const [role, setRole] = useState('')
    const [email, setEmail] = useState('')

    
    const clearForm = () => {
        setName('')
        setUsername('')
        setPassword('')
        setPasswordRepeat('')
        setEmail('')
        setRole('')
        setPhone('')
    }
    
  const getDataOrden = async (formData) => {
    
    setIsLoading(true)
        var data = new FormData();

         //console.log(formData.imgUrl);

    data.append("name", name);
    data.append("username", username);
    data.append("password", password);
    data.append("passwordrepeat", passwordRepeat);
    data.append("email", email);
    data.append("role", role);
    data.append("phone", phone);


    fetch("https://slogan.com.bo/vulcano/users/addMobile", {
      method: 'POST',
      body: data,
    })
      .then(response => response.json())
      .then(data => {
        console.log('VALOR ENDPOINTS: ', data);

        setIsLoading(false)
        if(data.status){
          clearForm()
          //setReloadClient(!reloadClient)
          setShowModal(false)
        } else {
          alert(JSON.stringify(data.errors, null, 4))
        }
        
      },
      (error) => {
        console.log(error)
      }
      )

  }

  useEffect(() => {
    console.warn('ahora name: '+ name);
  }, [ name])
 


  ////////////////// VALIDATION ///////////////////

  
  const validationSchema = Yup.object().shape({
    name: Yup.string()
    .required('name is required'),

    username: Yup.string()
      .required("username is required"),
    // //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

    password: Yup.string()
      .required('password is required'),
    //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),

    email: Yup.string()
      .required('email is required'),
    //   //.min(6, 'minimo 6 caracteres'),
    // //.matches(/[a-zA-Z0-9]/, 'solo numeros y letras'),

    phone: Yup.string()
    .required('phone is required'),

    role: Yup.string()
    .required('Role is required')
    .oneOf(['Administrator', 'Shop Foreman', 'Mechanic', 'Inventory', 'Invoicing', 'Service Advisor', ]),
// //   //.matches(/[A-Za-z ]/, 'Ingreso solo letras'),
   
    });
    const formOptions = { resolver: yupResolver(validationSchema) };

    // get functions to build form with useForm() hook
    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
        alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
        console.log('string is NOT empty')
        getDataOrden(data)
        return false;
        
    }


    return (
        <>
            <div>
                <button
                    className="w-[82px] h-[26px] md:w-[90px] md:h-[30px] lg:w-[100px] lg:h-[30px] text-[#FFFFFF] bg-[#3682F7] text-[12px] md:text-[13px] lg:text-[14px] rounded-[7px] text-center items-center hover:bg-[#fff] hover:border-[#3682F7] hover:border hover:text-[#3682F7] justify-center"
                    type="button"
                    onClick={() => setShowModal(true)}
                >
                    Create Client
                </button>
            </div>
            {showModal ? (
                <>
                    <div className="fixed inset-0 z-10 overflow-y-auto">
                        <div
                            className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                            onClick={() => setShowModal(false)}
                        ></div>
                        <div className="flex items-center min-h-screen px-4 py-8">
                            <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">

                                <div>
                                    <h4 className="text-lg font-medium text-gray-800">
                                        New Client
                                    </h4>

                                </div>
                                <div>
                                    <form onSubmit={handleSubmit(onSubmit)}>

                                        <div className="mt-[20px] grid grid-cols-12 gap-4">

                                            <div className='col-span-12 md:col-span-12 lg:col-span-6 '>
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>name</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="text"
                                                    name="name"
                                                    {...register('name')}
                                                    value={name}
                                                    onChange={(e) => {
                                                        setName(e.target.value)
                                                    }}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.name?.message}</div>
                                            </div>

                                            <div className='col-span-12 md:col-span-12 lg:col-span-6'>
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Username</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="text"
                                                    name="username"
                                                    {...register('username')}
                                                    value={username}
                                                    onChange={(e) => setUsername(e.target.value)}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.username?.message}</div>
                                            </div>
                                            <div className='col-span-12 md:col-span-6 lg:col-span-6'>
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Password</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px]"
                                                    type="text"
                                                    name="password"
                                                    {...register('password')}
                                                    value={password}
                                                    onChange={(e) => setPassword(e.target.value)}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.password?.message}</div>
                                            </div>
                                            <div className='col-span-12 md:col-span-6 lg:col-span-6'>
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Password Repeat</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px]"
                                                    type="text"
                                                    name="passwordRepeat"
                                                    {...register('password')}
                                                    value={passwordRepeat}
                                                    onChange={(e) => setPasswordRepeat(e.target.value)}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.passwordRepeat?.message}</div>
                                            </div>
                                            <div className="col-span-12 md:col-span-6 lg:col-span-6">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Email</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="email"
                                                    name="email"
                                                    {...register('email')}
                                                    value={email}
                                                    onChange={(e) => setEmail(e.target.value)}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.email?.message}</div>
                                            </div>
                                            <div className="col-span-12 md:col-span-6 lg:col-span-6">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>Phone</p>
                                                <input className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                    type="text"
                                                    name="phone"
                                                    {...register('phone')}
                                                    value={phone}
                                                    onChange={(e) => setPhone(e.target.value)}
                                                />
                                                <div className="text-[14px] text-[#FF0000]">{errors.phone?.message}</div>
                                            </div>
                     
                                            <div className="col-span-12 md:col-span-6 lg:col-span-6 ">
                                                <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>Role</p>
                                                <select
                                                    name="role"
                                                    {...register('role')}
                                                    value={role}
                                                    onChange={(e) => {
                                                        setRole(e.target.value)
                                                    }}
                                                    className="w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                                                >
                                                    <option value="Administrator">Administrator</option>
                                                    <option value="Shop Foreman">Shop Foreman</option>
                                                    <option value="Mechanic">Mechanic</option>
                                                    <option value="Inventory">Inventory</option>
                                                    <option value="Invoicing">Invoicing</option>
                                                    <option value="Service Advisor">Service Advisor</option>
                                                </select>
                                                <div className="text-[14px] text-[#FF0000]">{errors.role?.message}</div>
                                            </div>
                                        </div>
                                        <div className="flex flex-row justify-between mt-[20px]">

                                            <div>
                                                <h1 className="text-[12px] mt-[10px]">* This field is mandatory</h1>
                                            </div>

                                            <div>
                                                <button
                                                    className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                                    onClick={() =>
                                                        setShowModal(false)
                                                    }
                                                    disabled={isLoading}
                                                >
                                                    Cancel
                                                </button>
                                                <button
                                                    className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                                                   type="submit"
                                                   disabled={isLoading}

                                                >
                                                    {isLoading ? <Dots className='m-auto' size={7} color={'#582BE7'}></Dots> : 'Create'}
                                                    
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </>
            ) : null}
        </>
    );
}
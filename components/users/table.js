import React from 'react';
import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'
import Image from 'next/image'
//import logo from '../../../public/logo.svg';
//import ImagenFaceboock from "../../../public/SocialMedia/Facebook.svg";
//import ImagenInstagram from "../../../public/SocialMedia/Instagram.svg";
//import ImagenYoutube from "../../../public/SocialMedia/Youtube.svg"
//import ImagenTikTok from "../../../public/SocialMedia/TikTok.svg"
//import ImgTwitter from "../../../public/SocialMedia/Twitter.svg"
//import ImgLinkedin from "../../../public/SocialMedia/messenger.svg"



import { useEffect, useState } from 'react'
//import Link from 'next/link';
//import TooltipClient from './tooltip';

const TableDatosUsers = (props) => {

    const {row, reloadClients,  setReloadClients} = props
    const [isLoading, setIsLoading] = useState(true)

    const [data, setData] = useState([]);
    const headlist = [
        'Name', "Username", "Email", 'Phone', "Role",  ""
    ];

    useEffect(() => {

        setIsLoading(true)
        
        fetch('https://slogan.com.bo/roadie/clients/all')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    //console.log(data.data);
                    setData(data.data)

                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

            console.warn('reloaddd' + reloadClients);

    }, [reloadClients])

    return (
        isLoading ?
            <>
                <div className='flex justify-center items-center' style={{ width: '100%', height: 50 }}>
                    <Spinner color="#3682F7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                </div>

            </>
            :
            <>
                <div>
                    <TablaProductos data={data} headlist={headlist} reloadClients={reloadClients} setReloadClients={setReloadClients}/>
                    <TablaResponsive headlist={headlist} data={data} reloadClients={reloadClients} setReloadClients={setReloadClients} />
                </div>
            </>
    )
}


const TablaProductos = (props) => {
    const { headlist, data, reloadClients,  setReloadClients } = props;

    const splitData = (obj) => {
        data.forEach(element => {
            let date = element.created.split("T");
            element.created = date[0];
        });
        data = obj;
    };
    splitData(data);

    return (

        <div className="rounded-lg shadow hidden lg:block md:block">
            <table className="w-full">
                <thead className='bg-[#FFFF] shadow'>
                    <tr>
                        {headlist.map((header, index) => <th key={index} className='text-[12px] font-semibold text-[#000000] text-center pt-[10px] pb-[10px]'>{header}</th>)}
                    </tr>
                </thead>

                <tbody>

                    {data.map(row =>

                        <tr key={row.id} >

                            {/* <td className=''>
                                <div className='flex flex-row pl-[15px]'>
                                    <div className='mr-[10px]'>
                                        {row.img_url !== null ?
                                            <Image 
                                            className='rounded-full bg-[#F3F3F3]'
                                            //src={row.img_url}
                                            alt="facebook"
                                            height={30}
                                            width={30}>
                    
                                            </Image> 
                                            : <></> 
                                        }
                            
                                    </div>
                                
                            
                                    <div className='text-[12px] self-center'>

                                        {row.name}

                                    </div>
                                </div>
                            

                            </td>
                            <td className='text-center'>

                                {row.plans !== null && row.plans.length  > 0  ?

                                <div>
                                    <p className='text-[#643DCE] font-semibold text-[20px] text-center'>YES</p>
                                    <p className='text-[#000000] text-[12px] text-center'>{row.plans[0].name}</p>
                                </div>
                                : <></>
                                }
                            </td> */}


                            {/* <td >
                                <div className='text-center text-[12px]'>
                                    <p>
                                        {row.client_since}
                                    </p>
                                </div>
                            </td> */}

                            <td className='text-center'>
                                {/* <TooltipClient
                                    row={row}
                                    id={row.id}
                                    name={row.name}
                                    country={row.country}
                                    city={row.city}
                                    email={row.email}
                                    website={row.website}
                                    phone={row.phone}
                                    client_since={row.client_since}
                                    facebook_urle={row.facebook_url}
                                    instagram_url={row.instagram_url}
                                    youtube_url={row.youtube_url}
                                    tiktok_url={row.tiktok_url}
                                    twitter_url={row.twitter_url}
                                    linkedin_url={row.linkedin_url}
                                    reloadClients={reloadClients}
                                    setReloadClients={setReloadClients}
                                /> */}
                            </td>

                        </tr>
                    )}


                </tbody>
            </table>
        </div>
    );
};


const TablaResponsive = (props) => {
    const { data, reloadClients,  setReloadClients  } = props;

    return (
        <div className='grid grid-cols-12'>
            <div className='col-span-12 md:col-span-12 lg:col-span-12 rounded-[10px] md:hidden'>
                
                {data.map(row =>
                    {       
                        if(row.client_since !== null && row.client_since !== undefined){
                            var eventdate = row.client_since;
                            var splitdate = eventdate.split('-');
                            //console.log(splitdate);
                            var day = splitdate[2];
                            var year = splitdate[0];
                        }


                        if(row.client_since !== null && row.client_since !== undefined){
                            var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                            var d = new Date(row.client_since);
                            var dayName = days[d.getDay()];
                        }
                
                
                    
                        if(row.client_since !== null && row.client_since !== undefined){
                            var months = ['Jan.', 'Feb. ', 'Mar.', 'Apr.', 'May', 'Jun.', 'Jul.','Aug.', 'Sept.', 'Oct.', 'Nov.', 'Dec.'];
                            var m = new Date(row.client_since);
                            var monthName = months[m.getMonth()];
                        }     
                    return (
                    <div key={row.id} className='grid grid-cols-12 bg-[#fff] rounded-[15px] shadow-md pl-[10px] pb-[10px] mb-[15px]'>
                        <div className='col-span-12 md:col-span-12'>
                            <div className='grid grid-cols-12'>
                                <div className='col-span-10 md:col-span-12 pt-[10px]'>
                                    <div className='grid grid-cols-12 gap-3'>
                                        <div className='col-span-7 md:col-span-7'>
                                            <div className='flex flex-row'>
                                                <div className='mr-[10px]'>
                                                    {row.img_url !== null ?
                                                        <Image 
                                                        className='rounded-full bg-[#F3F3F3]'
                                                        //src={row.img_url}
                                                        alt="facebook"
                                                        height={30}
                                                        width={30}>
                                
                                                        </Image> 
                                                        : <></> 
                                                    }
                                        
                                                </div>
                                            
                                        
                                                <div className='text-[12px] self-center leading-4 whitespace-normal'>

                                                    {row.name}

                                                </div>
                                            </div>

                                        </div>
                                        <div className='col-span-5 md:col-span-5'>

                                            {row.plans !== null && row.plans.length  > 0  ?

                                            <div>
                                                <p className='text-[#643DCE] font-semibold text-[20px] text-center'>YES</p>
                                                <p className='text-[#000000] text-[12px] text-center'>{row.plans[0].name}</p>
                                            </div>
                                            : <></>
                                            }

                                        </div>

                                    </div>

                                    <div className='grid grid-cols-12 gap-3 mt-[10px]'>
                                        <div className='col-span-6 md:col-span-6 flex flex-row justify-between'>
                                            <div className='text-left self-center items-center'>
                                                <p className='text-[26px] font-bold text-center'>{day}</p>
                                                
                                            </div>
                                            <div className='text-end self-center items-center'>
                                                <p className='text-[14px] text-[#582BE7]  leading-4 whitespace-normal'>{dayName}</p>
                                                <p className='text-[16px] font-semibold leading-4 whitespace-normal'>{monthName} {year}</p>
                                                
                                            </div>

                                        </div>
                                        
                                         
                                    </div>
                                </div>
                                <div className='col-span-2 md:col-span-12'>
                                    <div className=' items-center text-center '>
                                    {/* <TooltipClient
                                            row={row}
                                            id={row.id}
                                            name={row.name}
                                            country={row.country}
                                            city={row.city}
                                            email={row.email}
                                            website={row.website}
                                            phone={row.phone}
                                            client_since={row.client_since}
                                            facebook_urle={row.facebook_url}
                                            instagram_url={row.instagram_url}
                                            youtube_url={row.youtube_url}
                                            tiktok_url={row.tiktok_url}
                                            twitter_url={row.twitter_url}
                                            linkedin_url={row.linkedin_url}
                                            reloadClients={reloadClients}
                                            setReloadClients={setReloadClients}
                                        /> */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    )
                    }
                    )}
            </div>
        </div>

        
    );
};


export default TableDatosUsers;


import React from 'react'
import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'
import Image from 'next/image'
import { useEffect, useState } from 'react'
import TooltipCliente from './tooltip'

const TableCitasAll = () => {
    const [isLoading, setIsLoading] = useState(true)
    const [total, setTotal] = useState(0);
    const [data, setData] = useState([]);

    const headlist = [
        "", "", "", ""
    ];

    useEffect(() => {

        setIsLoading(true)
        fetch('https://slogan.com.bo/vulcano/appointments/today')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log(data.data);
                    setData(data.data)

                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })


    }, [])

    return (
        isLoading ?
            <div className='flex justify-center items-center self-center mt-[30px]'  >
                <Spinner color="#3682F7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
            <div >
                <div>
                    <Table data={data} headlist={headlist} />
                    <TablaResponsive headlist={headlist} data={data} />
                </div>
            </div>
    )
}


const Table = (props) => {
    const { data, headlist } = props;

    return (
        <div className=" hidden lg:block md:block">
            <table className='w-full bg-[#fff] rounded-r-lg shadow-[25px]'>
                <thead>
                    <tr>
                        {headlist.map(header =>
                            <th
                                key={headlist.id}
                                className='text-[14px] font-semibold text-[#000000]'>
                                {header}
                            </th>
                        )}
                    </tr>
                </thead>
                <tbody>
                    {data.map(row =>
                        <tr>
                            <td className='flex flex-col'>
                                <div className='text-[24px] font-700 text-center self-center'>
                                    {row.time_to}
                                </div>
                                <p className='text-[24px] text-center mr-[-45px] text-[#D3D3D3]'>
                                    AM
                                </p>
                            </td>
                            <td>
                                <div className="flex justify-center items-center w-[82px] h-[82px] bg-[#F6F6FA] rounded-full ">
                                    <Image
                                        src={row.car.cars_models_version.cars_model.catalogues_record.additional_info}
                                        alt="media"
                                        layout="fixed"
                                        width={54}
                                        height={54}
                                    />
                                </div>
                            </td>
                            <td>
                                <div className='self-center items-center'>
                                    <p className="text-[26px] text-[#000000] font-bold">
                                        {row.client.name}
                                    </p>
                                    <div className='flex flex-row'>
                                        <div className="flex justify-center items-center text-center font-light w-[99px] h-[22px] bg-[#3682F7] text-[13px] text-[#FFFFFF] rounded-[20px]">
                                            {row.status}
                                        </div>
                                        <div className='flex justify-center items-center text-center gap-1'>
                                            <div className="text-[17px] text-[#000000] ml-[5px]">
                                                {row.car.cars_models_version.name}
                                            </div>
                                            <p className="text-[17px] text-[#000000]">
                                                •
                                            </p>
                                            <div className="text-[17px] text-[#000000] ">
                                                {row.car.plate}
                                            </div>
                                            <p className="text-[17px] text-[#000000]">
                                                •
                                            </p>
                                            <div className="text-[17px] text-[#000000]">
                                                {row.car.vin}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td className='self-center items-center text-center'>
                                <TooltipCliente />
                            </td>
                        </tr>
                    )}
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>

    );
};



const TablaResponsive = (props) => {
    const { data, } = props;
    return (
        <div className='grid grid-cols-12 mt-[20px] '>
            <div className='col-span-12 md:col-span-12 lg:col-span-12 rounded-[10px] md:hidden'>
                {data.map(row => {
                    return (
                        <div key={row.id} className='grid grid-cols-12 bg-[#fff] rounded-[15px] shadow-md pl-[10px] pb-[10px] mb-[15px]'>
                            <div className='col-span-12 md:col-span-12'>
                                <div className='grid grid-cols-12'>
                                    <div className='col-span-10 md:col-span-12 pt-[10px]'>
                                        <div className='grid grid-cols-12 gap-3'>
                                            <div className='col-span-12 md:col-span-12 flex flex-row justify-between'>
                                                <div className='text-[24px] font-semibold self-center'>
                                                    {row.time_to}
                                                </div>
                                                <div className="flex justify-center items-center text-center w-[70px] h-[19px] bg-[#3682F7] text-[12px] text-[#FFFFFF] rounded-[20px]">
                                                    {row.status}
                                                </div>
                                            </div>
                                            <div className='col-span-12 md:col-span-12 flex flex-row'>
                                                <div className="flex justify-center items-center  w-[82px] h-[82px] bg-[#F6F6FA] rounded-full self-center items-center">
                                                    <Image
                                                        src={row.car.cars_models_version.cars_model.catalogues_record.additional_info}
                                                        alt="media"
                                                        layout="fixed"
                                                        width={54}
                                                        height={54}
                                                    />
                                                </div>
                                                <div className='self-center items-center ml-[20px]'>
                                                    <p className="text-[18px] text-[#000000] font-bold">
                                                        {row.client.name}
                                                    </p>
                                                    <div className='flex justify-center items-center text-center gap-1'>
                                                        <div className="text-[12px] text-[#000000]">
                                                            {row.car.cars_models_version.name}
                                                        </div>
                                                        <p className="text-[17px] text-[#000000]">
                                                            •
                                                        </p>
                                                        <div className="text-[12px] text-[#000000] ">
                                                            {row.car.plate}
                                                        </div>
                                                        <p className="text-[17px] text-[#000000]">
                                                            •
                                                        </p>
                                                        <div className="text-[12px] text-[#000000]">
                                                            {row.car.vin}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className='col-span-2 md:col-span-12'>
                                        <div className=' items-center text-center '>
                                            <TooltipCliente />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                }
                )}
            </div>
        </div>
    );
};

export default TableCitasAll;
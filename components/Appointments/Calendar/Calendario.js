import React, { useEffect, useState } from "react";

import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";

import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import Link from 'next/link'

import CounterReservas from "./counterReservas";
import CounterConfirmadas from "./counterConfirmadas";
import CounterCancel from "./counterCancel";
import TableCitasNext from "./tableCitasNext";
import TableCitasAll from "./tableCitasAll";
import TableCitasConfir from "./tableConfirmados";
import TableCitasCancel from "./tableCitasCancel";

export default function Calendario(props) {
    const [showDots, setShowDots] = useState(true);
    const { date } = props;

    return (
        <div>
            <Desktop date={date} showDots={showDots} setShowDots={setShowDots} />
            <Responsive date={date} showDots={showDots} setShowDots={setShowDots} />
        </div>
    )
}
const Desktop = (props) => {
    const { date, showDots, setShowDots } = props;
    const dateObject = formartDateFromUrl();
    const [anterior, setAnterior] = useState([]);
    const [centro, setCentro] = useState([]);
    const [posterior, setPosterior] = useState([]);

    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([]);

    function getCentro() {
        var datosText = [];
        let newDate = formartDateFromUrl();
        newDate.setDate(newDate.getDate());
        let datenumero = newDate.getDate()
        let diaText = newDate.getDay()
        let dia = ["LUN", "MAR", "MIE", "JUE", "VIE", "SAB", "DOM"]
        let nombreDia = dia[diaText];
        let mesText = newDate.getMonth()
        let mes = ["ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"]
        let nombreMes = mes[mesText];
        datosText.push({
            dateNumber: datenumero,
            weekDay: nombreDia,
            monthName: nombreMes,
            fullDate: newDate.getFullYear() + "-" + (newDate.getMonth() + 1) + "-" + newDate.getDate()
        });
        setCentro(datosText);
    }

    function formartDateFromUrl() {
        var parts = date.split('-');
        // Please pay attention to the month (parts[1]); JavaScript counts months from 0:
        // January - 0, February - 1, etc.
        var mydate = new Date(parts[0], parts[1] - 1, parts[2]);
        return mydate;
    }

    function getAnterior(fecha) {
        var pastDates = [];
        for (let i = 4; i >= 1; i--) {
            let newDate = formartDateFromUrl();
            newDate.setDate(newDate.getDate() - i);
            let datenumero = newDate.getDate()
            let diaText = newDate.getDay()
            let dia = ["LUN", "MAR", "MIE", "JUE", "VIE", "SAB", "DOM"]
            let nombreDia = dia[diaText];
            let mesText = newDate.getMonth()
            let mes = ["ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"]
            let nombreMes = mes[mesText];
            pastDates.push({
                dateNumber: datenumero,
                weekDay: nombreDia,
                monthName: nombreMes,
                fullDate: newDate.getFullYear() + "-" + (newDate.getMonth() + 1) + "-" + newDate.getDate()
            });
        }
        setAnterior(pastDates);
    };

    function getPosterior() {
        var upcomingDates = [];
        for (let i = 1; i <= 4; i++) {
            let newDate = formartDateFromUrl();
            newDate.setDate(newDate.getDate() + i);
            let datenumero = newDate.getDate()
            let diaText = newDate.getDay()
            let dia = ["LUN", "MAR", "MIE", "JUE", "VIE", "SAB", "DOM"]
            let nombreDia = dia[diaText];
            let mesText = newDate.getMonth()
            let mes = ["ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"]
            let nombreMes = mes[mesText];
            upcomingDates.push({
                dateNumber: datenumero,
                weekDay: nombreDia,
                monthName: nombreMes,
                fullDate: newDate.getFullYear() + "-" + (newDate.getMonth() + 1) + "-" + newDate.getDate()
            });
        }
        setPosterior(upcomingDates);
    };

    useEffect(() => {
        setShowDots(true);
        getAnterior();
        getCentro();
        getPosterior();

        // setIsLoading(true)
        // fetch('https://slogan.com.bo/vulcano/appointments/getFromDate/' + date + "/upcoming")
        //     .then(response => response.json())
        //     .then(data => {
        //         if (data.status) {
        //             console.log("datadata");
        //             console.log(data.data);
        //             console.log("datadata");
        //             setData(data.data)

        //         } else {
        //             console.error(data.error)
        //         }
        //         setIsLoading(false)
        //     })

        // setShowDots(false);
    }, [date]);

    return (
        <div className=" hidden lg:block md:block" >
            <div className='flex justify-center items-center'>
                <>
                    {
                        showDots ?
                            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                                <Spinner color="#3682F7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                            </div>
                            :
                            <div className="flex flex-row gap-2 justify-center items-center grid grid-cols-12">
                                <div className=" col-span-5 md:col-span-5lg:col-span-5 flex flex-row justify-center items-center gap-9">
                                    {anterior.map(item => (
                                        <Link
                                            href={"http://localhost:3000/calendar/" + item.fullDate}>
                                            <div className="flex flex-col justify-center items-center cursor-pointer border-[#3682F7] border-[1px] rounded-[14px] w-[72px] h-[72px] gap-2">
                                                <p className="text-[12px] mt-[02px] font-semibold ">
                                                    {item.weekDay}
                                                </p>
                                                <p className="text-[28px] mt-[-15px] font-bold"
                                                    id={item.id}
                                                >
                                                    {item.dateNumber}
                                                </p>
                                                <p className="text-[700] text-[16px] mt-[-14px] font-bold ">
                                                    {item.monthName}
                                                </p>
                                            </div>
                                        </Link>
                                    ))}
                                </div>
                                <div className="flex justify-center items-center col-span-2 md:col-span-2 lg:col-span-2 ">
                                    {centro.map(item => (
                                        <div className="flex flex-col justify-center items-center bg-[#FFFFFF] w-[88px] h-[92px] rounded-[14px] gap-1">
                                            <p className="text-[#FF0000] text-[14px] mt-[02px] font-semibold ">
                                                {item.weekDay}
                                            </p>
                                            <p className="text-[37px] mt-[-15px] font-bold ">
                                                {dateObject.getDate()}
                                            </p>
                                            <p className="text-[700] text-[18px] mt-[-14px] font-bold ">
                                                {item.monthName}
                                            </p>
                                        </div>
                                    ))}
                                </div>
                                <div className="flex flex-row gap-9 justify-center items-center col-span-5 md:col-span-5 lg:col-span-5">
                                    {posterior.map(item => (
                                        <Link
                                            href={"http://localhost:3000/calendar/" + item.fullDate}>
                                            <div className="flex flex-col justify-center items-center cursor-pointer border-[#3682F7] border-[1px] rounded-[14px] w-[72px] h-[72px] gap-2">
                                                <p className="text-[12px] mt-[02px] font-semibold ">
                                                    {item.weekDay}
                                                </p>
                                                <p className="text-[28px] mt-[-15px] font-bold"
                                                    id={item.id}
                                                >
                                                    {item.dateNumber}
                                                </p>
                                                <p className="text-[700] text-[16px] mt-[-14px] font-bold ">
                                                    {item.monthName}
                                                </p>
                                            </div>
                                        </Link>
                                    ))}
                                </div>
                            </div>
                    }
                </>
            </div>
            <div className="grid grid-cols-12 gap-6 mt-[25px] md:mt-[35px] lg:mt-[50px]">
                <div className="col-span-12 md:col-span-12 lg:col-span-2">
                    <div className="grid grid-cols-12 gap-3">
                        <div className="flex flex-col justify-center items-center col-span-4 md:col-span-4 lg:col-span-12 bg-[#fff] shadow-md rounded-[17px] pl-[10px] pt-[10px] pr-[10px] pb-[10px]">
                            <div className="bg-[#3682F7] w-[50px] h-[25px] rounded mt-2 flex justify-center shadow-md">
                                <div className="bg-[#FFFF] w-[18px] h-[12px] rounded flex justify-center mt-2 ">
                                    <p className="text-[#3682F7] text-[12px] font-semibold mt-[-4px]">
                                        ↑
                                    </p>
                                </div>
                                <p className="text-[14px] text-[#FFFF] mt-[3px] ml-[4px]">
                                    15%
                                </p>
                            </div>
                            <div className="flex flex-col justify-center items-center">
                                <p className="text-[#000000] text-[34px] md:text-[38px]  lg:text-[40px] font-medium ">
                                    <CounterReservas />
                                </p>
                                <p className="text-[#000000] text-[15px] font-light mt-[-12px]">
                                    Reservas
                                </p>
                            </div>
                        </div>

                        <div className="flex flex-col justify-center items-center col-span-4 md:col-span-4 lg:col-span-12 bg-[#fff] shadow-md rounded-[17px] pl-[10px] pt-[10px] pr-[10px] pb-[10px]">
                            <div className="bg-[#3682F7] w-[50px] h-[25px] rounded mt-2 flex justify-center shadow-md">
                                {" "}
                                <div className="bg-[#FFFF] w-[18px] h-[12px] rounded flex justify-center mt-2 ">
                                    <p className="text-[#3682F7] text-[12px] font-semibold mt-[-4px]">
                                        ↑
                                    </p>
                                </div>
                                <p className="text-[14px] text-[#FFFF] mt-[3px] ml-[4px]">
                                    15%
                                </p>
                            </div>
                            <div className="flex flex-col justify-center items-center">
                                <p className="text-[#000000] text-[34px] md:text-[38px]  lg:text-[40px] font-medium ">
                                    <CounterConfirmadas />
                                </p>
                                <p className="text-[#000000] text-[15px] font-light mt-[-12px]">
                                    Confirmadas
                                </p>
                            </div>
                        </div>

                        <div className="flex flex-col justify-center items-center col-span-4 md:col-span-4 lg:col-span-12 bg-[#fff] shadow-md rounded-[17px] pl-[10px] pt-[10px] pr-[10px] pb-[10px]">
                            <div className="bg-[#EE002B] w-[50px] h-[25px] rounded mt-2 flex justify-center shadow-md">
                                <div className="bg-[#FFFF] w-[18px] h-[12px] rounded flex justify-center mt-2 ">
                                    <p className="text-[#EE002B] text-[12px] font-semibold mt-[-4px]">
                                        ↑
                                    </p>
                                </div>
                                <p className="text-[14px] text-[#FFFF] mt-[3px] ml-[4px]">
                                    15%
                                </p>
                            </div>
                            <div className="flex flex-col justify-center items-center">
                                <p className="text-[#000000] text-[34px] md:text-[38px]  lg:text-[40px]  font-medium ">
                                    <CounterCancel />
                                </p>
                                <p className="text-[#000000] text-[15px] font-light mt-[-12px]">
                                    Canceladas
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-span-12 md:col-span-12 lg:col-span-10 ">
                    <Tabs className="">
                        <TabList className="flex flex-row w-full gap-4 md:w-[280px] lg:w-[300px] bg-[#E4E7EB] focus:outline-none rounded-t-lg h-8">
                            <Tab className="cursor-pointer flex justify-center items-center text-[12px] focus:text-[#fff] focus:bg-[#3682F7] focus:text-[#fff] px-2">
                                Siguientes
                            </Tab>
                            <Tab className="cursor-pointer text-gray-500 text-[12px] flex justify-center items-center focus:bg-[#3682F7] focus:text-[#fff]  px-2 py-2">
                                Todos
                            </Tab>
                            <Tab className="cursor-pointer text-gray-500 text-[12px] flex justify-center items-center focus:bg-[#3682F7] focus:text-[#fff]  px-2 py-2">
                                Confirmados
                            </Tab>
                            <Tab className="cursor-pointer text-gray-500 text-[12px] flex justify-center items-center focus:bg-[#3682F7] focus:text-[#fff]  px-2 py-2">
                                Cancelados
                            </Tab>
                        </TabList>

                        <TabPanel>
                            <div className="grid grid-cols-12">
                                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                                    {/* <TableCitasNext date={date} /> */}
                                </div>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="grid grid-cols-12">
                                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                                    <TableCitasAll date={date} />
                                </div>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="grid grid-cols-12">
                                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                                    <TableCitasConfir date={date} />
                                </div>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="grid grid-cols-12">
                                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                                    <TableCitasCancel date={date} />
                                </div>
                            </div>
                        </TabPanel>
                    </Tabs>
                </div>
            </div>
        </div>
    )
}


const Responsive = (props) => {
    const { date, showDots, setShowDots } = props;
    const dateObject = formartDateFromUrl();
    const [anterior, setAnterior] = useState([]);
    const [centro, setCentro] = useState([]);
    const [posterior, setPosterior] = useState([]);

    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([]);

    function getCentro() {
        var datosText = [];
        let newDate = formartDateFromUrl();
        newDate.setDate(newDate.getDate());
        let datenumero = newDate.getDate()
        let diaText = newDate.getDay()
        let dia = ["LUN", "MAR", "MIE", "JUE", "VIE", "SAB", "DOM"]
        let nombreDia = dia[diaText];

        let mesText = newDate.getMonth()
        let mes = ["ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"]
        let nombreMes = mes[mesText];
        datosText.push({
            dateNumber: datenumero,
            weekDay: nombreDia,
            monthName: nombreMes,
            fullDate: newDate.getFullYear() + "-" + (newDate.getMonth() + 1) + "-" + newDate.getDate()
        });
        setCentro(datosText);
    }

    function formartDateFromUrl() {
        var parts = date.split('-');
        // Please pay attention to the month (parts[1]); JavaScript counts months from 0:
        // January - 0, February - 1, etc.
        var mydate = new Date(parts[0], parts[1] - 1, parts[2]);
        return mydate;
    }

    function getAnterior(fecha) {
        var pastDates = [];
        for (let i = 2; i >= 1; i--) {
            let newDate = formartDateFromUrl();
            newDate.setDate(newDate.getDate() - i);
            let datenumero = newDate.getDate()
            let diaText = newDate.getDay()
            let dia = ["LUN", "MAR", "MIE", "JUE", "VIE", "SAB", "DOM"]
            let nombreDia = dia[diaText];
            let mesText = newDate.getMonth()
            let mes = ["ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"]
            let nombreMes = mes[mesText];
            pastDates.push({
                dateNumber: datenumero,
                weekDay: nombreDia,
                monthName: nombreMes,
                fullDate: newDate.getFullYear() + "-" + (newDate.getMonth() + 1) + "-" + newDate.getDate()
            });
            console.error(diaText);
            console.error(pastDates);
            console.log(newDate.getFullYear() + "-" + (newDate.getMonth() + 1) + "-" + newDate.getDate());
        }
        setAnterior(pastDates);
    };

    function getPosterior() {
        var upcomingDates = [];

        for (let i = 1; i <= 2; i++) {
            let newDate = formartDateFromUrl();
            newDate.setDate(newDate.getDate() + i);
            let datenumero = newDate.getDate()
            let diaText = newDate.getDay()
            let dia = ["LUN", "MAR", "MIE", "JUE", "VIE", "SAB", "DOM"]
            let nombreDia = dia[diaText];
            let mesText = newDate.getMonth()
            let mes = ["ENE", "FEB", "MAR", "ABR", "MAY", "JUN", "JUL", "AGO", "SEP", "OCT", "NOV", "DIC"]
            let nombreMes = mes[mesText];
            upcomingDates.push({
                dateNumber: datenumero,
                weekDay: nombreDia,
                monthName: nombreMes,
                fullDate: newDate.getFullYear() + "-" + (newDate.getMonth() + 1) + "-" + newDate.getDate()
            });
            console.log(newDate.getFullYear() + "-" + (newDate.getMonth() + 1) + "-" + newDate.getDate());
        }
        setPosterior(upcomingDates);
    };

    useEffect(() => {
        setShowDots(true);
        getAnterior();
        getCentro();
        getPosterior();

        setIsLoading(true)
        fetch('https://slogan.com.bo/vulcano/appointments/today')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log(data.data);
                    setData(data.data)

                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

        setShowDots(false);
    }, [date]);

    return (
        <div className="md:hidden">
            <div className='flex justify-center items-center'>
                <>
                    {showDots ?
                        <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                            <Spinner color="#3682F7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
                        </div>
                        :
                        <div className="flex flex-row gap-4 justify-center items-center grid grid-cols-12">
                            <div className="flex flex-row justify-center items-center gap-4 col-span-5 md:col-span-5 lg:col-span-5">
                                {anterior.map(item => (
                                    <Link
                                        href={"http://localhost:3000/calendar/" + item.fullDate}>
                                        <div className="flex flex-col justify-center items-center cursor-pointer border-[#3682F7] border-[1px] rounded-[14px] w-[62px] h-[62px] gap-1">
                                            <p className="text-[12px] mt-[02px] font-semibold ">
                                                {item.weekDay}
                                            </p>
                                            <p className="text-[26px] mt-[-15px] font-bold"
                                                id={item.id}
                                            >
                                                {item.dateNumber}
                                            </p>
                                            <p className="text-[700] text-[14px] mt-[-14px] font-bold ">
                                                {item.monthName}
                                            </p>
                                        </div>
                                    </Link>
                                ))}
                            </div>
                            <div className="flex justify-center items-center col-span-2 md:col-span-2 lg:col-span-2">
                                {centro.map(item => (
                                    <div className="flex flex-col justify-center items-center bg-[#FFFFFF] w-[68px] h-[72px] rounded-[14px] gap-1">
                                        <p className="text-[#FF0000] text-[14px] mt-[02px] font-semibold ">
                                            {item.weekDay}
                                        </p>
                                        <p className="text-[34px] mt-[-15px] font-bold ">
                                            {dateObject.getDate()}
                                        </p>
                                        <p className="text-[700] text-[16px] mt-[-15px] font-bold ">
                                            {item.monthName}
                                        </p>
                                    </div>
                                ))}
                            </div>
                            <div className="flex flex-row gap-4 justify-center items-center col-span-5 md:col-span-5 lg:col-span-5">
                                {posterior.map(item => (
                                    <Link
                                        href={"http://localhost:3000/calendar/" + item.fullDate}>
                                        <div className="flex flex-col justify-center items-center cursor-pointer border-[#3682F7] border-[1px] rounded-[14px] w-[62px] h-[62px] gap-1">
                                            <p className="text-[12px] mt-[02px] font-semibold ">
                                                {item.weekDay}
                                            </p>
                                            <p className="text-[26px] mt-[-15px] font-bold"
                                                id={item.id}
                                            >
                                                {item.dateNumber}
                                            </p>
                                            <p className="text-[700] text-[14px] mt-[-14px] font-bold ">
                                                {item.monthName}
                                            </p>
                                        </div>
                                    </Link>
                                ))}
                            </div>
                        </div>
                    }
                </>
            </div>
            <div className="grid grid-cols-12 gap-6 mt-[25px] md:mt-[35px] lg:mt-[50px]">
                <div className="col-span-12 md:col-span-12 lg:col-span-2">
                    <div className="grid grid-cols-12 gap-3">
                        <div className="flex flex-col justify-center items-center col-span-4 md:col-span-4 lg:col-span-12 bg-[#fff] shadow-md rounded-[17px] pl-[10px] pt-[10px] pr-[10px] pb-[10px]">
                            <div className="bg-[#3682F7] w-[50px] h-[25px] rounded mt-2 flex justify-center shadow-md">
                                <div className="bg-[#FFFF] w-[18px] h-[12px] rounded flex justify-center mt-2 ">
                                    <p className="text-[#3682F7] text-[12px] font-semibold mt-[-4px]">
                                        ↑
                                    </p>
                                </div>
                                <p className="text-[14px] text-[#FFFF] mt-[3px] ml-[4px]">
                                    15%
                                </p>
                            </div>
                            <div className="flex flex-col justify-center items-center">
                                <p className="text-[#000000] text-[34px] md:text-[38px]  lg:text-[40px] font-medium ">
                                    <CounterReservas />
                                </p>
                                <p className="text-[#000000] text-[15px] font-light mt-[-12px]">
                                    Reservas
                                </p>
                            </div>
                        </div>

                        <div className="flex flex-col justify-center items-center col-span-4 md:col-span-4 lg:col-span-12 bg-[#fff] shadow-md rounded-[17px] pl-[10px] pt-[10px] pr-[10px] pb-[10px]">
                            <div className="bg-[#3682F7] w-[50px] h-[25px] rounded mt-2 flex justify-center shadow-md">
                                {" "}
                                <div className="bg-[#FFFF] w-[18px] h-[12px] rounded flex justify-center mt-2 ">
                                    <p className="text-[#3682F7] text-[12px] font-semibold mt-[-4px]">
                                        ↑
                                    </p>
                                </div>
                                <p className="text-[14px] text-[#FFFF] mt-[3px] ml-[4px]">
                                    15%
                                </p>
                            </div>
                            <div className="flex flex-col justify-center items-center">
                                <p className="text-[#000000] text-[34px] md:text-[38px]  lg:text-[40px] font-medium ">
                                    <CounterConfirmadas />
                                </p>
                                <p className="text-[#000000] text-[15px] font-light mt-[-12px]">
                                    Confirmadas
                                </p>
                            </div>
                        </div>

                        <div className="flex flex-col justify-center items-center col-span-4 md:col-span-4 lg:col-span-12 bg-[#fff] shadow-md rounded-[17px] pl-[10px] pt-[10px] pr-[10px] pb-[10px]">
                            <div className="bg-[#EE002B] w-[50px] h-[25px] rounded mt-2 flex justify-center shadow-md">
                                <div className="bg-[#FFFF] w-[18px] h-[12px] rounded flex justify-center mt-2 ">
                                    <p className="text-[#EE002B] text-[12px] font-semibold mt-[-4px]">
                                        ↑
                                    </p>
                                </div>
                                <p className="text-[14px] text-[#FFFF] mt-[3px] ml-[4px]">
                                    15%
                                </p>
                            </div>
                            <div className="flex flex-col justify-center items-center">
                                <p className="text-[#000000] text-[34px] md:text-[38px]  lg:text-[40px]  font-medium ">
                                    <CounterCancel />
                                </p>
                                <p className="text-[#000000] text-[15px] font-light mt-[-12px]">
                                    Canceladas
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="col-span-12 md:col-span-12 lg:col-span-10 ">
                    <Tabs className="">
                        <TabList className="flex flex-row w-full gap-4 md:w-[280px] lg:w-[300px] bg-[#E4E7EB] focus:outline-none rounded-t-lg h-8">
                            <Tab className="cursor-pointer flex justify-center items-center text-[12px] focus:text-[#fff] focus:bg-[#3682F7] focus:text-[#fff] px-2">
                                Siguientes
                            </Tab>
                            <Tab className="cursor-pointer text-gray-500 text-[12px] flex justify-center items-center focus:bg-[#3682F7] focus:text-[#fff]  px-2 py-2">
                                Todos
                            </Tab>
                            <Tab className="cursor-pointer text-gray-500 text-[12px] flex justify-center items-center focus:bg-[#3682F7] focus:text-[#fff]  px-2 py-2">
                                Confirmados
                            </Tab>
                            <Tab className="cursor-pointer text-gray-500 text-[12px] flex justify-center items-center focus:bg-[#3682F7] focus:text-[#fff]  px-2 py-2">
                                Cancelados
                            </Tab>
                        </TabList>

                        <TabPanel>
                            <div className="grid grid-cols-12">
                                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                                    {/* <TableCitasNext /> */}
                                </div>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="grid grid-cols-12">
                                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                                    <TableCitasAll />
                                </div>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="grid grid-cols-12">
                                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                                    <TableCitasConfir />
                                </div>
                            </div>
                        </TabPanel>
                        <TabPanel>
                            <div className="grid grid-cols-12">
                                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                                    <TableCitasCancel />
                                </div>
                            </div>
                        </TabPanel>
                    </Tabs>
                </div>
            </div>
        </div>
    )
}
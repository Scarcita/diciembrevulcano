import React, { useEffect, useState, FlatList } from 'react';
import Image from 'next/image';
import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'

// import ImgLogoFord from '../../public/Desktop5/logoFord.svg'

export default function ListOTsCerradas() {
    const [isLoading, setIsLoading] = useState(true)
    const [data, setData] = useState([]);
    console.log(data);

    const getDatos = () => {
        setIsLoading(true)
        fetch('https://slogan.com.bo/vulcano/orders/invoicingDashboard')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    var temp = [];
                    Object.values(data.data).map((result) =>{
                        temp.push(result);
                    })
    
                    setData(temp[10])
                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })
    }

    useEffect(() => {
        getDatos();
    }, []);

    return (

        isLoading ?
            <div className='flex justify-center items-center self-center mt-[30px]'  >
                <Spinner color="#3682F7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
            <div >
                <div style={{maxHeight: '225px', overflowY: 'auto'}} className="mt-3">
                    <Table data={data} />
                </div>
                <div className="grid grid-cols-12 ">
                    <button
                    style={{position: 'sticky', top: 0}} className="col-span-12 md:col-span-12 lg:col-span-12 h-[35px] bg-[#3682F7] text-[#FFFFFF] rounded-b-[24px] hover:bg-[#FFFFFF] hover:text-[#3682F7] hover:border-2 hover:border-[#3682F7] ">Ver todo</button>
                </div>  
            </div>


    )

}

const Table = (props) => {
    const { data } = props;

    return (
        <div className="grid grid-cols-12">
            <table className={`col-span-12 md:col-span-12 lg:col-span-12 h-[225px] bg-[#FFFFFF] rounded-t-[24px]`}>
                <tbody>
                    <div className="grid grid-cols-1 divide-y pl-3">
     
                        {data.map(row => <TableRow key={data.id} row={row} />)}
                    </div>
                </tbody>
                <tfoot>
                
                </tfoot>
            </table>
        </div>

    );
};



class TableRow extends React.Component {

    render() {

        let row = this.props.row;

        return (
            <div className="flex flex-row">
                <div className="w-[54px] h-[54px] rounded-full bg-[#F6F6FA] items-center self-center">

                    <Image
                        src={row.car.cars_models_version.cars_model.catalogues_record.additional_info}
                        alt="logo"
                        layout="fixed"
                        width={54}
                        height={54}
                        className="rounded-full"
                    />


                </div>
                <div className="px-3 py-2">
                    <div className="w-[72px] h-[15px] bg-[#3682F7] rounded-[13px] text-center items-center justify-center">
                        <p className="text-[10px] text-[#FFFFFF] text-center self-center">{row.status}</p>

                    </div>
                    <tr className="text-[14px] text-[#000000] font-bold">{row.client.name}</tr>
                    <tr className="text-[12px] text-[#000000]"> {row.car.cars_models_version.name} *{row.car.plate} *{row.car.vin}</tr>
                </div>

            </div>
        )

    }
} 
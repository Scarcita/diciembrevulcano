import React, { useEffect, useState } from "react";
import Image from "next/image";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import imagenOpciones from "../../../public/opciones.svg";
import imagenMercedes from "../../../public/mercedes.png";
import { ReactSearchAutocomplete } from "react-search-autocomplete";
import Spinner from "react-activity/dist/Spinner";
import "react-activity/dist/Spinner.css";

function Vehiculo() {
  const [searchStringPlate, setSearchStringPlate] = useState("");
  const [searchStringVin, setSearchStringVin] = useState("");
  const [dataPlate, setDataPlate] = useState("");
  const [dataVin, setDataVin] = useState("");
  const [isLocked, setIsLocked] = useState(true);
  const [fillData, setFillData] = useState(false);
  const [marca, setMarca] = useState("");
  const [modelo, setModelo] = useState("");
  const [version, setVersion] = useState("");
  const [color, setColor] = useState("");
  const [kilometraje, setKilometraje] = useState(" ");
  const [transmision, setTransmision] = useState("");
  const [showDots, setShowDots] = useState(false);

  const guardarDatos = (formData) => {
    let data = new FormData();
    data.append("plate", marca);
    data.append("enabled", parseInt(modelo));
    data.append("version_id", version);
    data.append("year", kilometraje);
    data.append("transmission", color);
    data.append("created_by", 1);

    fetch("https://slogan.com.bo/vulcano/cars/addMobile", {
      method: "POST",
      body: data,
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          setMarca(data.data.plate);
          setModelo(data.data.enabled);
          setVersion(data.data.version_id);
          setColor(data.data.transmission);
          setKilometraje(data.data.year);
          setTransmision(data.data.transmission);
        } else {
          console.error(data.error);
        }
      })
      .then(getDatosPlaca(), getDatosVin());
  };

  const getDatosPlaca = () => {
    fetch("https://slogan.com.bo/vulcano/cars/allBy/plate")
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          const keyARR = Object.keys(data.data);
          var dataArr = [];
          for (let index = 0; index < keyARR.length; index++) {
            const element = keyARR[index];
            // console.log(element);
            // console.log(data.data[element]);
            dataArr.push({ id: index, name: data.data[element] });
          }
          // console.log(dataArr);
          setDataPlate(dataArr);
        } else {
          // console.error(data.error);
          alert("Datos no encontrados o no existen");
        }
      });
  };

  // useEffect(() => {
  //     console.log("getDatosPlaca: " + Object.values(dataPlate));
  // }, [dataPlate]);

  const getDatosVin = () => {
    fetch("https://slogan.com.bo/vulcano/cars/allBy/vin")
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          // console.log(data.data);
          const keyARR = Object.keys(data.data);
          var dataArr = [];
          for (let index = 0; index < keyARR.length; index++) {
            const element = keyARR[index];
            // console.log(element);
            // console.log(data.data[element]);
            dataArr.push({ id: element, name: data.data[element] });
          }
          // console.log(dataArr);
          setDataVin(dataArr);
        } else {
          // console.error(data.error);
          alert("datos no encontrados o no existen.");
        }
      });
  };
  useEffect(() => {
    getDatosPlaca();
    getDatosVin();
  }, []);

  // PARA LA ANIMACION DEL CANDADO0
  useEffect(() => {
    for (let key of dataPlate) {
      // console.log(key.name);
      // console.log(searchStringPlate);
      if (searchStringPlate == key.name) {
        setIsLocked(true);
        // console.log(true);
        break;
      } else {
        setIsLocked(false);
        setFillData(false);
      }
    }
  }, [searchStringPlate, dataPlate]);

  useEffect(() => {
    for (let key of dataVin) {
      // console.log(key.name);
      // console.log(searchStringVin);
      if (searchStringVin == key.name) {
        setIsLocked(true);
        // console.log(true);
        break;
      } else {
        setIsLocked(false);
        setFillData(false);
      }
    }
  }, [searchStringVin, dataVin]);

  const handleOnSearchPlate = (string, results) => {
    setSearchStringPlate(string);
  };

  const handleOnSearchVin = (string, results) => {
    setSearchStringVin(string);
  };

  const handleOnSelectPlate = (item) => {
    setShowDots(true);
    fetch("https://slogan.com.bo/vulcano/cars/getById/" + item.id)
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          console.table("handleOnSelectPlate: " + Object.entries(data.data));
          setMarca(data.data.plate);
          setModelo(data.data.enabled);
          setVersion(data.data.version_id);
          setColor(data.data.transmission);
          setKilometraje(data.data.year);
          setTransmision(data.data.transmission);
          setFillData(true);
          setShowDots(false);
          setIsLocked(true);
        } else {
          console.error("handleOnSelectPlate: " + data.error);
          alert("Datos no encontrados o no existen");
        }
      });
  };

  const handleOnSelectVin = (item) => {
    setShowDots(true);
    fetch("https://slogan.com.bo/vulcano/cars/getById/" + item.id)
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          console.log("handleOnSelectVin: " + data.data);
          setMarca(data.data.plate);
          setModelo(data.data.enabled);
          setVersion(data.data.version_id);
          setColor(data.data.transmission);
          setKilometraje(data.data.year);
          setTransmision(data.data.transmission);
          setFillData(true);
          // setShowDots(false)
          setIsLocked(true);
        } else {
          console.error("handleOnSelectVin: " + data.error);
          alert("Datos no encontrados o no existen");
        }
      });
  };

  const formatResult = (item) => {
    return (
      <>
        <div className="flex flex-row ">
          <Image
            style={{ objectFit: "contain" }}
            src={item.additional_info}
            className={"w-[17px] h-[17px]"}
            alt="logo"
          />
          <span className="ml-2">{item.name}</span>
        </div>
      </>
    );
  };

  // VALIDACION
  const validationSchema = Yup.object().shape({
    placa: Yup.string()
      .required("campo obligatorio")
      .matches(/^[a-zA-Z0-9-]*$/, "Ingresar números y letras"),
    vin: Yup.string()
      .required("campo obligatorio")
      .matches(/^[a-zA-Z0-9-]*$/, "Ingresar números y letras"),
    marca: Yup.string()
      .required("campo obligatorio")
      .matches(/^[a-zA-Z0-9-]*$/, "Ingresar números y letras"),
    numero: Yup.string()
      .required("campo obligatorio")
      .matches(/^[0-9]*$/, "Ingresar un numero valido"),
    modelo: Yup.string()
      .required("campo obligatorio")
      .matches(/^[a-zA-Z0-9-]*$/, "Ingresar solo letras"),
    color: Yup.string()
      .required("campo obligatorio")
      .matches(/^[a-zA-ZÀ-ÿ\u00f1\u00d1]*$/, "Ingresar solo letras"),
    version: Yup.string()
      .required("campo obligatorio")
      .matches(/^[a-zA-Z0-9-]*$/, "Ingresar números y letras"),
    kilometraje: Yup.string()
      .required("campo obligatorio")
      .matches(/^[0-9]*$/, "Ingresar un numero valido"),
    transmision: Yup.string()
      .required("campo obligatorio")
      .matches(/^[a-zA-ZÀ-ÿ\u00f1\u00d1]*$/, "Ingresar solo letras"),
  });

  const formOptions = { resolver: yupResolver(validationSchema) };
  const { register, handleSubmit, reset, formState } = useForm(formOptions);
  const { errors } = formState;

  function onSubmit(data) {
    // console.warn(data);
    guardarDatos(data);
    // console.log("onSubmit guardarDatos: " + onSubmit);
    alert("SUCCESS!! :-)\n\n" + JSON.stringify(data, null, 4));
    return false;
  }

  return (
    <div>
      <div className="w-full flex flex-row justify-start items-center">
        <p className=" font-bold text-lg">Vehículo</p>
      </div>
      <div className="bg-[#FFFF] rounded-2xl justify-center items-center mt-[10px]">
        <div className="px-2 grid grid-cols-12 gap-2 justify-center items-center">
          <div className="col-span-12 md:col-span-6 lg:col-span-6 h-full justify-center items-center mt-[1.5px]">
            <div className="w-full flex flex-row justify-start items-center">
              <label className="font-light text-[#D9D9D9] text-lg">Placa</label>
            </div>
            <div className="w-full">
              <ReactSearchAutocomplete
                items={dataPlate}
                fuseOptions={{ threshold: 0 }}
                onSearch={handleOnSearchPlate}
                inputSearchString={searchStringPlate}
                onSelect={handleOnSelectPlate}
                formatResult={formatResult}
                showItemsOnFocus={true}
                styling={{
                  height: "44.26px",
                  borderRadius: "10px",
                  backgroundColor: "#000000",
                  boxShadow: "12px",
                  hoverBackgroundColor: "gray",
                  color: "#FFFF",
                  fontSize: "12px",
                  iconColor: "#FFFF",
                  lineColor: "#FFFF",
                  placeholderColor: "#FFFF",
                  clearIconMargin: "3px 8px 0 0",
                  cursor: "pointer",
                  zIndex: 8,
                }}
              />
            </div>
          </div>
          <div className="col-span-12 md:col-span-6 lg:col-span-6 h-full flex flex-col justify-center items-center">
            <div className="w-full flex flex-row justify-start items-center">
              <label className="font-light text-[#D9D9D9] text-lg">VIN</label>
            </div>
            <div className="w-full">
              <ReactSearchAutocomplete
                items={dataVin}
                fuseOptions={{ threshold: 0 }}
                onSearch={handleOnSearchVin}
                inputSearchString={searchStringVin}
                onSelect={handleOnSelectVin}
                formatResult={formatResult}
                showItemsOnFocus={true}
                styling={{
                  height: "44.26px",
                  borderRadius: "10px",
                  backgroundColor: "#D9D9D9",
                  boxShadow: "12px",
                  hoverBackgroundColor: "#000000",
                  color: "#FFFF",
                  fontSize: "12px",
                  iconColor: "#FFFF",
                  lineColor: "#000000",
                  placeholderColor: "#FFFF",
                  clearIconMargin: "3px 8px 0 0",
                  cursor: "pointer",
                  zIndex: 8,
                }}
              />
            </div>
          </div>
        </div>

        <div className="grid grid-cols-12 mt-2 ml-3 mr-3 flex justify-center items-center">
          <hr className="col-span-12 md:col-span-12 lg:col-span-12 border-[1px]" />
        </div>

        <div>
          <form
            id="formulario"
            className="flex flex-col"
            onClick={handleSubmit(onSubmit)}
          >
            <div className="px-2 w-full h-1/6 flex flex-row justify-between items-center mt-[2px]">
              <div className="w-full flex flex-row justify-start items-center">
                <p className="font-medium text-lg">Detalles de registro</p>
              </div>
              <div className="flex flex-row justify-center items-center">
                {isLocked == false ? (
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 24 24"
                    fill="currentColor"
                    className=" text-[#D3D3D3] w-[19px] h-[18px]"
                  >
                    <path d="M18 1.5c2.9 0 5.25 2.35 5.25 5.25v3.75a.75.75 0 01-1.5 0V6.75a3.75 3.75 0 10-7.5 0v3a3 3 0 013 3v6.75a3 3 0 01-3 3H3.75a3 3 0 01-3-3v-6.75a3 3 0 013-3h9v-3c0-2.9 2.35-5.25 5.25-5.25z" />
                  </svg>
                ) : (
                  <div
                    className="w-[19px] h-[18px] m-2"
                    onClick={() => {
                      setIsLocked(!isLocked);
                    }}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 24 24"
                      fill="currentColor"
                      className="text-[#D3D3D3] w-[19px] h-[18px]"
                    >
                      <path
                        fillRule="evenodd"
                        d="M12 1.5a5.25 5.25 0 00-5.25 5.25v3a3 3 0 00-3 3v6.75a3 3 0 003 3h10.5a3 3 0 003-3v-6.75a3 3 0 00-3-3v-3c0-2.9-2.35-5.25-5.25-5.25zm3.75 8.25v-3a3.75 3.75 0 10-7.5 0v3h7.5z"
                        clipRule="evenodd"
                      />
                    </svg>
                  </div>
                )}
                <div className="w-[5px] h-[19px] m-2">
                  <Image
                    src={imagenOpciones}
                    layout="responsive"
                    alt="imagenOpciones"
                  />
                </div>
              </div>
            </div>
            <div className="px-2 grid grid-cols-12 gap-2 justify-center items-center">
              <div className="col-span-12 md:col-span-6 lg:col-span-6 h-full flex flex-col justify-center items-center">
                <div className="w-full flex flex-row justify-start items-center">
                  <label className="text-s m text-[#A5A5A5] font-light">
                    Marca
                  </label>
                </div>
                {showDots ? (
                  <div className="flex justify-center items-center">
                    <Spinner
                      color="#3682F7"
                      size={17}
                      speed={1}
                      animating={true}
                      style={{ marginLeft: "auto", marginRight: "auto" }}
                    />
                  </div>
                ) : (
                  <div className="w-full h-8 border-[#D3D3D3] border-2 rounded-lg flex justify-center items-center">
                    <div className="w-full flex justify-center items-center">
                      <div className="w-4 relative left-2">
                        <Image
                          src={imagenMercedes}
                          layout="responsive"
                          width="20px"
                          height="20px"
                          alt="imagenMercedes"
                        />
                      </div>

                      <input
                        className="w-full  border-none text-left ml-[20px] "
                        type="text"
                        disabled={isLocked}
                        name="marca"
                        {...register("marca")}
                        value={marca}
                        onChange={(e) => {
                          setMarca(e.target.value);
                        }}
                      />
                    </div>
                  </div>
                )}
                <p className="text-[10px] text-[#FF0000] ">
                  {errors.marca?.message}
                </p>
              </div>
              <div className="col-span-12 md:col-span-6 lg:col-span-6 h-full flex flex-col justify-center items-center">
                <div className="w-full flex flex-row justify-start items-center">
                  <label className="text-s m text-[#A5A5A5] font-light">
                    Modelo
                  </label>
                </div>
                {showDots ? (
                  <div className="flex justify-center items-center">
                    <Spinner
                      color="#3682F7"
                      size={17}
                      speed={1}
                      animating={true}
                      style={{ marginLeft: "auto", marginRight: "auto" }}
                    />
                  </div>
                ) : (
                  <div className="w-full flex flex-row justify-center items-center">
                    <input
                      className="w-full h-8 border-[#D3D3D3] border-2 rounded-lg text-left text-sm font-Dosis font-normal"
                      name="modelo"
                      type="text"
                      {...register("modelo")}
                      disabled={isLocked}
                      value={modelo}
                      onChange={(e) => {
                        setModelo(e.target.value);
                      }}
                    />
                  </div>
                )}
                <p className="text-[10px] text-[#FF0000]">
                  {errors.modelo?.message}
                </p>
              </div>
            </div>
            <div className="px-2 grid grid-cols-12 gap-2 justify-center items-center">
              <div className="col-span-12 md:col-span-6 lg:col-span-6 h-full flex flex-col justify-center items-center">
                <div className="w-full flex flex-row justify-start items-center">
                  <label className="text-s m text-[#A5A5A5] font-light">
                    Versión
                  </label>
                </div>
                {showDots ? (
                  <div className="flex justify-center items-center">
                    <Spinner
                      color="#3682F7"
                      size={17}
                      speed={1}
                      animating={true}
                      style={{ marginLeft: "auto", marginRight: "auto" }}
                    />
                  </div>
                ) : (
                  <input
                    className="w-full h-8 border-[#D3D3D3] border-2 rounded-lg text-sm text-center font-normal"
                    name="version"
                    type="text"
                    {...register("version")}
                    disabled={isLocked}
                    value={version}
                    onChange={(e) => {
                      setVersion(e.target.value);
                    }}
                  />
                )}
                <p className="text-[10px] text-[#FF0000]">
                  {errors.version?.message}
                </p>
              </div>
              <div className="col-span-12 md:col-span-6 lg:col-span-6 h-full flex flex-col justify-center items-center">
                <div className="w-full flex flex-row justify-start items-center">
                  <label className="text-s m text-[#A5A5A5] font-light">
                    Color
                  </label>
                </div>
                {showDots ? (
                  <div className="flex justify-center items-center">
                    <Spinner
                      color="#3682F7"
                      size={17}
                      speed={1}
                      animating={true}
                      style={{ marginLeft: "auto", marginRight: "auto" }}
                    />
                  </div>
                ) : (
                  <input
                    className="w-full h-8 border-[#D3D3D3] border-2 rounded-lg text-sm text-center font-normal"
                    type="text"
                    name="color"
                    {...register("color")}
                    disabled={isLocked}
                    value={color}
                    onChange={(e) => {
                      setColor(e.target.value);
                    }}
                  />
                )}
                <p className="text-[10px] text-[#FF0000]">
                  {errors.color?.message}
                </p>
              </div>
              <div className="col-span-12 md:col-span-6 lg:col-span-6 h-full flex flex-col justify-center items-center">
                <div className="w-full flex flex-row justify-start items-center">
                  <label className="text-s m text-[#A5A5A5] font-light">
                    kilometraje
                  </label>
                </div>
                {showDots ? (
                  <div className="flex justify-center items-center">
                    <Spinner
                      color="#3682F7"
                      size={17}
                      speed={1}
                      animating={true}
                      style={{ marginLeft: "auto", marginRight: "auto" }}
                    />
                  </div>
                ) : (
                  <input
                    className="w-full h-8 border-[#D3D3D3] border-2 rounded-lg text-sm text-center font-normal"
                    type={"number"}
                    name="kilometraje"
                    {...register("kilometraje")}
                    disabled={isLocked}
                    value={kilometraje}
                    onChange={(e) => {
                      setKilometraje(e.target.value);
                    }}
                  />
                )}
                <p className="text-[10px] text-[#FF0000]">
                  {errors.kilometraje?.message}
                </p>
              </div>
              <div className="col-span-12 md:col-span-6 lg:col-span-6 h-full flex flex-col justify-center items-center">
                <div className="w-full flex flex-row justify-start items-center">
                  <label className="text-s m text-[#A5A5A5] font-light">
                    Transmisión
                  </label>
                </div>
                {showDots ? (
                  <div className="flex justify-center items-center">
                    <Spinner
                      color="#3682F7"
                      size={17}
                      speed={1}
                      animating={true}
                      style={{ marginLeft: "auto", marginRight: "auto" }}
                    />
                  </div>
                ) : (
                  <input
                    className="w-full h-8 border-[#D3D3D3] border-2 rounded-lg text-sm text-center font-normal"
                    type="text"
                    name="transmision"
                    {...register("transmision")}
                    disabled={isLocked}
                    value={transmision}
                    onChange={(e) => {
                      setTransmision(e.target.value);
                    }}
                  />
                )}
                <p className="text-[10px] text-[#FF0000]">
                  {errors.transmision?.message}
                </p>
              </div>
            </div>
          </form>
        </div>

        <div className="flex justify-end items-center">
          <div>
            <button
              className="w-[65px] h-7 bg-[#000] border-[#000] border-2 rounded-lg m-3 text-[15px] text-[#fff] text-center text-[12px] font-light"
              type="button"
              onClick={() => reset()}
            >
              Cancelar
            </button>
          </div>
          <div>
            <button
              type="submit"
              onClick={() => {
                guardarDatos();
                // dataPlate();
                // handleOnSearchVin();
                // window.location.href = window.Location
              }}
              className="w-[65px] h-7 bg-[#3682F7] border-[#3682F7] border-2 rounded-lg m-3 text-[15px] text-[#fff] text-center text-[12px] font-light"
            >
              Guardar
            </button>
          </div>
        </div>
      </div>
    </div>
    // </form>
  );
}

export default Vehiculo;

import React, { useEffect, useState } from 'react';
import Image from 'next/image'
import imagenOpciones from '../../../public/opciones.svg';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import { ReactSearchAutocomplete } from 'react-search-autocomplete';
function Cliente() {
    const [searchString, setSearchString] = useState("");
    const [showDots, setShowDots] = useState(true);
    const [nameData, setNameData] = useState('');
    const [isLocked, setIsLocked] = useState(false);
    const [fillData, setFillData] = useState(false);
    const [telefonoRegistro, setTelefonoRegistro] = useState('');
    const [emailRegistro, setEmailRegistro] = useState('');
    const [telefonoFacturacion, setTelefonoFacturacion] = useState('');
    const [nombreFacturacion, setNombreFacturacion] = useState('');

    const getDatos = () => {
        fetch('https://slogan.com.bo/vulcano/clients/allBy/name')
            .then(response => response.json())
            .then(nameData => {
                if (nameData.status) {
                    console.log(nameData.data);
                    const keyARR = Object.keys(nameData.data);
                    console.log(keyARR);
                    var dataArr = [];
                    for (let index = 0; index < keyARR.length; index++) {
                        const element = keyARR[index];
                        console.log(element);
                        console.log(nameData.data[element]);
                        dataArr.push({ id: element, name: nameData.data[element] });
                    }
                    console.log(dataArr);
                    setNameData(dataArr);
                } else {
                    console.error(nameData.error)
                }
            })
            .then(setShowDots(false))
    }
    useEffect(() => {
        getDatos();
    }, [])

    useEffect(() => {
        for (let key of nameData) {
            console.log('searchString: ' + searchString);
            console.log('key.name: ' + key.name);
            if (searchString == key.name) {
                console.log('true')
                setIsLocked(true)
                break;
            }
            else {
                setIsLocked(false);
                setFillData(false);
            }
        }
    }, [searchString, nameData]);

    const handleOnSearch = (string, results) => {
        setSearchString(string);
    };

    const handleOnSelect = (item) => {
        fetch('https://slogan.com.bo/vulcano/clients/getById/' + item.id)
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log('handleOnSelect: ' + data.data);
                    setTelefonoRegistro(data.data.phone)
                    setEmailRegistro(data.data.email)
                    setTelefonoFacturacion(data.data.phone)
                    setNombreFacturacion(data.data.name)
                    setFillData(true)
                } else {
                    console.error('handleOnSelect: ' + data.error)
                }
            })
            .then(setShowDots(false))
            .then(setIsLocked(true));
    };


    const formatResult = (item) => {
        return (
            <>
                <div className='flex flex-row '>
                    <Image
                        style={{ 'objectFit': 'contain' }}
                        src={item.additional_info}
                        className={"w-[17px] h-[17px]"}
                        alt=''
                    />
                    <span className='ml-2'>
                        {item.name}
                    </span>
                </div>
            </>
        )
    }

    const validationSchema = Yup.object().shape({
        telefonoRegistro: Yup.string()
            .required('Teléfono is required')
            .matches(/^[0-9]*$/, 'Ingrese un numero valido'),
        emailRegistro: Yup.string()
            .required('Email is required')
            .email('Email is invalid'),
        telefonoFacturacion: Yup.string()
            .required('Teléfono is required')
            .matches(/^[0-9]*$/, 'Ingrese un numero valido'),
        nombreFacturacion: Yup.string()
            .required('Nombre is required')
            .matches(/^[a-zA-Z\s]*$/, 'Ingreso solo letras'),
    })

    const formOptions = { resolver: yupResolver(validationSchema) };
    const { register, handleSubmit, reset, formState } = useForm(formOptions);
    const { errors } = formState;
    const onSubmit = (data) => {
        console.log(data);
    }

    return (
        <form
            className='flex flex-col w-full h-full'
            onSubmit={handleSubmit(onSubmit)}
        >
            <div className='w-full flex flex-row justify-start items-center'>
                <p className=' font-bold text-lg'>
                    Cliente
                </p>
            </div>
            <div className='bg-[#fff] w-full px-2 rounded-2xl justify-center items-center mt-[10px]'>
                <div className='flex flex-col'>
                    <div className='w-full flex flex-row justify-start items-center'>
                        <label className='font-light text-[#D9D9D9] text-lg'>
                            Nombre
                        </label>
                    </div>
                    <div className='w-full'>
                        <ReactSearchAutocomplete
                            items={nameData}
                            fuseOptions={{ threshold: 0 }}
                            onSearch={handleOnSearch}
                            inputSearchString={searchString}
                            onSelect={handleOnSelect}
                            formatResult={formatResult}
                            showItemsOnFocus={true}
                            styling={
                                {
                                    height: '44.26px',
                                    borderRadius: '10px',
                                    backgroundColor: '#D9D9D9',
                                    boxShadow: "12px",
                                    hoverBackgroundColor: "#000000",
                                    color: "#FFFF",
                                    fontSize: "12px",
                                    iconColor: "#FFFF",
                                    lineColor: "#000000",
                                    placeholderColor: "#FFFF",
                                    clearIconMargin: "3px 8px 0 0",
                                    cursor: "pointer",
                                    zIndex: 8,
                                }
                            }
                        />
                    </div>

                    <div className='grid grid-cols-12 mt-2 ml-3 mr-3 flex justify-center items-center'>
                        <hr
                            className='col-span-12 md:col-span-12 lg:col-span-12 border-[1px]'
                        />
                    </div>

                    <div className=' w-full h-1/6 flex flex-row justify-between items-center mt-2'>
                        <div className='w-full flex flex-row justify-start items-center'>
                            <p className='font-medium text-lg'>
                                Detalles de registro
                            </p>
                        </div>
                        <div className='flex flex-row justify-center items-center'>
                            <div
                                className='w-[19px] h-[18px] m-2'
                                onClick={() => {
                                    setIsLocked(!isLocked)
                                }}
                            >
                                {isLocked == false ?
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className=" text-[#D3D3D3] w-[19px] h-[18px]">
                                        <path d="M18 1.5c2.9 0 5.25 2.35 5.25 5.25v3.75a.75.75 0 01-1.5 0V6.75a3.75 3.75 0 10-7.5 0v3a3 3 0 013 3v6.75a3 3 0 01-3 3H3.75a3 3 0 01-3-3v-6.75a3 3 0 013-3h9v-3c0-2.9 2.35-5.25 5.25-5.25z" />
                                    </svg>
                                    :
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor" className="text-[#D3D3D3] w-[19px] h-[18px]">
                                        <path fillRule="evenodd" d="M12 1.5a5.25 5.25 0 00-5.25 5.25v3a3 3 0 00-3 3v6.75a3 3 0 003 3h10.5a3 3 0 003-3v-6.75a3 3 0 00-3-3v-3c0-2.9-2.35-5.25-5.25-5.25zm3.75 8.25v-3a3.75 3.75 0 10-7.5 0v3h7.5z" clipRule="evenodd"
                                        />
                                    </svg>
                                }
                            </div>
                            <div className='w-[5px] h-[19px] m-2'>
                                <Image
                                    src={imagenOpciones}
                                    layout='responsive'
                                    alt='imagenOpciones'
                                />
                            </div>
                        </div>
                    </div>
                    {/* <div className=' w-full h-2/6 md:flex md:flex-row '> */}
                    <div className='px-2 grid grid-cols-12 gap-2 justify-center items-center'>
                        {/* <div className='md:w-1/3'> */}
                        <div className='col-span-12 md:col-span-5 lg:col-span-4 h-full justify-center items-center'>
                            <div className='w-full flex flex-row justify-start items-center'>
                                <label className='font-light text-[#D9D9D9] text-lg'>
                                    Teléfono
                                </label>
                            </div>
                            <input
                                className='w-full  h-8 border-[#D3D3D3] border-2 rounded-lg text-[13px] text-[#000] text-center text-[14px] font-normal'
                                type="number"
                                disabled={isLocked}
                                name='telefonoRegistro'
                                {...register("telefonoRegistro")}
                                value={fillData ? telefonoRegistro : ''}
                                onChange={(e) => {
                                    setTelefonoRegistro(e.target.value)
                                }}
                            />
                            <div className=" w-full flex flex-row justify-center items-center">
                                <p className='text-[12px] text-[#FF0000] '>
                                    {errors.telefonoRegistro?.message}
                                </p>
                            </div>
                        </div>
                        <div className='col-span-12 md:col-span-7 lg:col-span-8 h-full flex flex-col justify-center items-center'>
                            <div className='w-full flex flex-row justify-start items-center'>
                                <label className='font-light text-[#D9D9D9] text-lg'>
                                    Email
                                </label>
                            </div>
                            <input
                                className='w-full h-8  border-[#D3D3D3] border-2 rounded-lg text-[13px] text-[#000] text-left text-[14px] font-Dosis font-normal px-2 '
                                name='emailRegistro'
                                type="email"
                                {...register("emailRegistro")}
                                disabled={isLocked}
                                value={fillData ? emailRegistro : ''}
                                onChange={(e) => {
                                    setEmailRegistro(e.target.value)
                                }}
                            />
                            <div className=" w-full flex flex-row justify-center items-center">
                                <p className='text-[12px] text-[#FF0000] '>
                                    {errors.emailRegistro?.message}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className='w-full h-1/6 flex flex-row justify-between items-center mt-2'>
                        <div className='w-full flex flex-row justify-start items-center'>
                            <p className='font-medium text-lg'>
                                Detalles de facturación
                            </p>
                        </div>
                    </div>
                    <div className='px-2 grid grid-cols-12 gap-2 mt-2 justify-center items-center'>
                        {/* <div className='md:w-1/3'> */}
                        <div className='col-span-12 md:col-span-5 lg:col-span-4 h-full justify-center items-center'>
                            <div className='w-full flex flex-row justify-start items-center'>
                                <label className='font-light text-[#D9D9D9] text-lg'>
                                    Teléfono
                                </label>
                            </div>
                            <div>
                                <input
                                    className='w-full h-8 border-[#D3D3D3] border-2 rounded-lg text-[13px] text-[#000] text-center text-[14px] font-normal'
                                    name='telefonoFacturacion'
                                    type="number"
                                    {...register("telefonoFacturacion")}
                                    disabled={isLocked}
                                    value={fillData ? telefonoFacturacion : ''}
                                    onChange={(e) => {
                                        setTelefonoFacturacion(e.target.value)
                                    }}
                                />
                                <div className=" w-full flex flex-row justify-center items-center">
                                    <p className='text-[12px] text-[#FF0000] '>
                                        {errors.telefonoFacturacion?.message}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className='col-span-12 md:col-span-7 lg:col-span-8 h-full flex flex-col justify-center items-center'>
                            <div className='w-full flex flex-row justify-start items-center'>
                                <label className='font-light text-[#D9D9D9] text-lg'>
                                    Nombre/Razón Social
                                </label>
                            </div>
                            <input className='w-full h-8 border-[#D3D3D3] border-2 rounded-lg text-[13px] text-[#000] text-left text-[14px] font-Dosis font-normal px-2'
                                name='nombreFacturacion'
                                type="text"
                                {...register("nombreFacturacion")}
                                disabled={isLocked}
                                value={fillData ? nombreFacturacion : ''}
                                onChange={(e) => {
                                    setNombreFacturacion(e.target.value)
                                }}
                            />
                            <div className=" w-full flex flex-row justify-center items-center">
                                <p className='text-[12px] text-[#FF0000] '>
                                    {errors.nombreFacturacion?.message}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className='flex justify-end md:mt-4 lg:mt-2'>
                        <div>
                            <button className='w-[65px] h-7 bg-[#000] border-[#000] border-2 rounded-lg m-3'>
                                <p className='text-[15px] text-[#fff] text-center text-[12px] font-light'>
                                    Cancelar
                                </p>
                            </button>
                        </div>
                        <div>
                            <button
                                type={'submit'}
                                className='w-[65px] h-7 bg-[#3682F7] border-[#3682F7] border-2 rounded-lg m-3'>
                                <p className='text-[15px] text-[#fff] text-center text-[12px] font-light'>
                                    Guardar
                                </p>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    )
}
export default Cliente;

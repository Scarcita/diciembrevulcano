import React, { useState } from "react";
import { useForm } from 'react-hook-form';
import Tippy from '@tippyjs/react'
import 'tippy.js/dist/tippy.css'
import 'tippy.js/animations/scale.css'
import 'tippy.js/themes/light.css';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';

export default function ModalServicios(props) {
    const { modal, setModal } = props;
    const { servicesList, setServicesList } = props;
    const { selectedItem } = props;
    console.log("modal servicis: " + Object.entries(selectedItem));
    const [codigo, setCodigo] = useState(selectedItem.code);
    const [nombre, setNombre] = useState(selectedItem.name);
    const [tiempo, setTiempo] = useState(selectedItem.time);
    const [precio, setPrecio] = useState(selectedItem.price);

    console.log("MODAL OCULTO: " + codigo + nombre + tiempo);

    const validationSchema = Yup.object().shape({
        codigo: Yup.string()
            .required('Is required')
            .matches(/^[a-zA-Z0-9-]*$/, 'Ingreso números y letras'),
        nombre: Yup.string()
            .required('Is required')
            .matches(/^[a-zA-Z0-9-]*$/, 'Ingreso números y letras'),
        tiempo: Yup.string()
            .required('Is required')
            .matches(/^[a-zA-Z0-9-]*$/, 'Ingreso números y letras'),
        precio: Yup.string()
            .required('Is required'),
            // .matches(/^[a-zA-Z0-9-]*$/, 'Ingreso números y letras'),
        sale_price: Yup.string()
            .required('Is required')
            .matches(/^[a-zA-Z0-9-]*$/, 'Ingreso números y letras')
    });

    const formOptions = { resolver: yupResolver(validationSchema) };
    const { register, handleSubmit, formState } = useForm(formOptions);
    const { errors } = formState;

    const AnadirCarrito = (formData) => {
        console.log("formData: " + formData)

        selectedItem["sale_price"] = formData.sale_price;

        var bodyTemp = servicesList;
        bodyTemp.push(selectedItem);
        setServicesList([...bodyTemp]);
        console.log(bodyTemp);
        setModal(false);
    }

    function onSubmit(data) {
        AnadirCarrito(data);
    };

    return (
        <>
            <form
                id='formulario'
                className='flex flex-col m-4 w-full h-full'
                onSubmit={handleSubmit(onSubmit)}
            >
                <div>
                    <Tippy
                        trigger='click'
                        placement={'left'}
                        animation='scale'
                        theme='light'
                        interactive={true}
                        content={
                            <Tippy>
                                <>
                                    <div className="flex flex-col justify-left items-left ">
                                        <button
                                            type="button"
                                            className="pt-1 pb-1 items-center flex"
                                            onClick={() => setModal(true)}
                                        >
                                            <svg
                                                xmlns="http://www.w3.org/2000/svg"
                                                fill="none"
                                                viewBox="0 0 24 24"
                                                strokeWidth="1.5"
                                                stroke="currentColor"
                                                className="w-6 h-6"
                                            >
                                                <path strokeLinecap="round" strokeLinejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z" />
                                            </svg>
                                            <p>Añadir</p>
                                        </button>
                                    </div>
                                </>
                            </Tippy>
                        }>
                        <button type="button">
                            <svg
                                width="8"
                                height="28"
                                viewBox="0 0 8 28"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                                className='w-[10px] h-[23px] '
                            >
                                <path d="M4 9.93548C6.21111 9.93548 8 11.7532 8 14C8 16.2468 6.21111 18.0645 4 18.0645C1.78889 18.0645 0 16.2468 0 14C0 11.7532 1.78889 9.93548 4 9.93548ZM0 4.06452C0 6.31129 1.78889 8.12903 4 8.12903C6.21111 8.12903 8 6.31129 8 4.06452C8 1.81774 6.21111 0 4 0C1.78889 0 0 1.81774 0 4.06452ZM0 23.9355C0 26.1823 1.78889 28 4 28C6.21111 28 8 26.1823 8 23.9355C8 21.6887 6.21111 19.871 4 19.871C1.78889 19.871 0 21.6887 0 23.9355Z" fill="#D9D9D9" />
                            </svg>
                        </button>
                    </Tippy>
                </div>
               {modal ? (
                    <>
                        <div className="fixed inset-0 z-10 overflow-y-auto">
                            <div
                                className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
                                onClick={() => setModal(false)}
                            />
                            <div className="flex items-center min-h-screen px-4 py-8">
                                <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">
                                    <div
                                        className="mt-[20px] grid grid-cols-12 gap-4 mt-[20px]"
                                        onSubmit={handleSubmit(onSubmit)}
                                    >
                                        <div className='col-span-12 md:col-span-12 lg:col-span-12 mt-[10px]'>
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>
                                                Código.
                                            </p>
                                            <input
                                                name="codigo"
                                                readOnly="true"
                                                type="text" {...register('codigo')}
                                                value={codigo}
                                                onChange={e => setCodigo(e.target.value)}
                                                className={`w-full h-[40px] outline-none bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px] form-control ${errors.codigo ? 'is-invalid' : ''}`}
                                            />
                                            <div className=" w-full flex flex-row justify-center items-center">
                                                <p className='text-[12px] text-[#FF0000] '>
                                                    {errors.codigo?.message}
                                                </p>
                                            </div>
                                        </div>
                                        <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>
                                                Nombre.
                                            </p>
                                            <input
                                                name="nombre"
                                                readOnly="true"
                                                type="text" {...register('nombre')}
                                                value={nombre}
                                                onChange={e => setNombre(e.target.value)}
                                                className={`w-full outline-none h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px] form-control ${errors.nombre ? 'is-invalid' : ''}`}
                                            />
                                            <div className=" w-full flex flex-row justify-center items-center">
                                                <p className='text-[12px] text-[#FF0000] '>
                                                    {errors.nombre?.message}
                                                </p>
                                            </div>
                                        </div>
                                        <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>
                                                Tiempo.
                                            </p>
                                            <input
                                                name="tiempo"
                                                readOnly="true"
                                                type="text" {...register('tiempo')}
                                                value={tiempo}
                                                onChange={e => setTiempo(e.target.value)}
                                                className={`w-full outline-none h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px] form-control ${errors.tiempo ? 'is-invalid' : ''}`}
                                            />
                                            <div className=" w-full flex flex-row justify-center items-center">
                                                <p className='text-[12px] text-[#FF0000] '>
                                                    {errors.tiempo?.message}
                                                </p>
                                            </div>
                                        </div>

                                        <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>
                                                Precio.
                                            </p>
                                            <input
                                                name="precio"
                                                readOnly="true"
                                                type="text" {...register('precio')}
                                                value={precio}
                                                onChange={e => setPrecio(e.target.value)}
                                                placeholder='Introdusca el Precio'
                                                className={`w-full outline-none h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px] form-control ${errors.precio ? 'is-invalid' : ''}`}
                                            />
                                            <div className=" w-full flex flex-row justify-center items-center">
                                                <p className='text-[12px] text-[#FF0000] '>
                                                    {errors.precio?.message}
                                                </p>
                                            </div>
                                        </div>

                                        <div className="col-span-6 md:col-span-6 lg:col-span-6 mt-[10px]">
                                            <p className='text-[12px] text-[#C1C1C1] mb-[2px]'>
                                                Precio Sugerido.
                                            </p>
                                            <input
                                                name="sale_price"
                                                type="text" {...register('sale_price')}
                                                placeholder='Introdusca el sale_price'
                                                className={`w-full outline-none h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px] form-control ${errors.sale_price ? 'is-invalid' : ''}`}
                                            />
                                            <div className=" w-full flex flex-row justify-center items-center">
                                                <p className='text-[12px] text-[#FF0000] '>
                                                    {errors.sale_price?.message}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="flex flex-row justify-between mt-[20px]">
                                        <div>
                                            <button
                                                className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                                                onClick={() => setModal(false)}>
                                                Cancelar
                                            </button>
                                            <button
                                                className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                                                type="submit"
                                                // onClick={() => {
                                                //     AnadirCarrito(selectedItem.id),
                                                //         setModal(false)
                                                // }}
                                            >
                                                Añadir
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </>
                ) : null}
            </form>
        </>
    )
}

import { useEffect } from "react";
import ModalService from "../ModalService";

export default function ListaServicios(props) {

    const { servicesList, setServicesList } = props;
    const { precioSugerido, setPrecioSugerido } = props;
    console.log("servicesList:" + servicesList);

    useEffect(() => {
        console.log("Precio Sugerido: " + precioSugerido)

    }, [precioSugerido])


    const handleDelete = (id) => {
        setServicesList(servicesList.filter(item => item.id !== id));
    };

    return (
        <div>
            {/* {servicesList.map((row, index) => {
                console.log("Services List: " + Object.entries(servicesList[0]))
                setPrecioSugerido(servicesList[0].sale_price)

                console.log("setPrecioSugerido: " + setPrecioSugerido);
                return (
                    <div className='bg-[#FFFF] mt-2 p-4 rounded-2xl flex justify-center items-center shadow-md'>
                        <div className='flex items-center'>
                            <div className='grid grid-cols-2 gap-8 flex justify-center items-center'>
                                <div>
                                    <p className="text-[20px] font-bold">
                                        {row.name}
                                    </p>
                                    <p className="text-[24px] font-bold text-[#FF0000]">
                                        Bs. {row.price}
                                    </p>
                                </div>
                                <div>
                                    <p className="font-light text-[15px] mt-1">
                                        {row.code}
                                    </p>

                                    <p className="text-[24px] mt-1 font-bold text-[#3682F7]">
                                        Bs. {row.sale_price}
                                    </p>
                                </div>

                            </div>

                            <div className="pl-3">
                                <ModalService id={row.id} servicesList={servicesList} setServicesList={servicesList} />

                                <button className="mt-3" onClick={() => handleDelete(row.id)}>
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        strokeWidth="1.5"
                                        stroke="currentColor"
                                        className="w-5 h-5 text-[#FF0000]">
                                        <path strokeLinecap="round" strokeLinejoin="round" d="M14.74 9l-.346 9m-4.788 0L9.26 9m9.968-3.21c.342.052.682.107 1.022.166m-1.022-.165L18.16 19.673a2.25 2.25 0 01-2.244 2.077H8.084a2.25 2.25 0 01-2.244-2.077L4.772 5.79m14.456 0a48.108 48.108 0 00-3.478-.397m-12 .562c.34-.059.68-.114 1.022-.165m0 0a48.11 48.11 0 013.478-.397m7.5 0v-.916c0-1.18-.91-2.164-2.09-2.201a51.964 51.964 0 00-3.32 0c-1.18.037-2.09 1.022-2.09 2.201v.916m7.5 0a48.667 48.667 0 00-7.5 0" />
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                )
            })} */}
        </div>
    )
}

import React, { useEffect, useState } from 'react';
import ModalPresupuestoProducto from './modalPresupuestoProducto';

const PresupuestoProducto = (props) => {
    const { dataColumnaDerecha, removeFromCart } = props;

    const [precioTotal, setPrecioTotal] = useState(0)
    const { precioSugerido, setPrecioSugerido } = props;

    useEffect(() => {
        console.log("Precio Sugerido Lista Productos: " + precioSugerido)

    }, [precioSugerido])

    useEffect(() => {
        const calculatetotal = () => {
            var temp_total = 0;
            dataColumnaDerecha.forEach(element => {
                temp_total += parseInt(element.price);
            });
            setPrecioTotal(temp_total);
        }
        calculatetotal();
    }, [dataColumnaDerecha])


    const calculatetotal = () => {
        var temp_total = 0;
        dataColumnaDerecha.forEach(element => {
            temp_total += element.price;
        });
        setPrecioTotal(temp_total);
    }
/////////////// SE ESTA USANDO SOLO PARA ISLOADING//////////
    // useEffect(() => {


    return (
        <div className='overflow-y h-500 divide-y' >
            <Table dataColumnaDerecha={dataColumnaDerecha} precioTotal={precioTotal}/>
        </div>
    )
}

const Table = (props) => {
    const { dataColumnaDerecha, precioTotal, headlist } = props;

    return (
        <div className=" grid grid-cols-12 gap-6 ">
            <table className='col-span-12 md:col-span-12 lg:col-span-12 bg-[#fff] min-h-[120px] rounded-[15px] shadow-md '>
                <tbody >
                {dataColumnaDerecha.map(row => 
                    <tr >
                         <td >
                            <div className='grid grid-cols-12 px-2 py-2'>
                                <div className='col-span-10 md:col-span-10 lg:col-span-10 '> 
                                    <div className='text-[20px] font-semibold'>
                                    {row.name}
                                    </div>
                                    <div className='text-[14px] font-normal'>
                                    {row.code}
                                    </div>
                                    <div className='flex'>
                                        <div className='text-[24px] text-[#3682F7] font-semibold'>
                                            BS. {row.price} 
                                        </div>
                                        <div className='text-[18px] text-[#A5A5A5] font-normal ml-[10px] self-center'>
                                            {row.created_by} ud/pzas.
                                        </div>
                                    </div>

                                </div>
                                <div className='col-span-2 md:col-span-2 lg:col-span-2 text-center'> 
                                    <ModalPresupuestoProducto
                                        row={row} 
                                        id={row.id}
                                        code={row.code}
                                        name={row.name}
                                        created_by={row.created_by}
                                        min_stock={row.min_stock}
                                        // index={index}
                                        // removeFromCart={removeFromCart}
                                        // setPrecioTotal={setPrecioTotal}
                                    />
                                
                                </div>
                            </div>
                         </td>
                    </tr>

                
                )}
                </tbody>
            </table>
            <div  className='col-span-12 md:col-span-12 lg:col-span-12'>
            <div className="grid grid-cols-12">
                    <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                        <p className="font-light text-[18px]">
                            Cantidad de ud/pzas: 
                            {/* {dataColumnaDerecha.length} */}
                        </p>
                    </div>
                    <div  className='col-span-12 md:col-span-12 lg:col-span-12 flex space-between'>
                        <div>
                            <p className="text-[37.31px] font-bold text-[#3682F7]">
                                Total
                            </p>
                        </div>
                        <div className='w-18'>
                            <p className="text-[37.31px] font-bold text-[#3682F7]">
                                Bs. {precioTotal}
                            </p>
                        </div>
                    </div>
                    <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                        <button
                            type={'submit'}
                            className=' h-[45px] w-full bg-[#3682F7] border-[#3682F7] border-2 rounded-lg text-[25.45px] text-[#fff]'>
                            Guardar
                        </button>
                    </div>
                    
            </div>

            </div>
        </div>

    );
};
export default PresupuestoProducto;

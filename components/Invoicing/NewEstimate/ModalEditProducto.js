import React, { useState, useEffect } from "react";
import Tippy from '@tippyjs/react'
import 'tippy.js/dist/tippy.css'
import 'tippy.js/animations/scale.css'
import 'tippy.js/themes/light.css';
import Link from "next/link";
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';

export default function ModalEditProducto(props) {
    const [showEditModal, setShowEditModal] = useState(false);
    const {
        row,
        id,
        code,
        name,
        created_by,
        min_stock
    } = props;

    const [nameForm, setNameForm] = useState(code);
    const [codeForm, setCodeForm] = useState(name);
    const [createdbyForm, setCreateByForm] = useState(min_stock);
    const [cantidadReq, setCantidadReq] = useState('')

    const validationSchema = Yup.object().shape({

        producto: Yup.string()
            .required('Is required')
            .matches(/^[a-zA-Z0-9-]*$/, 'Ingreso números y letras'),
        codigo: Yup.string()
            .required('Is required')
            .matches(/^[a-zA-Z0-9-]*$/, 'Ingreso números y letras'),
        cantidad: Yup.string()
            .required('Is required')
            .matches(/^[0-9]*$/, 'Ingrese un numero valido'),
        cantidadReq: Yup.string()
            .required('Is required')
            .matches(/^[0-9]*$/, 'Ingrese un numero valido'),
    });

    const formOptions = { resolver: yupResolver(validationSchema) };
    const { register, handleSubmit, formState } = useForm(formOptions);
    const { errors } = formState;

    function onSubmit(data) {
        // console.log('onSubmit data: ' + data);
    }

    return (
        <div>
            <form onSubmit={handleSubmit(onSubmit)}>

                <div
                    className="mt-[20px] grid grid-cols-12 gap-4 mt-[20px]"
                >
                    <div className="col-span-6 md:col-span-6 lg:col-span-6">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>
                            Nombre producto
                        </p>
                        <input
                            name="producto"
                            readOnly="true"
                            type={'text'}
                            className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]'
                            {...register('producto')}
                            value={nameForm}
                            onChange={(e) => {
                                setNameForm(e.target.value)
                            }}
                        />
                        <div className="text-[14px] text-[#FF0000]">
                            {errors.producto?.message}
                        </div>
                    </div>
                    <div className="col-span-6 md:col-span-6 lg:col-span-6">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>
                            Codigo
                        </p>
                        <input
                            name="codigo"
                            readOnly="true"
                            type={'text'}
                            className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]'
                            {...register('codigo')}
                            value={codeForm}
                            onChange={(e) => {
                                setCodeForm(e.target.value)
                            }}
                        />
                        <div className="text-[14px] text-[#FF0000]">
                            {errors.codigo?.message}
                        </div>
                    </div>
                    <div className="col-span-6 md:col-span-6 lg:col-span-6">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>
                            Cantidad en Stock
                        </p>
                        <input
                            name="cantidad"
                            readOnly="true"
                            type={'text'}
                            className='w-full h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[10px] text-[12px]'
                            {...register('cantidad')}
                            value={createdbyForm}
                            onChange={(e) => {
                                setCreateByForm(e.target.value)
                            }}
                        />
                        <div className="text-[14px] text-[#FF0000]">
                            {errors.cantidad?.message}
                        </div>
                    </div>

                    <div className="col-span-6 md:col-span-6 lg:col-span-6">
                        <p className='text-[12px] text-[#C1C1C1] mb-[2px] text-start'>
                            Cantidad requerida
                        </p>
                        <input
                            placeholder='Introdusca la cantidad que desea adquirir '
                            name="cantidadReq"
                            type="number"
                            value={cantidadReq}
                            className={`w-full outline-none h-[40px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px] mr-[10px] form-control ${errors.cantidadReq ? 'is-invalid' : ''}`}
                            onChange={(e) => setCantidadReq(e.target.value)}
                        />
                    </div>
                </div>
                <div className="flex flex-row justify-between mt-[20px]">
                    <div>
                        <h1 className="text-[12px] mt-[10px]">
                            This field is mandatory
                        </h1>
                    </div>
                    <div>
                        <button
                            className="w-[75px] h-[35px] border-[1px] border-[#000000] rounded-[30px] text-[#000000] hover:bg-[#000000] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                            onClick={() => setShowEditModal(false)}>
                            Cancel
                        </button>
                        <button
                            className="w-[100px] h-[35px] border-[1px] bg-[#000000] rounded-[30px] text-[#FFFFFF] hover:border-[#000000] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#000000] text-[14px] mt-[3px]"
                            type="button"
                            onClick={() => {
                                agregarColumnaDerecha(row),
                                    setShowEditModal(false)
                            }}
                        >
                            Update
                        </button>
                    </div>
                </div>
            </form>
        </div>
    )
}
import Link from "next/link";
import Image from "next/image";
import { useRouter } from "next/router";
import ImgLogo from "../public/Layout/logo_azul.png";
import ImgCar from "../public/Layout/car-2.png";
import ImgCarRepair from "../public/Layout/car-repair.png";
import { useEffect, useRef, useState } from "react";
import { useSession } from "next-auth/react";

const Layout = ({ children }) => {
  const router = useRouter();
  const { data: session } = useSession();
  const mobileMenu = useRef();
  const handleclick = () => {
    console.log("clicked");
    mobileMenu.current.classList.toggle("hidden");
  };

  var menuItems = [];

  const [currentDate, setCurrentDate] = useState(new Date().toISOString().split('T')[0])
  useEffect(() => {
    // console.log("currentDate");
    // console.log(currentDate);
    // console.log("currentDate");
  }, [currentDate]);

  // var currentDate = new Date().toISOString();
  // currentDate = currentDate.split('T')[0]

  switch (session.user.role) {
    case "ADMIN":
      menuItems = [
        // ADMIN
        {
          href: "/Admin/Dashboard",
          title: "Admin Dashboard",
          type: "home",
        },
        // APPOINTMENTS
        {
          href: "/Appointments/Dashboard",
          title: "Appointments Dashboard",
          type: "citas",
        },
        {
          href: "/calendar/[requesteddate]",
          title: "Appointments Calendar",
          type: "citas",
        },
        {
          href: "/Appointments/New",
          title: "Appointments New",
          type: "citas",
        },

        // INVENTORY
        {
          href: "/Inventory/Dashboard",
          title: "Inventory Dashboard",
          type: "inventario",
        },
        {
          href: "/Inventory/Products",
          title: "Inventory Products",
          type: "inventario",
        },
        // {
        //   href: "/Inventory/ProductDetail",
        //   title: "Inventory ProductDetail",
        //   type: "inventario",
        // },
        {
          href: "/product/[productInventory].js",
          title: "Inventory ProductDetail",
          type: "inventario",
        },
        {
          href: "/Inventory/NewSale",
          title: "Inventory NewSale",
          type: "inventario",
        },

        // ORDERS
        {
          href: "/Orders/Dashboard",
          title: "Orders Dashboard",
          type: "ordenes",
        },
        {
          href: "/Orders/AssignSRS",
          title: "Orders AssignSRS",
          type: "ordenes",
        },
        {
          href: "/Orders/ShopForeman",
          title: "Orders ShopForeman",
          type: "ordenes",
        },
        {
          href: "/Orders/History",
          title: "Orders History",
          type: "ordenes",
        },
        {
          href: "/Orders/New",
          title: "Orders New",
          type: "ordenes",
        },
        // INVOICING
        {
          href: "/Invoicing/Dashboard",
          title: "Invoicing Dashboard",
          type: "facturacion",
        },
        {
          href: "/Invoicing/NewEstimate",
          title: "Invoicing NewEstimate",
          type: "facturacion",
        },
        {
          href: "/users/",
          title: "users",
          type: "users",
        },
        {
          href: "/settings/",
          title: "settings",
          type: "settings",
        },

      ];
      break;
    case "SERVICE ADVISOR":
      menuItems = [
        {
          href: "/Orders/Dashboard",
          title: "Dashboard",
          type: "Home",
        },
        {
          href: "/Orders/AssignSRS",
          title: "Assign SRs",
          type: "Home",
        },
        {
          href: "/Orders/New",
          title: "New Order",
          type: "Home",
        },
        {
          href: "/Invoicing/NewEstimate",
          title: "New Estimate",
          type: "Home",
        },
      ];
      break;
    case "APPOINTMENTS MANAGER":
      menuItems = [
        {
          href: "/Appointments/Dashboard",
          title: "Dashboard",
          type: "Home",
        },
        {
          href: "/Appointments/Calendar",
          title: "Calendar",
          type: "Home",
        },
        {
          href: "/Appointments/New",
          title: "New",
          type: "Home",
        },
      ];
      break;
    case "SHOP FOREMAN":
      menuItems = [
        {
          href: "/Orders/Dashboard",
          title: "Dashboard",
          type: "Home",
        },
        {
          href: "/Orders/ShopForeman",
          title: "Shop Foreman",
          type: "Home",
        },
        {
          href: "/Orders/AssignSRS",
          title: "Assign SRs",
          type: "Home",
        },
      ];
      break;
    case "INVENTORY":
      menuItems = [
        {
          href: "/Inventory/Dashboard",
          title: "Dashboard",
          type: "Home",
        },
        {
          href: "/Inventory/Products",
          title: "Products",
          type: "Home",
        },
      ];
      break;
    case "INVOICING":
      menuItems = [
        {
          href: "/Invoicing/Dashboard",
          title: "Dashboard",
          type: "Home",
        },
        {
          href: "/Invoicing/NewEstimate",
          title: "New Estimate",
          type: "Home",
        },
        {
          href: "/Inventory/New Sale",
          title: "New Sale",
          type: "Home",
        },
        {
          href: "/Orders/AssignSRS",
          title: "Assign SRs",
          type: "Home",
        },
      ];
      break;
    default:
      break;
  }

  useEffect(() => {
    router.asPath == "/calendar/[requesteddate]" ? (
      router.push({
        pathname: '/calendar/' + currentDate,
      }))
      : null
  }, [router]);

  return (
    <div className="min-h-screen flex flex-col bg-[#F6F6FA]">
      <div className="h-screen grid grid-cols-12 w-full bg-[#F6F6FA]">
        <aside className="hidden md:block md:col-span-2 bg-[#F6F6FA] w-full  md:h-screen lg:flex lg:flex-col">
          <div className="flex flex-row justify-center items-center h-1/6 relative top-0 bg-[#F6F6FA]">
            <Image src={ImgLogo} layout="fixed" alt="ImgLogo" />
          </div>
          <div className="w-full h-3/4 overflow-y-scroll">
            <ul>
              {menuItems.map(({ href, title, type }) => (
                <li key={title}>
                  <Link href={href}>
                    <a
                      className={
                        router.asPath === href
                          ? "w-5/6 flex flex-row m-auto justify-center items-center rounded-lg bg-[#fff] h-14 my-2 text-[12px] text-[#3682F7] cursor-pointer"
                          : "w-5/6 flex flex-row m-auto justify-center rounded-lg items-center bg-[#F6F6FA] h-14 my-2 text-[12px] hover:bg-[#fff] text-[#000] hover:text-[#3682F7] cursor-pointer"
                      }
                    >
                      <div className="w-2/6 flex flex-row justify-end items-center">
                        {/*ADMIN*/}
                        {title == "Admin Dashboard" ? (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24"
                            fill="currentColor"
                            className="w-4 h-4"
                          >
                            <path
                              fillRule="evenodd"
                              d="M2.25 2.25a.75.75 0 000 1.5H3v10.5a3 3 0 003 3h1.21l-1.172 3.513a.75.75 0 001.424.474l.329-.987h8.418l.33.987a.75.75 0 001.422-.474l-1.17-3.513H18a3 3 0 003-3V3.75h.75a.75.75 0 000-1.5H2.25zm6.04 16.5l.5-1.5h6.42l.5 1.5H8.29zm7.46-12a.75.75 0 00-1.5 0v6a.75.75 0 001.5 0v-6zm-3 2.25a.75.75 0 00-1.5 0v3.75a.75.75 0 001.5 0V9zm-3 2.25a.75.75 0 00-1.5 0v1.5a.75.75 0 001.5 0v-1.5z"
                              clipRule="evenodd"
                            />
                          </svg>
                        ) : null}

                        {/*APPOINTMENTS*/}
                        {title == "Appointments Dashboard" ? (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24"
                            fill="currentColor"
                            className="w-4 h-4"
                          >
                            <path
                              fillRule="evenodd"
                              d="M1.5 5.625c0-1.036.84-1.875 1.875-1.875h17.25c1.035 0 1.875.84 1.875 1.875v12.75c0 1.035-.84 1.875-1.875 1.875H3.375A1.875 1.875 0 011.5 18.375V5.625zM21 9.375A.375.375 0 0020.625 9h-7.5a.375.375 0 00-.375.375v1.5c0 .207.168.375.375.375h7.5a.375.375 0 00.375-.375v-1.5zm0 3.75a.375.375 0 00-.375-.375h-7.5a.375.375 0 00-.375.375v1.5c0 .207.168.375.375.375h7.5a.375.375 0 00.375-.375v-1.5zm0 3.75a.375.375 0 00-.375-.375h-7.5a.375.375 0 00-.375.375v1.5c0 .207.168.375.375.375h7.5a.375.375 0 00.375-.375v-1.5zM10.875 18.75a.375.375 0 00.375-.375v-1.5a.375.375 0 00-.375-.375h-7.5a.375.375 0 00-.375.375v1.5c0 .207.168.375.375.375h7.5zM3.375 15h7.5a.375.375 0 00.375-.375v-1.5a.375.375 0 00-.375-.375h-7.5a.375.375 0 00-.375.375v1.5c0 .207.168.375.375.375zm0-3.75h7.5a.375.375 0 00.375-.375v-1.5A.375.375 0 0010.875 9h-7.5A.375.375 0 003 9.375v1.5c0 .207.168.375.375.375z"
                              clipRule="evenodd"
                            />
                          </svg>
                        ) : null}
                        {title == "Appointments Calendar" ? (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24"
                            fill="currentColor"
                            className="w-4 h-4"
                          >
                            <path d="M5.625 1.5c-1.036 0-1.875.84-1.875 1.875v17.25c0 1.035.84 1.875 1.875 1.875h12.75c1.035 0 1.875-.84 1.875-1.875V12.75A3.75 3.75 0 0016.5 9h-1.875a1.875 1.875 0 01-1.875-1.875V5.25A3.75 3.75 0 009 1.5H5.625z" />
                            <path d="M12.971 1.816A5.23 5.23 0 0114.25 5.25v1.875c0 .207.168.375.375.375H16.5a5.23 5.23 0 013.434 1.279 9.768 9.768 0 00-6.963-6.963z" />
                          </svg>
                        ) : null}
                        {title == "Appointments New" ? (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24"
                            fill="currentColor"
                            className="w-4 h-4"
                          >
                            <path d="M12.75 12.75a.75.75 0 11-1.5 0 .75.75 0 011.5 0zM7.5 15.75a.75.75 0 100-1.5.75.75 0 000 1.5zM8.25 17.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zM9.75 15.75a.75.75 0 100-1.5.75.75 0 000 1.5zM10.5 17.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zM12 15.75a.75.75 0 100-1.5.75.75 0 000 1.5zM12.75 17.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zM14.25 15.75a.75.75 0 100-1.5.75.75 0 000 1.5zM15 17.25a.75.75 0 11-1.5 0 .75.75 0 011.5 0zM16.5 15.75a.75.75 0 100-1.5.75.75 0 000 1.5zM15 12.75a.75.75 0 11-1.5 0 .75.75 0 011.5 0zM16.5 13.5a.75.75 0 100-1.5.75.75 0 000 1.5z" />
                            <path
                              fillRule="evenodd"
                              d="M6.75 2.25A.75.75 0 017.5 3v1.5h9V3A.75.75 0 0118 3v1.5h.75a3 3 0 013 3v11.25a3 3 0 01-3 3H5.25a3 3 0 01-3-3V7.5a3 3 0 013-3H6V3a.75.75 0 01.75-.75zm13.5 9a1.5 1.5 0 00-1.5-1.5H5.25a1.5 1.5 0 00-1.5 1.5v7.5a1.5 1.5 0 001.5 1.5h13.5a1.5 1.5 0 001.5-1.5v-7.5z"
                              clipRule="evenodd"
                            />
                          </svg>
                        ) : null}

                        {/*INVENTORY*/}
                        {title == "Inventory Dashboard" ? (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24"
                            fill="currentColor"
                            className="w-4 h-4"
                          >
                            <path d="M5.223 2.25c-.497 0-.974.198-1.325.55l-1.3 1.298A3.75 3.75 0 007.5 9.75c.627.47 1.406.75 2.25.75.844 0 1.624-.28 2.25-.75.626.47 1.406.75 2.25.75.844 0 1.623-.28 2.25-.75a3.75 3.75 0 004.902-5.652l-1.3-1.299a1.875 1.875 0 00-1.325-.549H5.223z" />
                            <path
                              fillRule="evenodd"
                              d="M3 20.25v-8.755c1.42.674 3.08.673 4.5 0A5.234 5.234 0 009.75 12c.804 0 1.568-.182 2.25-.506a5.234 5.234 0 002.25.506c.804 0 1.567-.182 2.25-.506 1.42.674 3.08.675 4.5.001v8.755h.75a.75.75 0 010 1.5H2.25a.75.75 0 010-1.5H3zm3-6a.75.75 0 01.75-.75h3a.75.75 0 01.75.75v3a.75.75 0 01-.75.75h-3a.75.75 0 01-.75-.75v-3zm8.25-.75a.75.75 0 00-.75.75v5.25c0 .414.336.75.75.75h3a.75.75 0 00.75-.75v-5.25a.75.75 0 00-.75-.75h-3z"
                              clipRule="evenodd"
                            />
                          </svg>
                        ) : null}
                        {title == "Inventory Products" ? (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24"
                            fill="currentColor"
                            className="w-4 h-4"
                          >
                            <path d="M5.223 2.25c-.497 0-.974.198-1.325.55l-1.3 1.298A3.75 3.75 0 007.5 9.75c.627.47 1.406.75 2.25.75.844 0 1.624-.28 2.25-.75.626.47 1.406.75 2.25.75.844 0 1.623-.28 2.25-.75a3.75 3.75 0 004.902-5.652l-1.3-1.299a1.875 1.875 0 00-1.325-.549H5.223z" />
                            <path
                              fillRule="evenodd"
                              d="M3 20.25v-8.755c1.42.674 3.08.673 4.5 0A5.234 5.234 0 009.75 12c.804 0 1.568-.182 2.25-.506a5.234 5.234 0 002.25.506c.804 0 1.567-.182 2.25-.506 1.42.674 3.08.675 4.5.001v8.755h.75a.75.75 0 010 1.5H2.25a.75.75 0 010-1.5H3zm3-6a.75.75 0 01.75-.75h3a.75.75 0 01.75.75v3a.75.75 0 01-.75.75h-3a.75.75 0 01-.75-.75v-3zm8.25-.75a.75.75 0 00-.75.75v5.25c0 .414.336.75.75.75h3a.75.75 0 00.75-.75v-5.25a.75.75 0 00-.75-.75h-3z"
                              clipRule="evenodd"
                            />
                          </svg>
                        ) : null}
                        {title == "Inventory ProductDetail" ? (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24"
                            fill="currentColor"
                            className="w-4 h-4"
                          >
                            <path d="M6 12a.75.75 0 01-.75-.75v-7.5a.75.75 0 111.5 0v7.5A.75.75 0 016 12zM18 12a.75.75 0 01-.75-.75v-7.5a.75.75 0 011.5 0v7.5A.75.75 0 0118 12zM6.75 20.25v-1.5a.75.75 0 00-1.5 0v1.5a.75.75 0 001.5 0zM18.75 18.75v1.5a.75.75 0 01-1.5 0v-1.5a.75.75 0 011.5 0zM12.75 5.25v-1.5a.75.75 0 00-1.5 0v1.5a.75.75 0 001.5 0zM12 21a.75.75 0 01-.75-.75v-7.5a.75.75 0 011.5 0v7.5A.75.75 0 0112 21zM3.75 15a2.25 2.25 0 104.5 0 2.25 2.25 0 00-4.5 0zM12 11.25a2.25 2.25 0 110-4.5 2.25 2.25 0 010 4.5zM15.75 15a2.25 2.25 0 104.5 0 2.25 2.25 0 00-4.5 0z" />
                          </svg>
                        ) : null}
                        {title == "Inventory NewSale" ? (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24"
                            fill="currentColor"
                            className="w-4 h-4"
                          >
                            <path d="M2.25 2.25a.75.75 0 000 1.5h1.386c.17 0 .318.114.362.278l2.558 9.592a3.752 3.752 0 00-2.806 3.63c0 .414.336.75.75.75h15.75a.75.75 0 000-1.5H5.378A2.25 2.25 0 017.5 15h11.218a.75.75 0 00.674-.421 60.358 60.358 0 002.96-7.228.75.75 0 00-.525-.965A60.864 60.864 0 005.68 4.509l-.232-.867A1.875 1.875 0 003.636 2.25H2.25zM3.75 20.25a1.5 1.5 0 113 0 1.5 1.5 0 01-3 0zM16.5 20.25a1.5 1.5 0 113 0 1.5 1.5 0 01-3 0z" />
                          </svg>
                        ) : null}

                        {/*ORDERS*/}
                        {title == "Orders Dashboard" ? (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24"
                            fill="currentColor"
                            className="w-4 h-4"
                          >
                            <path d="M11.47 3.84a.75.75 0 011.06 0l8.69 8.69a.75.75 0 101.06-1.06l-8.689-8.69a2.25 2.25 0 00-3.182 0l-8.69 8.69a.75.75 0 001.061 1.06l8.69-8.69z" />
                            <path d="M12 5.432l8.159 8.159c.03.03.06.058.091.086v6.198c0 1.035-.84 1.875-1.875 1.875H15a.75.75 0 01-.75-.75v-4.5a.75.75 0 00-.75-.75h-3a.75.75 0 00-.75.75V21a.75.75 0 01-.75.75H5.625a1.875 1.875 0 01-1.875-1.875v-6.198a2.29 2.29 0 00.091-.086L12 5.43z" />
                          </svg>
                        ) : null}
                        {title == "Orders AssignSRS" ? (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24"
                            fill="currentColor"
                            className="w-4 h-4"
                          >
                            <path
                              fillRule="evenodd"
                              d="M9 1.5H5.625c-1.036 0-1.875.84-1.875 1.875v17.25c0 1.035.84 1.875 1.875 1.875h12.75c1.035 0 1.875-.84 1.875-1.875V12.75A3.75 3.75 0 0016.5 9h-1.875a1.875 1.875 0 01-1.875-1.875V5.25A3.75 3.75 0 009 1.5zm6.61 10.936a.75.75 0 10-1.22-.872l-3.236 4.53L9.53 14.47a.75.75 0 00-1.06 1.06l2.25 2.25a.75.75 0 001.14-.094l3.75-5.25z"
                              clipRule="evenodd"
                            />
                            <path d="M12.971 1.816A5.23 5.23 0 0114.25 5.25v1.875c0 .207.168.375.375.375H16.5a5.23 5.23 0 013.434 1.279 9.768 9.768 0 00-6.963-6.963z" />
                          </svg>
                        ) : null}
                        {title == "Orders ShopForeman" ? (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24"
                            fill="currentColor"
                            className="w-4 h-4"
                          >
                            <path d="M18.375 2.25c-1.035 0-1.875.84-1.875 1.875v15.75c0 1.035.84 1.875 1.875 1.875h.75c1.035 0 1.875-.84 1.875-1.875V4.125c0-1.036-.84-1.875-1.875-1.875h-.75zM9.75 8.625c0-1.036.84-1.875 1.875-1.875h.75c1.036 0 1.875.84 1.875 1.875v11.25c0 1.035-.84 1.875-1.875 1.875h-.75a1.875 1.875 0 01-1.875-1.875V8.625zM3 13.125c0-1.036.84-1.875 1.875-1.875h.75c1.036 0 1.875.84 1.875 1.875v6.75c0 1.035-.84 1.875-1.875 1.875h-.75A1.875 1.875 0 013 19.875v-6.75z" />
                          </svg>
                        ) : null}
                        {title == "Orders History" ? (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24"
                            fill="currentColor"
                            className="w-4 h-4"
                          >
                            <path
                              fillRule="evenodd"
                              d="M12 2.25c-5.385 0-9.75 4.365-9.75 9.75s4.365 9.75 9.75 9.75 9.75-4.365 9.75-9.75S17.385 2.25 12 2.25zM12.75 6a.75.75 0 00-1.5 0v6c0 .414.336.75.75.75h4.5a.75.75 0 000-1.5h-3.75V6z"
                              clipRule="evenodd"
                            />
                          </svg>
                        ) : null}
                        {title == "Orders New" ? (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24"
                            fill="currentColor"
                            className="w-4 h-4"
                          >
                            <path d="M11.47 3.84a.75.75 0 011.06 0l8.69 8.69a.75.75 0 101.06-1.06l-8.689-8.69a2.25 2.25 0 00-3.182 0l-8.69 8.69a.75.75 0 001.061 1.06l8.69-8.69z" />
                            <path d="M12 5.432l8.159 8.159c.03.03.06.058.091.086v6.198c0 1.035-.84 1.875-1.875 1.875H15a.75.75 0 01-.75-.75v-4.5a.75.75 0 00-.75-.75h-3a.75.75 0 00-.75.75V21a.75.75 0 01-.75.75H5.625a1.875 1.875 0 01-1.875-1.875v-6.198a2.29 2.29 0 00.091-.086L12 5.43z" />
                          </svg>
                        ) : null}

                        {/*INVOICING*/}
                        {title == "Invoicing Dashboard" ? (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24"
                            fill="currentColor"
                            className="w-4 h-4"
                          >
                            <path
                              fillRule="evenodd"
                              d="M4.125 3C3.089 3 2.25 3.84 2.25 4.875V18a3 3 0 003 3h15a3 3 0 01-3-3V4.875C17.25 3.839 16.41 3 15.375 3H4.125zM12 9.75a.75.75 0 000 1.5h1.5a.75.75 0 000-1.5H12zm-.75-2.25a.75.75 0 01.75-.75h1.5a.75.75 0 010 1.5H12a.75.75 0 01-.75-.75zM6 12.75a.75.75 0 000 1.5h7.5a.75.75 0 000-1.5H6zm-.75 3.75a.75.75 0 01.75-.75h7.5a.75.75 0 010 1.5H6a.75.75 0 01-.75-.75zM6 6.75a.75.75 0 00-.75.75v3c0 .414.336.75.75.75h3a.75.75 0 00.75-.75v-3A.75.75 0 009 6.75H6z"
                              clipRule="evenodd"
                            />
                            <path d="M18.75 6.75h1.875c.621 0 1.125.504 1.125 1.125V18a1.5 1.5 0 01-3 0V6.75z" />
                          </svg>
                        ) : null}
                        {title == "Invoicing NewEstimate" ? (
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                            className="w-4 h-4"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              d="M2.25 8.25h19.5M2.25 9h19.5m-16.5 5.25h6m-6 2.25h3m-3.75 3h15a2.25 2.25 0 002.25-2.25V6.75A2.25 2.25 0 0019.5 4.5h-15a2.25 2.25 0 00-2.25 2.25v10.5A2.25 2.25 0 004.5 19.5z"
                            />
                          </svg>
                        ) : null}
                        {title == "users" ? (
                          <svg 
                          xmlns="http://www.w3.org/2000/svg" 
                          fill="none" viewBox="0 0 24 24" 
                          stroke-width="1.5" 
                          stroke="currentColor" 
                          className="w-4 h-4">
                          <path stroke-linecap="round" stroke-linejoin="round" d="M15 19.128a9.38 9.38 0 002.625.372 9.337 9.337 0 004.121-.952 4.125 4.125 0 00-7.533-2.493M15 19.128v-.003c0-1.113-.285-2.16-.786-3.07M15 19.128v.106A12.318 12.318 0 018.624 21c-2.331 0-4.512-.645-6.374-1.766l-.001-.109a6.375 6.375 0 0111.964-3.07M12 6.375a3.375 3.375 0 11-6.75 0 3.375 3.375 0 016.75 0zm8.25 2.25a2.625 2.625 0 11-5.25 0 2.625 2.625 0 015.25 0z" />
                        </svg>
                        
                        ) : null}
                        {title == "settings" ? (
                           <svg
                           xmlns="http://www.w3.org/2000/svg"
                           viewBox="0 0 24 24"
                           fill="currentColor"
                           className="w-4 h-4"
                         >
                           <path
                             fillRule="evenodd"
                             d="M11.078 2.25c-.917 0-1.699.663-1.85 1.567L9.05 4.889c-.02.12-.115.26-.297.348a7.493 7.493 0 00-.986.57c-.166.115-.334.126-.45.083L6.3 5.508a1.875 1.875 0 00-2.282.819l-.922 1.597a1.875 1.875 0 00.432 2.385l.84.692c.095.078.17.229.154.43a7.598 7.598 0 000 1.139c.015.2-.059.352-.153.43l-.841.692a1.875 1.875 0 00-.432 2.385l.922 1.597a1.875 1.875 0 002.282.818l1.019-.382c.115-.043.283-.031.45.082.312.214.641.405.985.57.182.088.277.228.297.35l.178 1.071c.151.904.933 1.567 1.85 1.567h1.844c.916 0 1.699-.663 1.85-1.567l.178-1.072c.02-.12.114-.26.297-.349.344-.165.673-.356.985-.57.167-.114.335-.125.45-.082l1.02.382a1.875 1.875 0 002.28-.819l.923-1.597a1.875 1.875 0 00-.432-2.385l-.84-.692c-.095-.078-.17-.229-.154-.43a7.614 7.614 0 000-1.139c-.016-.2.059-.352.153-.43l.84-.692c.708-.582.891-1.59.433-2.385l-.922-1.597a1.875 1.875 0 00-2.282-.818l-1.02.382c-.114.043-.282.031-.449-.083a7.49 7.49 0 00-.985-.57c-.183-.087-.277-.227-.297-.348l-.179-1.072a1.875 1.875 0 00-1.85-1.567h-1.843zM12 15.75a3.75 3.75 0 100-7.5 3.75 3.75 0 000 7.5z"
                             clipRule="evenodd"
                           />
                         </svg>
                        
                        ) : null}

                      </div>
                      <div className="w-4/6 flex flex-row justify-start items-center mx-2">
                        <p>{title}</p>
                      </div>
                    </a>
                  </Link>
                </li>
              ))}
            </ul>
          </div>

          <div className="w-full h-1/4 flex flex-col justify-end items-center">
            <div className="lg:w-[80%] h-1/5 flex flex-row justify-start items-center mb-2">
              <div className="bg-[#E4E7EB] w-8 h-8 rounded-full"></div>
            </div>
            <div className="w-full h-2/5 flex flex-col justify-center items-center">
              <div className="flex h-1/2 flex-row w-full my-2">
                <div className="w-2/6 flex flex-row justify-end items-center">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 24 24"
                    fill="currentColor"
                    className="w-4 h-4"
                  >
                    <path
                      fillRule="evenodd"
                      d="M11.078 2.25c-.917 0-1.699.663-1.85 1.567L9.05 4.889c-.02.12-.115.26-.297.348a7.493 7.493 0 00-.986.57c-.166.115-.334.126-.45.083L6.3 5.508a1.875 1.875 0 00-2.282.819l-.922 1.597a1.875 1.875 0 00.432 2.385l.84.692c.095.078.17.229.154.43a7.598 7.598 0 000 1.139c.015.2-.059.352-.153.43l-.841.692a1.875 1.875 0 00-.432 2.385l.922 1.597a1.875 1.875 0 002.282.818l1.019-.382c.115-.043.283-.031.45.082.312.214.641.405.985.57.182.088.277.228.297.35l.178 1.071c.151.904.933 1.567 1.85 1.567h1.844c.916 0 1.699-.663 1.85-1.567l.178-1.072c.02-.12.114-.26.297-.349.344-.165.673-.356.985-.57.167-.114.335-.125.45-.082l1.02.382a1.875 1.875 0 002.28-.819l.923-1.597a1.875 1.875 0 00-.432-2.385l-.84-.692c-.095-.078-.17-.229-.154-.43a7.614 7.614 0 000-1.139c-.016-.2.059-.352.153-.43l.84-.692c.708-.582.891-1.59.433-2.385l-.922-1.597a1.875 1.875 0 00-2.282-.818l-1.02.382c-.114.043-.282.031-.449-.083a7.49 7.49 0 00-.985-.57c-.183-.087-.277-.227-.297-.348l-.179-1.072a1.875 1.875 0 00-1.85-1.567h-1.843zM12 15.75a3.75 3.75 0 100-7.5 3.75 3.75 0 000 7.5z"
                      clipRule="evenodd"
                    />
                  </svg>
                </div>
                <div className="w-4/6 flex flex-row justify-start items-center mx-2">
                  <p className="text-sm">Settings</p>
                </div>
              </div>
              <div className="flex flex-row w-full my-2">
                <div className="w-2/6 h-1/2 flex flex-row justify-end items-center">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 24 24"
                    fill="currentColor"
                    className="w-4 h-4"
                  >
                    <path
                      fillRule="evenodd"
                      d="M5.337 21.718a6.707 6.707 0 01-.533-.074.75.75 0 01-.44-1.223 3.73 3.73 0 00.814-1.686c.023-.115-.022-.317-.254-.543C3.274 16.587 2.25 14.41 2.25 12c0-5.03 4.428-9 9.75-9s9.75 3.97 9.75 9c0 5.03-4.428 9-9.75 9-.833 0-1.643-.097-2.417-.279a6.721 6.721 0 01-4.246.997z"
                      clipRule="evenodd"
                    />
                  </svg>
                </div>
                <div className="w-4/6 flex flex-row justify-start items-center mx-2">
                  <p className="text-sm">Support</p>
                </div>
              </div>
            </div>
            <div className="w-full h-1/5 flex flex-row justify-center items-center mb-1">
              <p className="text-sm text-center">Copyright @Vulcano 2022</p>
            </div>
          </div>
        </aside>
        <div className="md:hidden col-span-12 flex justify-between p-2 pt-4 pb-4">
          <Image src={ImgLogo} layout="fixed" alt="ImgLogo" />
          <div className="flex items-center">
            <button
              className="outline-none mobile-menu-button"
              onClick={handleclick}
            >
              <svg
                className=" w-6 h-6 text-gray-500 hover:text-blue-500 "
                // x-show="!showMenu"
                fill="none"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path d="M4 6h16M4 12h16M4 18h16"></path>
              </svg>
            </button>
          </div>
        </div>
        <div
          className="hidden md:hidden col-span-12 mobile-menu"
          ref={mobileMenu}
        >
          <ul>
            {menuItems.map(({ href, title, type }) => (
              <li
                key={title}
                className={
                  router.asPath === href &&
                  "bg-[#3682F7] text-[#FFFF] hover:bg-[#3682F7] hover:text-[#FFFFFF]"
                }
              >
                <Link href={href}>
                  <a className={"w-full mt-2 mb-2 ml-4 text-[12px] text-right"}>
                    {title}
                  </a>
                </Link>
              </li>
            ))}
          </ul>
        </div>
        <main className="col-span-12 md:col-span-10 ">{children}</main>
      </div>
    </div>
  );
};

export default Layout;

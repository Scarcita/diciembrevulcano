import React, { useEffect, useState, useRef } from "react";
import Image from "next/image";

import { Bounce, Sentry } from "react-activity";
import "react-activity/dist/library.css";

//import imagenOpciones from "../../../public/opciones.svg";

import CreatableSelect from "react-select/creatable";
import { setLocale } from "yup";

function TableCompanyInf(props) {
  const { setDatosCliente } = props;

  const selectRef = useRef();

  const [selectOptions, setSelectOptions] = useState("");

  const [isLocked, setIsLocked] = useState(false);
  const [userDataLoading, setUserDataLoading] = useState(false);
  const [selectLoading, setSelectLoading] = useState(false);
  const [newClientLoading, setNewClientLoading] = useState(false);

  const [sendingData, setSendingData] = useState(false);
  const [doneMessage, setDoneMessage] = useState(false);
  const [reload, setReload] = useState(false);

  const [selectedClientId, setSelectedClientId] = useState(0);
  const [selectId, setSelectId] = useState(null);

  const [companyName, setCompanyName] = useState("");
  const [address, setAdress] = useState("");
  const [zipNumber, setZipNumber] = useState("");
  const [phone, setPhone] = useState("");
  const [email, setEmail] = useState("");
  const [licenseNumber, setLicenseNumber] = useState("");
  const [salesTax, setSalestax] = useState("");



  useEffect(() => {
    getDatos();
  }, [reload]);

  useEffect(() => {
    getDatos();
  }, []);

  const getDatos = () => {
    setSelectLoading(true);
    fetch("https://slogan.com.bo/vulcano/clients/allBy/name")
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          const dataKeys = Object.keys(data.data);

          let indexedData = [];

          for (let i = 0; i < dataKeys.length; i++) {
            let index = dataKeys[i];
            if (data.data[index] !== null) {
              indexedData.push({
                value: index,
                label: data.data[index],
              });
            }
          }
          setSelectOptions(indexedData);
        } else {
          console.error("getDatos: " + data.error);
        }
        setSelectLoading(false);
      });
  };

  const handleOnSelect = (id) => {
    setIsLocked(true);
    setUserDataLoading(true);
    fetch("https://slogan.com.bo/vulcano/clients/getById/" + id)
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          setIsLocked(false);
          setClientName(data.data.name);
          setTelefono(data.data.phone);
          setEmail(data.data.email);
          setCiNit(data.data.invoice_cinit);
          setNombre(data.data.invoice_name);

          setSelectedClientId(data.data.id);

          let tempSelectOptions = [
            {
              value: data.data.id,
              label: data.data.name,
            },
          ];
          for (let i = 0; i < selectOptions.length; i++) {
            if (selectOptions[i].label !== data.data.name) {
              tempSelectOptions.push(selectOptions[i]);
            }
          }
          setSelectOptions(tempSelectOptions);
          setSelectId(0);

          setUserDataLoading(false);
          setIsLocked(true);
        } else {
          console.error("HandleOnSelectUser: " + data.error);
        }
      });
  };

  const handleOnCreate = (item) => {
    setNewClientLoading(true);
    setSelectedClientId(0);
    let tempSelectOptions = [
      {
        value: 0,
        label: item,
      },
    ];
    for (let i = 0; i < selectOptions.length; i++) {
      tempSelectOptions.push(selectOptions[i]);
    }
    setSelectOptions(tempSelectOptions);
    setSelectId(0);

    setClientName(item);
    setTimeout(() => {
      setNewClientLoading(false);
    }, 500);
  };

  const addNewClient = () => {
    if (
      // show red button
      clientName === "" ||
      telefono === "" ||
      email === "" ||
      ciNit === "" ||
      nombre === ""
    ) {
      return;
    }
    setSendingData(true);
    const data = new FormData();
    data.append("name", clientName);
    data.append("phone", telefono);
    data.append("email", email);
    data.append("invoice_cinit", ciNit);
    data.append("invoice_name", nombre);

    fetch("https://slogan.com.bo/vulcano/clients/addMobile", {
      method: "POST",
      body: data,
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          console.log("Cliente agregado con exito");
          console.log("New client: " + Object.entries(data.data));

          setReload(!reload);
          handleOnSelect(data.data.id);

          setSendingData(false);
          setDoneMessage(true);

          setTimeout(() => {
            setDoneMessage(false);
          }, 2000);
        } else {
          console.error("addNewClient: " + data.error);
        }
      });
  };

  const editClientData = () => {
    if (
      clientName === "" ||
      telefono === "" ||
      email === "" ||
      ciNit === "" ||
      nombre === ""
    ) {
      return;
    }
    setSendingData(true);
    const data = new FormData();
    data.append("name", clientName);
    data.append("phone", telefono);
    data.append("email", email);
    data.append("invoice_cinit", ciNit);
    data.append("invoice_name", nombre);

    fetch(
      "https://slogan.com.bo/vulcano/clients/editMobile/" + selectedClientId,
      {
        method: "POST",
        body: data,
      }
    )
      .then((response) => response.json())
      .then((data) => {
        console.log("editClientData Data: " + data.status);
        if (data.status) {
          console.log("Cliente editado con exito");
          console.log("Updated Client: " + Object.entries(data.data));

          handleOnSelect(selectedClientId);

          setSendingData(false);
          setDoneMessage(true);

          setTimeout(() => {
            setDoneMessage(false);
          }, 2000);
        } else {
          console.error("editClientData: " + data.error);
        }
      });
  };

  const formatResult = (item) => {
    return (
      <div className="flex flex-row ">
        <p className="ml-2">{item.name}</p>
      </div>
    );
  };

  return (
    // COTENERDOR PRINCIPAL
    <form className="grid grid-cols-12">
        <div className="col-span-12 md:col-span-12 lg:col-span-12 justify-center items-center px-4 bg-[#fff] rounded-lg shadow-lg rounded-lg"
        >
          <div className="grid grid-cols-12">
            <div className="col-span-12 md:col-span-12 lg:col-span-12">
              <div className="flex justify-between">
                <div>
                  <p className="font-medium text-lg"> Detalles de registro</p>
                </div>
                <div>
                  <div
                    className="w-[19px] h-[18px] m-2"
                    onClick={() => {
                      setIsLocked(!isLocked);
                    }}
                  >
                    {isLocked == false ? (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                        fill="currentColor"
                        className="
                      text-[#D3D3D3] w-[19px] h-[18px] cursor-pointer
                      {/*HOVER*/}
                      hover:text-[#000]
                      "
                      >
                        <path d="M18 1.5c2.9 0 5.25 2.35 5.25 5.25v3.75a.75.75 0 01-1.5 0V6.75a3.75 3.75 0 10-7.5 0v3a3 3 0 013 3v6.75a3 3 0 01-3 3H3.75a3 3 0 01-3-3v-6.75a3 3 0 013-3h9v-3c0-2.9 2.35-5.25 5.25-5.25z" />
                      </svg>
                    ) : (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                        fill="currentColor"
                        className="
                      text-[#D3D3D3] w-[19px] h-[18px] cursor-pointer
                      {/*HOVER*/}
                      hover:text-[#000] 
                      "
                      >
                        <path
                          fillRule="evenodd"
                          d="M12 1.5a5.25 5.25 0 00-5.25 5.25v3a3 3 0 00-3 3v6.75a3 3 0 003 3h10.5a3 3 0 003-3v-6.75a3 3 0 00-3-3v-3c0-2.9-2.35-5.25-5.25-5.25zm3.75 8.25v-3a3.75 3.75 0 10-7.5 0v3h7.5z"
                          clipRule="evenodd"
                        />
                      </svg>
                    )}
                  </div>
                  <div className="w-[5px] h-[19px] m-2">
                    <Image

                      layout="responsive"
                      alt="imagenOpciones"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="grid grid-cols-12 gap-4">
            <div className="col-span-12 md:col-span-12 lg:col-span-12">
              <div className="">
                <label className=" w-full self-start text-s m text-[#A5A5A5] font-light">
                  Company Name
                </label>
                <div className="w-full h-10 flex flex-col justify-center items-center border-[#D3D3D3] border-2 rounded-lg">
                  {userDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <input className=" w-full h-10 text-sm text-left font-normal rounded-full px-2 outline-none"
                      type="text"
                      disabled={isLocked}
                      placeholder="Descripcion"
                      name="address"
                      // {...register("telefono")}
                      value={address}
                      onChange={(e) => {
                        setAdress(e.target.value);
                      }}
                    />
                  )}
                </div>
              </div>

            </div>
            <div className="col-span-12 md:col-span-6 lg:col-span-4">
              <div className="">
                <label className=" w-full self-start text-s m text-[#A5A5A5] font-light">
                  Address
                </label>
                <div className="w-full h-10 flex flex-col justify-center items-center border-[#D3D3D3] border-2 rounded-lg">
                  {userDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <input className=" w-full h-10 text-sm text-left font-normal rounded-full px-2 outline-none"
                      type="text"
                      disabled={isLocked}
                      placeholder="Descripcion"
                      name="address"
                      // {...register("telefono")}
                      value={address}
                      onChange={(e) => {
                        setAdress(e.target.value);
                      }}
                    />
                  )}
                </div>
              </div>

            </div>
            <div className="col-span-12 md:col-span-6 lg:col-span-4">
              <div className="">
                <label className=" w-full self-start text-s m text-[#A5A5A5] font-light">
                  Zip Number
                </label>
                <div className="w-full h-10 flex flex-col justify-center items-center border-[#D3D3D3] border-2 rounded-lg">
                  {userDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <input className=" w-full h-10 text-sm text-left font-normal rounded-full px-2 outline-none"
                      type="text"
                      disabled={isLocked}
                      placeholder="Descripcion"
                      name="address"
                      // {...register("telefono")}
                      value={address}
                      onChange={(e) => {
                        setAdress(e.target.value);
                      }}
                    />
                  )}
                </div>
              </div>

            </div>
            <div className="col-span-12 md:col-span-6 lg:col-span-4">
              <div className="">
                <label className=" w-full self-start text-s m text-[#A5A5A5] font-light">
                  Phone
                </label>
                <div className="w-full h-10 flex flex-col justify-center items-center border-[#D3D3D3] border-2 rounded-lg">
                  {userDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <input className=" w-full h-10 text-sm text-left font-normal rounded-full px-2 outline-none"
                      type="number"
                      disabled={isLocked}
                      placeholder="Descripcion"
                      name="address"
                      // {...register("telefono")}
                      value={address}
                      onChange={(e) => {
                        setAdress(e.target.value);
                      }}
                    />
                  )}
                </div>
              </div>

            </div>
            <div className="col-span-12 md:col-span-6 lg:col-span-4">
              <div className="">
                <label className=" w-full self-start text-s m text-[#A5A5A5] font-light">
                  Email
                </label>
                <div className="w-full h-10 flex flex-col justify-center items-center border-[#D3D3D3] border-2 rounded-lg">
                  {userDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <input className=" w-full h-10 text-sm text-left font-normal rounded-full px-2 outline-none"
                      type="email"
                      disabled={isLocked}
                      placeholder="Descripcion"
                      name="address"
                      // {...register("telefono")}
                      value={address}
                      onChange={(e) => {
                        setAdress(e.target.value);
                      }}
                    />
                  )}
                </div>
              </div>

            </div>
            <div className="col-span-12 md:col-span-6 lg:col-span-4">
              <div className="">
                <label className=" w-full self-start text-s m text-[#A5A5A5] font-light">
                  License Number
                </label>
                <div className="w-full h-10 flex flex-col justify-center items-center border-[#D3D3D3] border-2 rounded-lg">
                  {userDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <input className=" w-full h-10 text-sm text-left font-normal rounded-full px-2 outline-none"
                      type="text"
                      disabled={isLocked}
                      placeholder="Descripcion"
                      name="address"
                      // {...register("telefono")}
                      value={address}
                      onChange={(e) => {
                        setAdress(e.target.value);
                      }}
                    />
                  )}
                </div>
              </div>

            </div>
            <div className="col-span-12 md:col-span-6 lg:col-span-4">
              <div className="">
                <label className=" w-full self-start text-s m text-[#A5A5A5] font-light">
                  Sales Tax
                </label>
                <div className="w-full h-10 flex flex-col justify-center items-center border-[#D3D3D3] border-2 rounded-lg">
                  {userDataLoading == true ? (
                    <Bounce
                      size={20}
                      color="#D3D3D3"
                      speed={1}
                      animating={true}
                      style={{
                        marginBottom: "0.1rem",
                      }}
                    />
                  ) : (
                    <input className=" w-full h-10 text-sm text-left font-normal rounded-full px-2 outline-none"
                      type="text"
                      disabled={isLocked}
                      placeholder="Descripcion"
                      name="address"
                      // {...register("telefono")}
                      value={address}
                      onChange={(e) => {
                        setAdress(e.target.value);
                      }}
                    />
                  )}
                </div>
              </div>

            </div>
            
          </div>
          

          {/* BOTONES */}
          <div
            className="
            w-full h-14 flex flex-row justify-end items-center mb-2
          "
          >
            <button
              type="button"
              className={`
              w-24 h-10 border-2 rounded-lg text-[#fff] text-center text-[14px] font-Dosis ml-2 outline-none bg-[#000] border-[#000] hover:bg-[#fff] hover:text-[#000]
              `}
              onClick={() => {
                selectRef.current.clearValue();
                setSelectedClientId(0);
                setClientName("");
                setTelefono("");
                setEmail("");
                setCiNit("");
                setNombre("");
                setIsLocked(false);
              }}
            >
              Cancelar
            </button>

            <button
              type="button"
              className={`
              w-24 h-10 border-2 rounded-lg text-[#fff] text-center text-[14px] font-Dosis ml-2 outline-none ${
                isLocked
                  ? "text-[#D9D9D9]"
                  : "bg-[#3682F7] border-[#3682F7] hover:bg-[#fff] hover:text-[#3682F7]"
              }
              `}
              disabled={isLocked}
              onClick={() => {
                selectedClientId !== 0 ? editClientData() : addNewClient();
              }}
            >
              {selectedClientId !== 0 ? "Editar" : "Guardar"}
            </button>
          </div>
        </div>
    </form>
  );
}
export default TableCompanyInf;

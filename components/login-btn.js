import { useSession, signIn, signOut } from "next-auth/react"
import Link from "next/link";

export default function LoginCheckerButton() {
  const { data: session } = useSession()
  if (session) {
    return (
      <>
        <div  className="grid grid-cols-12 ">
          <div className="col-span-12 md:col-span-12 lg:col-span-12">
            <div>
              Signed in as {session.user.name} <br />
            </div>
            <div>
              Role: {session.user.role} <br />
            </div>
            <div className="flex mt-[20px] ">
              <div className="w-[100px]  h-[40px] bg-[#3682F7] flex justify-center items-center text-[#fff] text-[14px] mr-[20px] rounded-lg hover:border-[#3682F7] hover:border hover:text-[#3682F7] hover:bg-transparent" >
                <Link href='/Admin/Dashboard'>
                  <p>Go To Dashboard</p>
                </Link>
              </div>
              <button
                className="w-[100px] h-[40px] bg-[#3682F7] text-[#fff] text-[14px] rounded-lg hover:border-[#3682F7] hover:border hover:text-[#3682F7] hover:bg-transparent" 
                onClick={() => signOut()}>
                Sign out
              </button>
            </div>
          </div>
        </div>
      </>
    )
  }
  return (
    <>
      Not signed in <br />
      <button
        className="w-[100px] h-[48px] bg-[#3682F7] text-[#fff] rounded-lg items-center mx-[10px] mt-[10px] hover:border-[#3682F7] hover:border hover:text-[#3682F7] hover:bg-[#fff]" 
        onClick={() => signIn()}>
        Sign in
      </button>
    </>
  )
}
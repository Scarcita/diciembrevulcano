import Spinner from "react-activity/dist/Spinner";
import "react-activity/dist/Spinner.css";
import React, { useEffect, useState } from "react";
import Link from "next/link";

import Modal from "./modal";
// import Modal from '../pantallaProductos/Modal';

export default function Datos() {
  const [showDots, setShowDots] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    fetch("https://slogan.com.bo/vulcano/appointments/today")
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          console.log(data.data);
          setData(data.data);
        } else {
          console.error(data.error);
        }
      })
      .then(
        setTimeout(function () {
          setShowDots(false);
        }, 2000)
      );
    // .then(setShowDots(false))
  }, []);

  return (
    <div className="flex justify-center items-center">
      {/* <table> */}
      <table className={`flex justify-center items-center`}>
        <tbody className="flex justify-center items-center">
          {data.map((row) => (
            <TableRow showDots={showDots} key={data.id} row={row} />
          ))}
        </tbody>
      </table>
    </div>
  );
}

class TableRow extends React.Component {
  render() {
    let row = this.props.row;
    console.log(row);

    let spliteado = row.created.split("T");
    let fecha = spliteado[0];
    let hora = spliteado[1].split("+")[0];
    const { showDots } = this.props;

    return showDots ? (
      <div className="flex justify-center items-center">
        <Spinner
          color="#3682F7"
          size={17}
          speed={1}
          animating={true}
          style={{ marginLeft: "auto", marginRight: "auto" }}
        />
      </div>
    ) : (
      <div className="flex flex-col justify-center">
        <div className="flex grid grid-cols-12 ">
          <div className="flex col-span-10 md:col-span-10 lg:col-span-10 ">
            <p className="font-bold lg:text-[25.88px] md:text-[25.88px] text-[22px] mt-3 ">
              {row.client.name}
            </p>
          </div>
          <div className="flex col-span-2 md:col-span-2 lg:col-span-2 grid justify-items-end">
            {/* <button className='h-[9px] lg:mt-[18px] md:mt-[18px] mt-[11px] '> */}
            <div className="h-[9px] lg:mt-[18px] md:mt-[18px] mt-[11px] ">
              <Modal showDots={showDots} row={row} />
            </div>
          </div>
        </div>

        <div className="flex lg:mt-[5px] md:mt-[5px] mt-[10px]">
          <div className="bg-[#3682F7] lg:w-[80px] md:w-[80px] w-[75px] h-[25px] rounded-2xl flex justify-center items-center ">
            <p className="text-[#FFFF] lg:text-[12.83px] md:text-[12.83px] text-[12px] font-extralight">
              {row.status}
            </p>
          </div>
          <p className="lg:text-[17px] md:text-[17px] lg:ml-[28px] md:ml-[28px] ml-[12px] lg:mt-[-3px] md:mt-[-3px]">
            ENTREGA: {fecha} {hora}
          </p>
        </div>
        <div className="lg:flex md:flex lg:mt-1 md:mt-1 justify-center items-center mb-8">
          <div className="lg:flex-col md:flex-col justify-center items-center">
            <p className="font-semibold text-[18px] lg:mt-[2px] md:mt-[2px] mt-[10px]">
              Contacto
            </p>
          </div>
          <div className="lg:flex md:flex justify-center items-center">
            <div className="flex lg:ml-[70px] md:ml-[70px] ml-[1px]">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth="1.5"
                stroke="currentColor"
                className="w-[15px] h-[15px] text-[#D9D9D9] mt-[7px]"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M15.75 6a3.75 3.75 0 11-7.5 0 3.75 3.75 0 017.5 0zM4.501 20.118a7.5 7.5 0 0114.998 0A17.933 17.933 0 0112 21.75c-2.676 0-5.216-.584-7.499-1.632z"
                />
              </svg>
              <button className="ml-2">
                <Link href="http://www.wantpublicidad.com/">
                  <p className="text-[#D9D9D9] lg:text-[14px] md:text-[14px]">
                    {row.client.email}
                  </p>
                </Link>
              </button>
            </div>
            <div className="flex lg:ml-[70px] md:ml-[70px] ml-[1px]">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth="1.5"
                stroke="currentColor"
                className="w-[15px] h-[15px] text-[#D9D9D9] mt-[6px]"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M2.25 6.75c0 8.284 6.716 15 15 15h2.25a2.25 2.25 0 002.25-2.25v-1.372c0-.516-.351-.966-.852-1.091l-4.423-1.106c-.44-.11-.902.055-1.173.417l-.97 1.293c-.282.376-.769.542-1.21.38a12.035 12.035 0 01-7.143-7.143c-.162-.441.004-.928.38-1.21l1.293-.97c.363-.271.527-.734.417-1.173L6.963 3.102a1.125 1.125 0 00-1.091-.852H4.5A2.25 2.25 0 002.25 4.5v2.25z"
                />
              </svg>
              <p className="text-[#D9D9D9] mt-[3px] ml-[7px] lg:text-[14px] md:text-[14px]">
                (1) 305 7777 7777
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

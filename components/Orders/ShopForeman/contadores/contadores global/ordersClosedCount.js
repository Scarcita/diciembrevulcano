import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';
import React, { useEffect, useState } from 'react';


export default function OrdersClosedCount() {
    const [isLoading, setIsLoading] = useState(true)
    const [counterOrdersFinalized, setCounterOrdersFinalized] = useState([]);
    console.log(counterOrdersFinalized);

    useEffect(() => {

        setIsLoading(true)

        fetch('https://slogan.com.bo/vulcano/orders/shopForemanDashboard')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    var temp = [];
                    Object.values(data.data).map((result) =>{
                        temp.push(result);
                    })
    
                    setCounterOrdersFinalized(temp[5])
                    
                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

    }, [])

    return (

    isLoading ?
            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                <Spinner color="#3682F7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
            <div>{counterOrdersFinalized}</div>
    )
    }
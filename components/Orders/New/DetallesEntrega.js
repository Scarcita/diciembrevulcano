import React, { useEffect, useState } from "react";

function DetallesEntrega(props) {
  const { detallesEntrega, setDetallesEntrega } = props;

  return (
    // CONTENEDOR PRINCIPAL
    <div
      className="
    w-full h-full flex flex-col justify-around items-center
    "
    >
      {/* CABECERA */}
      <div
        className="
        w-full h-14 flex flex-row justify-start items-center
      "
      >
        <p
          className="
          text-lg font-bold
        "
        >
          Promesa de entrega
        </p>
      </div>

      {/* CUERPO */}
      <div
        className="
        w-full h-fit flex flex-col justify-center items-center p-4 bg-[#fff] rounded-lg shadow-lg rounded-lg
        {/*DESKTOP*/}
        lg:h-full lg:flex-row
      "
      >
        {/* FECHA */}
        <div
          className="
            w-full flex flex-col justify-center items-center
            "
        >
          <label
            className="
                w-full self-start text-s m text-[#A5A5A5] font-light mb-2
                "
          >
            Fecha en que se entrega el vehiculo
          </label>

          <div
            className="
                w-full h-10 flex flex-col justify-center items-center
                "
          >
            <input
              className="
                    w-full h-10 text-md text-center font-normal border-[#D3D3D3] border-2 rounded-lg pt-1 outline-none
                    "
              type="datetime-local"
              value={detallesEntrega}
              onChange={(e) => {
                setDetallesEntrega(e.target.value);
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
export default DetallesEntrega;

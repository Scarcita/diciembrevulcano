import React, { useEffect, useState } from "react";

function Kilometraje(props) {
  const { kilometraje, setKilometraje } = props;

  return (
    // CONTENEDOR PRINCIPAL
    <div
      className="
    w-full h-full flex flex-col justify-around items-center
    "
    >
      {/* CABECERA */}
      <div
        className="
        w-full h-14 flex flex-row justify-start items-center
      "
      >
        <p
          className="
          text-lg font-bold
        "
        >
          Kilometraje
        </p>
      </div>

      {/* CUERPO */}
      <div
        className="
        w-full h-fit flex flex-col justify-center items-center p-4 bg-[#fff] rounded-lg shadow-lg rounded-lg
        {/*DESKTOP*/}
        lg:h-full lg:flex-row
      "
      >
        {/* KILOMETRAJE */}
        <div
          className="
            w-full h-full flex flex-col justify-center items-center
            "
        >
          <label
            className="
                w-full self-start text-s m text-[#A5A5A5] font-light mb-2
                "
          >
            Kilometraje al momento del ingreso
          </label>

          <div
            className="
                w-full h-10 flex flex-col justify-center items-center
                "
          >
            <input
              className="
              w-full h-10 text-sm text-left font-normal px-2 outline-none border-[#D3D3D3] border-2 rounded-lg
              "
              type="number"
              placeholder="Ingrese el kilometraje"
              value={kilometraje}
              onChange={(e) => {
                setKilometraje(e.target.value);
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
export default Kilometraje;



function BarraAzul() {

    const Estado = (props) => {

        const { estado } = props;

        console.log(estado);

        return (
            <div
                className="ml-[13vw]"
            >
                {estado == "ABIERTO" ?
                    <div
                        className=" flex justify-center mt-[-7px]"
                    >
                        <BarraEstado name="ABIERTO" status={true}></BarraEstado>
                        <div className="bg-[#BBBABA] w-[7vw] h-[7px] mt-[8px] z-20" />
                        <BarraEstado name="FINALIZADO" status={false}></BarraEstado>
                        <div className="bg-[#BBBABA] w-[7vw] h-[7px] mt-[8px] z-20" />
                        <BarraEstado name="CERRADO" status={false}></BarraEstado>
                        <div className="bg-[#BBBABA] w-[7vw] h-[7px] mt-[8px] z-20" />
                        <BarraEstado name="ENTREGADO" status={false}></BarraEstado>
                    </div>
                    : <></>
                }
                {estado == "FINALIZADO" ?
                    <div
                        className=" flex justify-center mt-[-7px]"
                    >
                        <BarraEstado name="ABIERTO" status={true}></BarraEstado>
                        <div className="bg-[#3682F7] w-[7vw] h-[7px] mt-[8px] z-20" />
                        <BarraEstado name="FINALIZADO" status={true}></BarraEstado>
                        <div className="bg-[#BBBABA] w-[7vw] h-[7px] mt-[8px] z-20" />
                        <BarraEstado name="CERRADO" status={false}></BarraEstado>
                        <div className="bg-[#BBBABA] w-[7vw] h-[7px] mt-[8px] z-20" />
                        <BarraEstado name="ENTREGADO" status={false}></BarraEstado>
                    </div>
                    : <></>
                }
                {estado == "CERRADO" ?
                    <div
                        className=" flex justify-center mt-[-7px]"
                    >
                        <BarraEstado name="ABIERTO" status={true}></BarraEstado>
                        <div className="bg-[#3682F7] w-[7vw] h-[7px] mt-[8px] z-20" />
                        <BarraEstado name="FINALIZADO" status={true}></BarraEstado>
                        <div className="bg-[#3682F7] w-[7vw] h-[7px] mt-[8px] z-20" />
                        <BarraEstado name="CERRADO" status={true}></BarraEstado>
                        <div className="bg-[#BBBABA] w-[7vw] h-[7px] mt-[8px] z-20" />
                        <BarraEstado name="ENTREGADO" status={false}></BarraEstado>
                    </div>
                    : <></>
                }
                {estado == "ENTREGADO" ?
                    <div
                        className=" flex justify-center mt-[-7px]"
                    >
                        <BarraEstado name="ABIERTO" status={true}></BarraEstado>
                        <div className="bg-[#3682F7] w-[7vw] h-[7px] mt-[8px] z-20" />
                        <BarraEstado name="FINALIZADO" status={true}></BarraEstado>
                        <div className="bg-[#3682F7] w-[7vw] h-[7px] mt-[8px] z-20" />
                        <BarraEstado name="CERRADO" status={true}></BarraEstado>
                        <div className="bg-[#3682F7] w-[7vw] h-[7px] mt-[8px] z-20" />
                        <BarraEstado name="ENTREGADO" status={true}></BarraEstado>
                    </div>
                    : <></>
                }
            </div>
        );

    }



    const BarraEstado = (props) => {

        const { name, status } = props;

        console.log('status' + status);
        console.log('name' + name);

        return (
            <div>
                <div
                    className="justify-center items-center"
                >
                    {status ?
                        <div className="bg-[#3682F7] rounded-full w-6 h-6 mt-35 z-40" /> :
                        <div className="bg-[#BBBABA] rounded-full w-6 h-6 mt-35 z-40 border-[#BBBABA]" />
                    }
                    {status ?
                        <p className="flex-col text-[#3682F7] text-8 w-[20px] ml-[-12px] "> {name} </p> :
                        <p className="flex-col text-[#BBBABA] text-8 w-[20px] ml-[-12px] "> {name} </p>
                    }

                </div>
            </div>
        );
    }

    return (
        <div>
            <Estado estado='CERRADO'></Estado>
        </div>
    );
};

export default BarraAzul;
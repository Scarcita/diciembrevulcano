import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'
import { useEffect, useState } from 'react'

function TablaRespuestosRequeridos() {

    const [showDots, setShowDots] = useState(true);
    const [total, setTotal] = useState(0);
    const [data, setData] = useState([]);

    useEffect(() => {

        fetch('https://slogan.com.bo/vulcano/orders/all/abierto')
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log(data.data);
                    setData(data.data)
                } else {
                    console.error(data.error)
                }
            })
            .then(setShowDots(false))
    }, [])

    return (
        showDots ?
            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                <Spinner color="#3682F7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>
            :
            <>
                <div >
                    <TablaProductos data={data} />

                </div>
            </>
    )
};

const TablaProductos = (props) => {
    const { data } = props;
    console.log("assssssssssssssssssss");
    console.log(data);
    console.log(props);
    const map1 = data.map(row => console.log(row));

    return (

        <div className='lg:w-full lg:h-[120px] min-w-300px max-w-full bg-[#FFFF] grid grid-cols-12'>
            <div className='col-span-12 md:col-span-12'>
                <table className="table-auto hidden lg:table w-full h-60 ">
                    <thead className='bg-[#3682F7] h-8'>
                        <tr className='text-[12px] text-[#FFFFFF]'>
                            <th className='text-left px-2'>Nombre</th>
                            <th className='text-left'>Precio ref.</th>
                            <th className='text-left'>Precio hr.</th>
                            <th className='text-left'>Hrs. hombre</th>
                            <th className='text-left'>Cliente</th>
                            <th className='text-left'>Empresa</th>
                            <th className='text-left'>Garantía</th>
                            <th className='text-left'>Mecánico</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>

                    <tbody className='  overflow-scroll'>
                        {data.map(row =>
                            <tr key={data.id}>
                                <td className="m-2">{row.client.name}</td>
                                <td>{row.total}</td>
                                <td>{row.subtotal}</td>
                                <td>{row.discount}</td>
                                <td>{row.client.name}</td>
                                <td>{row.client.name}</td>
                                <td>{row.car.transmission}</td>
                                <td>{row.contact_name}</td>
                                <td>
                                    <input
                                        className='bg-[#FFFF] rounded-lg border-[#3682F7] border-2 text-[#3682F7] flex flex-row justify-center items-center w-[25px] h-[25px] m-2' type="button"
                                        value="X"
                                    />
                                </td>
                                <td>
                                    <input
                                        className='bg-[#FFFF] rounded-lg border-[#FF0000] border-2 text-[#FF0000] flex flex-row justify-center items-center w-[25px] h-[25px] m-2' type="button"
                                        value="X"
                                    />
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>

                <div className="p-5 bg-gray-100">
                    <div className="grid grid-cols-1 lg:grid-cols-2 gap-4 md:block  lg:hidden">
                        <div className="bg-[#F6F6F6] space-y-3 p-4 rounded-lg overflow-auto">
                            {data.map(row =>
                                <div key={data} className="grid grid-cols-3 items-center text-sm rounded bg-[#fff] shadow py-4">
                                    <div className='m-auto'>
                                        <div className='mb-2'>
                                            <div className='text-xl font-bold text-[#3682F7]'>
                                                Nombre
                                            </div>
                                            <div className="text-black font-bold">
                                                {row.client.name}
                                            </div>
                                        </div>
                                        <div className='mb-2'>
                                            <div className=' text-lg font-bold text-[#3682F7] '>
                                                Precio ref.
                                            </div>
                                            <div className="text-black font-bold">
                                                {row.total}
                                            </div>
                                        </div>
                                        <div>
                                            <div className='text-lg font-bold text-[#3682F7] '>
                                                Precio hr.
                                            </div>
                                            <div className="text-black font-bold ">
                                                {row.client.name}
                                            </div>
                                        </div>
                                    </div>
                                    <div className='m-auto'>
                                        <div className='mb-2'>
                                            <div className='text-xl font-bold text-[#3682F7] '>
                                                Cliente
                                            </div>
                                            <div className="text-black font-bold">
                                                {row.client.name}
                                            </div>
                                        </div>
                                        <div className='mb-2'>
                                            <div className='text-lg font-bold text-[#3682F7] '>
                                                Hrs. hombre
                                            </div>
                                            <div className="text-black font-bold">
                                                {row.discount}
                                            </div>
                                        </div>
                                        <div>
                                            <div className='text-xl font-bold text-[#3682F7] '>
                                                Cliente
                                            </div>
                                            <div className="text-black font-bold">
                                                {row.client.name}
                                            </div>
                                        </div>
                                    </div>
                                    <div className='m-auto'>
                                        <div className='mb-2'>
                                            <div className='text-xl font-bold text-[#3682F7]'>
                                                Empresa                                        </div>
                                            <div className="text-black font-bold">
                                                {row.client.name}

                                            </div>
                                        </div>
                                        <div className='mb-2'>
                                            <div className='text-xl font-bold text-[#3682F7]'>
                                                Garantía                                        </div>
                                            <div className="text-black font-bold">
                                                {row.car.transmission}

                                            </div>
                                        </div>
                                        <div>
                                            <div className='text-xl font-bold text-[#3682F7] '>
                                                Mecánico
                                            </div>
                                            <div className="text-black font-bold">
                                                {row.contact_name}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default TablaRespuestosRequeridos;
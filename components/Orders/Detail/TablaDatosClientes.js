function TablaDatosCliente() {
    return (
       
            <div className='lg:w-full lg:h-[120px] min-w-800px max-w-full bg-[#FFFF] grid grid-cols-12'>
                <table className="col-span-12 md:col-span-12">
                    <thead >
                        <tr className='text-[12px] text-[#ACB5BD] font-Dosis'>
                            <th className='text-left'>Cliente Nombre</th>
                            <th className='text-left'>Celular</th>
                            <th className='text-left'>E-mail</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr className='text-[9px]'>
                            <td>GOBIERNO AUTÓNOMO DE COCHABAMBA GAMLC (SEUWS)</td>
                            <td>+5593328773237</td>
                            <td>Casefs.eruhfse877823@gmail.com</td>
                        </tr>
                    </tbody>
                </table> <br></br>
                <table className="col-span-12 md:col-span-12">
                    <thead>
                        <tr className='text-[12px] text-[#ACB5BD] font-Dosis'>
                            <th className=' text-left'>Facturación Nombre</th>
                            <th className=' text-left'>C.I./NIT</th>
                            <th className=' text-left'>Contacto Nombre</th>
                            <th className=' text-left'>Celular</th>

                        </tr>
                    </thead>
                    <tbody>
                        <tr className='text-[9px]'>
                            <td>GOBIERNO AUTÓNOMO DE COCHABAMBA GAMLC (SEUWS)</td>
                            <td>+87813246665</td>
                            <td>GOBIERNO AUTÓNOMO DE COCHABAMBA GAMLC (SEUWS)</td>
                            <td>+87813246665</td>

                        </tr>
                    </tbody>
                </table>
            </div>
    );
};
export default TablaDatosCliente;
import Image from "next/image";

import  imagenFord from '../../../assets/ford.png'

function DatosDelAuto() {
    return (
     
            <div className='h-full bg-[#3682F7]'>
                <div className='w-ful h-auto flex flex-col justify-center items-center'>
                    <div className='w-[160px] mt-5'>
                        <Image
                            src={imagenFord}
                            layout='fixed'
                            alt='imagenFord'
                        />
                    </div>
                </div>
                <div>
                    <div className='px-10 text-[#FFFF] m-2'>
                        <p className='text-sm'>Marca</p>
                        <p className='text-base'>MERCEDES BENZ</p>
                    </div>
                    <div className='px-10 text-[#FFFF] m-2'>
                        <p className='text-[10px]'>Modelo</p>
                        <p className='text-[17px]'>MODELO</p>
                    </div>
                    <div className='px-10 text-[#FFFF] m-2'>
                        <p className='text-[10px]'>Versión</p>
                        <p className='text-[17px]'>Versión</p>
                    </div>
                    <div className='px-10 text-[#FFFF] m-2'>
                        <p className='text-[10px]'>Color</p>
                        <p className='text-[17px]'>COLOR</p>
                    </div>
                    <div className='px-10 text-[#FFFF] m-2'>
                        <p className='text-[10px]'>Placa</p>
                        <p className='text-[17px]'>PLACA</p>
                    </div>
                    <div className='px-10 text-[#FFFF] m-2'>
                        <p className='text-[10px]'>Vin</p>
                        <p className='text-[17px]'>1234567809123RGTF</p>
                    </div>
                    <div className='px-10 text-[#FFFF] m-2'>
                        <p className='text-[10px]'>Kilometraje</p>
                        <p className='text-[17px]'>7779875</p>
                    </div>
                    <div className='px-10 text-[#FFFF] m-2'>
                        <p className='text-[10px]'>Transmisión</p>
                        <p className='text-[17px]'>AUTOMÁTICA</p>
                    </div>
                </div>

                <div className='flex flex-col justify-center items-center bg-[#3682F7]' >
                    <button className=' w-[200px] h-8 border-white border-2 rounded-lg text-[13px] text-[#FFFF] m-3'>Editar Servicios Requeridos</button>
                    <button className=' w-[200px] h-8 border-white border-2 rounded-lg text-[#FFFF] text-[#FFFF] m-2'>Anular Orden de Trabajo</button>
                    <button className=' w-[200px] h-8 border-white border-2 rounded-lg text-[13px] text-[#FFFF] m-2'>Historial de Vehículo</button>
                    <button className=' w-[200px] h-8 bg-[#FFFF] border-2 rounded-lg text-[13px] text-[#3682F7] m-2'>Marcar como Entregado</button>
                    <button className=' w-[200px] h-8 bg-[#FFFF] border-2 rounded-lg text-[13px] text-[#3682F7] m-2'>Imprimir Pre-Factura</button>

                </div>

            </div>
       
    )
}
export default DatosDelAuto;
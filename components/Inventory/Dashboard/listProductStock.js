import React, { useEffect, useState } from "react";
import Image from "next/image";

import Spinner from "react-activity/dist/Spinner";
import "react-activity/dist/Spinner.css";

import ImgItems from "../../../public/Desktop8/items.svg";

export default function ListProducts() {
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState([]);
  const headlist = ["Producto", "", "", "Stock Min", "Stock Actual"];

  const getProducts = () => {
    setIsLoading(true);
    fetch("https://slogan.com.bo/vulcano/products/all")
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          console.log(data.data);
          setData(data.data);
        } else {
          console.error(data.error);
        }
        setIsLoading(false);
      });
  };

  useEffect(() => {
    getProducts();
  }, []);

  return isLoading ? (
    <div className="flex justify-center items-center self-center mt-[30px]">
      <Spinner
        color="#3682F7"
        size={17}
        speed={1}
        animating={true}
        style={{ marginLeft: "auto", marginRight: "auto" }}
      />
    </div>
  ) : (
    <div>
      <Table3 data={data} headlist={headlist} />
      <TablaResponsive data={data} headlist={headlist} />
    </div>
  );
}

///////////////// TABLE TRES //////////

const Table3 = (props) => {
  const { data, headlist } = props;

  return (
    <div className="hidden lg:block md:block">
      <table className="w-full bg-[#fff] rounded-[20px] shadow-[25px]">
        <thead className="">
          <tr>
            {headlist.map((header) => (
              <th
                key={headlist.id}
                className="text-[14px] border-b-2 font-medium md:text-[14px] lg:text-[16px] text-[#A5A5A5] py-3"
              >
                {header}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {data.map((row) => (
            <tr key={row.id} className="">
              <td className="">
                <div className="w-[45px] h-[45px] md:w-[48px] md:h-[48px] lg:w-[52px] md:h-[52px] bg-[#F6F6FA] rounded-full m-3"></div>
              </td>
              <td className="text-[12px] md:text-[14px] lg:text-[16px] text-[#000000] self-center text-start">
                {row.code}
              </td>
              <td className="text-[12px] md:text-[14px] lg:text-[16px] text-[#000000] font-bold self-center">
                {row.name}
              </td>
              <td>
                <div className="text-[20px] md:text-[30px] lg:text-[32px] text-[#000000] font-semibold text-center self-center">
                  {row.min_stock}
                </div>
              </td>
              <td>
                <div className="text-[20px] md:text-[30px] lg:text-[32px] text-[#FF0000] font-semibold text-center self-center">
                  {" "}
                  {row.stock_qty}
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

const TablaResponsive = (props) => {
  const { data } = props;

  return (
    <div className="grid grid-cols-12 mt-[20px] ">
      <div className="col-span-12 md:col-span-12 lg:col-span-12 rounded-[10px] md:hidden">
        {data.map((row) => {
          return (
            <div
              key={row.id}
              className="grid grid-cols-12 bg-[#fff] rounded-[15px] shadow-md pl-[10px] pb-[10px] mb-[15px]"
            >
              <div className="col-span-8 md:col-span-12">
                <div className="grid grid-cols-12">
                  <div className="col-span-4 md:col-span-12 self-center items-center">
                    <div className="w-[64px] h-[64px] bg-[#F6F6FA] rounded-full ">
                      {/* <Image
                                                    className='rounded-full '
                                                    src={row.car.cars_models_version.cars_model.catalogues_record.additional_info}
                                                    alt="media"
                                                    layout="fixed"
                                                    width={64}
                                                    height={64}
                                                /> */}
                    </div>
                  </div>
                  <div className="col-span-4 md:col-span-12 self-center items-center">
                    <p className="text-[14px] text-[#000000] ">{row.code}</p>
                  </div>

                  <div className="col-span-4 md:col-span-12 self-center items-center">
                    <p className="text-[14px] text-[#000000] font-bold">
                      {" "}
                      {row.name}
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-span-4 md:col-span-12">
                <div>
                  <p className="text-[#A5A5A5] text-[12px] text-center">
                    Stock Min
                  </p>
                  <p className="text-[20px] text-[#000000] font-semibold text-center self-center">
                    {row.min_stock}
                  </p>
                </div>
                <div>
                  <p className="text-[#A5A5A5] text-[12px] text-center">
                    Stock Actual
                  </p>
                  <p className="text-[20px] text-[#FF0000] font-semibold text-center self-center">
                    {row.stock_qty}
                  </p>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

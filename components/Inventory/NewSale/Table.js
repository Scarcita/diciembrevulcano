import React, { useEffect, useState } from "react";
import Modal from "./Modal";
import Spinner from "react-activity/dist/Spinner";
import "react-activity/dist/Spinner.css";

export default function Datos(props) {
  const headlist = ["", "Código", "Producto", "Cant. en stock", ""];
  const { getDatos, data, setDataColumnaDerecha, isLoading, setIsLoading } =
    props;

  // console.log('ROWWW ' + Object.entries(data))
  return isLoading ? (
    <div className="flex justify-center items-center self-center mt-[30px]">
      <Spinner
        color="#3682F7"
        size={17}
        speed={1}
        animating={true}
        style={{ marginLeft: "auto", marginRight: "auto" }}
      />
    </div>
  ) : (
    <div>
      <Table
        headlist={headlist}
        data={data}
        getDatos={getDatos}
        setDataColumnaDerecha={setDataColumnaDerecha}
      />
      <TableResponsive
        headlist={headlist}
        data={data}
        getDatos={getDatos}
        setDataColumnaDerecha={setDataColumnaDerecha}
      />
    </div>
  );
}

const Table = (props) => {
  const { headlist, data, getDatos, setDataColumnaDerecha } = props;

  const users = data;
  const [search, setSearch] = useState("");

  const searcher = (e) => {
    setSearch(e.target.value);
  };

  var results = !search
    ? users
    : users.filter((dato) =>
        dato.name.toLowerCase().includes(search.toLocaleLowerCase())
      );
  var results = !search
    ? users
    : users.filter((dato) =>
        dato.code.toLowerCase().includes(search.toLocaleLowerCase())
      );

  return (
    <div className="rounded-lg  hidden lg:block md:block mt-[20px]">
      <div className="flex">
        <input
          value={search}
          onChange={searcher}
          type="text"
          placeholder="Busqueda por Código"
          className="bg-[#FFFF] w-full h-[30px] rounded-[8px] border-[#d2d2d2] border-[1px] pl-[10px] pr-[10px] mr-[10px]"
        />
        <button className="w-[30px] h-[30px] rounded-lg border-[#3682F7] border-[1px] text-center hover:bg-[#3682F7] pl-[3px] self-center">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="w-5 h-5 text-[#3682F7] hover:text-[#fff] self-center text-center"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
            />
          </svg>
        </button>
      </div>

      <table className="w-full bg-[#fff] rounded-[24px] h-[150px] shadow-md mt-[20px]">
        <thead className="">
          <tr>
            {headlist.map((header) => (
              <th
                key={headlist.id}
                className="p-3 font-light text-left text-[12px] md:text-[14px] lg:text-[16px] font-semibold text-[#A5A5A5]"
              >
                {" "}
                {header}{" "}
              </th>
            ))}
          </tr>
        </thead>

        <tbody>
          {results.map((row) => (
            <tr
              key={row.id}
              className="hover:bg-[#F6F6FA] cursor-pointer"
              onClick={() => {
                setDataColumnaDerecha(row);
                getDatos(row);
              }}
            >
              <td className="p-3">
                <div className="flex justify-center items-center  w-[38px] h-[38px] md:w-[42px] md:h-[45px] lg:w-[45px] md:h-[45px] bg-[#F6F6FA] rounded-full m-[5px] self-center">
                  <svg
                    width="28"
                    height="28"
                    viewBox="0 0 20 15"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M13.2348 7.42596C12.7102 7.42596 12.2167 7.1478 11.9498 6.70275L9.95395 3.40815L7.96122 6.70275C7.69117 7.15089 7.19765 7.42905 6.67308 7.42905C6.5334 7.42905 6.39373 7.4105 6.26026 7.37033L2.00786 6.1588V11.6601C2.00786 12.1144 2.31825 12.51 2.75901 12.6182L9.46973 14.2902C9.78633 14.3675 10.1185 14.3675 10.432 14.2902L17.1489 12.6182C17.5896 12.5069 17.9 12.1113 17.9 11.6601V6.1588L13.6476 7.36723C13.5142 7.40741 13.3745 7.42596 13.2348 7.42596ZM19.8338 3.95828L18.2353 0.781129C18.139 0.589511 17.9311 0.478248 17.7169 0.506064L9.95395 1.49197L12.8003 6.1928C12.9182 6.38751 13.1541 6.48023 13.3745 6.41841L19.5172 4.67222C19.8245 4.58259 19.9735 4.24262 19.8338 3.95828ZM1.67263 0.781129L0.0741003 3.95828C-0.068681 4.24262 0.0834121 4.58259 0.387598 4.66913L6.5303 6.41532C6.75068 6.47714 6.98658 6.38442 7.10453 6.18971L9.95395 1.49197L2.18789 0.506064C1.97371 0.481339 1.76885 0.589511 1.67263 0.781129Z"
                      fill="#A5A5A5"
                    />
                  </svg>
                </div>
              </td>
              <td className="text-[12px] md:text-[14px] lg:text-[16px] text-[#000000] self-center whitespace-nowrap p-3">
                {row.code}
              </td>
              <td className="text-[12px] md:text-[14px] lg:text-[16px] text-[#000000] self-center whitespace-nowrap p-3 font-bold">
                {row.name}
              </td>
              <td className="text-[20px] md:text-[30px] lg:text-[32px] text-[#71AD46] self-center text-center font-semibold ">
                {row.min_stock}
              </td>
              <td className="text-center">
                <Modal
                  row={row}
                  id={row.id}
                  code={row.code}
                  name={row.name}
                  created_by={row.created_by}
                  min_stock={row.min_stock}
                  setDataColumnaDerecha={setDataColumnaDerecha}
                />{" "}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

const TableResponsive = (props) => {
  const { headlist, data, getDatos, setDataColumnaDerecha } = props;

  const users = data;
  const [search, setSearch] = useState("");

  const searcher = (e) => {
    setSearch(e.target.value);
  };

  var results = !search
    ? users
    : users.filter((dato) =>
        dato.name.toLowerCase().includes(search.toLocaleLowerCase())
      );
  var results = !search
    ? users
    : users.filter((dato) =>
        dato.code.toLowerCase().includes(search.toLocaleLowerCase())
      );

  return (
    <div className="grid grid-cols-12 mt-[20px] ">
      <div className="col-span-12 md:col-span-12 lg:col-span-12 rounded-[10px] md:hidden">
        <div className="flex">
          <input
            value={search}
            onChange={searcher}
            type="text"
            placeholder="Busqueda por Código"
            className="bg-[#FFFF] w-full h-[30px] rounded-[8px] border-[#d2d2d2] border-[1px] pl-[10px] pr-[10px] mr-[10px]"
          />
          <button className="w-[30px] h-[30px] rounded-lg border-[#3682F7] border-[1px] text-center hover:bg-[#3682F7] pl-[3px] self-center">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              strokeWidth={1.5}
              stroke="currentColor"
              className="w-5 h-5 text-[#3682F7] hover:text-[#fff] self-center text-center"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
              />
            </svg>
          </button>
        </div>

        {results.map((row) => {
          return (
            <div
              key={row.id}
              className="grid grid-cols-12 bg-[#fff] rounded-[15px] shadow-md pl-[10px] pb-[10px] mt-[20px] mb-[15px]"
            >
              <div className="col-span-10 md:col-span-12">
                <div className="grid grid-cols-12">
                  <div className="col-span-3 md:col-span-3 lg:col-span-1 self-center items-center">
                    <div className="flex justify-center items-center w-[50px] h-[50px] bg-[#F6F6FA] rounded-full self-center">
                      <svg
                        width="28"
                        height="28"
                        viewBox="0 0 20 15"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M13.2348 7.42596C12.7102 7.42596 12.2167 7.1478 11.9498 6.70275L9.95395 3.40815L7.96122 6.70275C7.69117 7.15089 7.19765 7.42905 6.67308 7.42905C6.5334 7.42905 6.39373 7.4105 6.26026 7.37033L2.00786 6.1588V11.6601C2.00786 12.1144 2.31825 12.51 2.75901 12.6182L9.46973 14.2902C9.78633 14.3675 10.1185 14.3675 10.432 14.2902L17.1489 12.6182C17.5896 12.5069 17.9 12.1113 17.9 11.6601V6.1588L13.6476 7.36723C13.5142 7.40741 13.3745 7.42596 13.2348 7.42596ZM19.8338 3.95828L18.2353 0.781129C18.139 0.589511 17.9311 0.478248 17.7169 0.506064L9.95395 1.49197L12.8003 6.1928C12.9182 6.38751 13.1541 6.48023 13.3745 6.41841L19.5172 4.67222C19.8245 4.58259 19.9735 4.24262 19.8338 3.95828ZM1.67263 0.781129L0.0741003 3.95828C-0.068681 4.24262 0.0834121 4.58259 0.387598 4.66913L6.5303 6.41532C6.75068 6.47714 6.98658 6.38442 7.10453 6.18971L9.95395 1.49197L2.18789 0.506064C1.97371 0.481339 1.76885 0.589511 1.67263 0.781129Z"
                          fill="#A5A5A5"
                        />
                      </svg>
                    </div>
                  </div>
                  <div className="mt-2 col-span-7 md:col-span-8 ls:col-span-10">
                    <div className="flex">
                      <div className="w-10 text-sm font-semibold tracking-wide text-left text-[#000000]">
                        Código:
                      </div>
                      <div className="whitespace-nowrap text-sm text-[#000000] pl-3">
                        {row.code}
                      </div>
                    </div>
                    <div className="flex">
                      <div className="w-10 text-sm font-semibold tracking-wide text-left text-[#000000]">
                        Producto:
                      </div>
                      <div className="w-18 whitespace-nowrap text-sm text-[#000000] pl-5">
                        {row.name}
                      </div>
                    </div>
                    <div className="flex">
                      <div className="w-18 text-sm font-semibold tracking-wide text-left text-[#000000]">
                        Cant. en stock :
                      </div>
                      <div className="whitespace-nowrap text-sm text-[#000000] pl-1">
                        {row.min_stock}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-span-2 md:col-span-12 text-center">
                <Modal
                  row={row}
                  id={row.id}
                  getDatos={getDatos}
                  code={row.code}
                  name={row.name}
                  created_by={row.created_by}
                  min_stock={row.min_stock}
                  setDataColumnaDerecha={setDataColumnaDerecha}
                />
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

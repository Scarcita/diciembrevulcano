import React, { useEffect, useState } from 'react';
import Spinner from 'react-activity/dist/Spinner';
import 'react-activity/dist/Spinner.css';

import Modal from './Modal';

export default function Datos(props) {
    const { getDatos, data } = props;

    const headlist = [
        "", 'Codigo', "Producto", "Precio", 'Costo Promedio', "Stock Min.", "Stock Actual", ""
    ];


    return (
        <div className=''>
            <Table headlist={headlist} data={data} getDatos={getDatos} />
            <TableResponsive headlist={headlist} data={data} getDatos={getDatos} />
        </div>
    )
}

const Table = (props) => {

    const { headlist, data, getDatos } = props;

    return (
        <div className='rounded-lg shadow-md hidden lg:block md:block'>
            <table className='w-full'>
                <thead className='bg-[#FFFF] shadow-md rounded-md'>
                    <tr>
                        {headlist.map(header => <th key={headlist.id} className='p-3 font-light text-left text-[12px] md:text-[14px] lg:text-[16px] text-[#000000]'>{header}</th>)}
                    </tr>
                </thead>

                <tbody>
                    {data.map(row => <TableRow key={row.id} row={row} getDatos={getDatos} />)}
                </tbody>
            </table>
        </div>
    );
};


const TableResponsive = (props) => {

    const { data, getDatos } = props;

    return (
        <div className='grid grid-cols-1 sm:grid-cols-2 gap-4 md:hidden'>
            {data.map(row => <TableRowResponsive key={row.id} row={row} getDatos={getDatos} />)}
        </div>

    );
};
class TableRow extends React.Component {
    render() {
        let row = this.props.row;
        console.log('Row.id: ' + row.id)
        return (
            <tr className='bg-[#FFFF] '>
                <td className='p-3'>
                    <div className="w-[38px] h-[38px] md:w-[42px] md:h-[42px] lg:w-[45px] md:h-[45px] bg-[#F6F6FA] rounded-full m-[5px] self-center " />
                </td>
                <td className='text-[12px] md:text-[14px] lg:text-[16px] text-[#000000] whitespace-nowrap p-3'>{row.code}</td>
                <td className='text-[12px] md:text-[14px] lg:text-[16px] text-[#000000] whitespace-nowrap p-3 font-bold'>{row.name}</td>
                <td className='text-[20px] md:text-[30px] lg:text-[32px] text-[#000000] whitespace-nowrap p-3'>{row.price}</td>
                <td className='text-[20px] md:text-[30px] lg:text-[32px] text-[#000000] whitespace-nowrap p-3'>{row.avg_cost}</td>
                <td className='text-[20px] md:text-[30px] lg:text-[32px] text-[#000000]'>{row.min_stock}</td>
                <td className='text-[20px] md:text-[30px] lg:text-[32px] text-[#FF0000] font-semibold '>{row.stock_qty}</td>
                <td className='text-[12px] md:text-[14px] lg:text-[16px] text-[#000000] text-center'>
                    <Modal
                        id={row.id}
                        getDatos={this.props.getDatos}
                    />
                </td>
            </tr>
        )
    }
}



class TableRowResponsive extends React.Component {
    render() {
        let row = this.props.row;

        return (
            <div className='bg-[#FFFF] p-4 rounded-2xl flex justify-center items-center shadow'>
                <div className='flex items-center'>
                    <div className='flex flex-col'>
                        <div className='grid grid-cols-12'>

                            <div className='col-span-3 md:col-span-3'>
                                <div className="w-[50px] h-[50px] md:w-[42px] md:h-[42px] lg:w-[45px] lg:h-[45px] bg-[#F6F6FA] rounded-full self-center " />
                            </div>

                            <div className='mt-2 col-span-7 md:col-span-8'>
                                <div className='flex'>
                                    <div className='w-10 text-sm font-semibold tracking-wide text-left text-[#000000] pl-1'>
                                        Código:
                                    </div>
                                    <div className='whitespace-nowrap text-sm text-[#000000] pl-3'>
                                        {row.code}
                                    </div>
                                </div>

                                <div className='flex'>
                                    <div className='w-10 text-sm font-semibold tracking-wide text-left text-[#000000]'>
                                        Producto:
                                    </div>
                                    <div className='whitespace-nowrap text-sm text-[#000000] pl-5'>
                                        {row.name}
                                    </div>
                                </div>
                            </div>

                            <div className='col-span-2 md:col-span-1 whitespace-nowrap p-3 text-sm text-[#000000] mt-[-10px] text-center'>
                                <Modal id={row.id} getDatos={this.props.getDatos} />
                            </div>
                        </div>

                        <div className='grid grid-cols-2'>
                            <div className='flex flex-col justify-center'>
                                <div className='flex'>
                                    <div className='w-10 text-sm font-semibold tracking-wide text-left text-[#000000]'>
                                        Precio:
                                    </div>
                                    <div className='whitespace-nowrap text-sm text-[#000000]'>
                                        {row.price}
                                    </div>
                                </div>

                                <div className='flex'>
                                    <div className=' text-sm font-semibold tracking-wide text-left text-[#000000]'>
                                        Costo Promedio:
                                    </div>
                                    <div className='whitespace-nowrap  text-sm text-[#000000]'>
                                        {row.avg_cost}
                                    </div>
                                </div>
                            </div>

                            <div className='flex flex-col justify-center pl-[15px] '>
                                <div className='flex'>
                                    <div className='w-13 text-sm font-semibold tracking-wide text-left text-[#000000]'>
                                        Stock Min.:
                                    </div>
                                    <div className='whitespace-nowrap text-sm text-[#000000]'>
                                        {row.min_stock}
                                    </div>
                                </div>

                                <div className='flex'>
                                    <div className='w-15 text-sm font-semibold tracking-wide text-left text-[#000000]'>
                                        Stock Max.:
                                    </div>
                                    <div className='whitespace-nowrap text-sm text-[#000000]'>
                                        {row.stock_qty}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
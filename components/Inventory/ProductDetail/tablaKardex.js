import React, { useEffect, useState } from "react";
import Spinner from "react-activity/dist/Spinner";
import "react-activity/dist/Spinner.css";
import Image from "next/image";
// import Modal from '../pantallaProductos/Modal';
import imagenIngreso from "../../../public/ingreso.svg";
import TooltipAmortiguador from "./tooltip";

export default function TablaKardex() {
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState([]);
  const headlist = ["", "Tipo", "Fecha", "Referencia", "Ingreso", "Egreso", ""];

  const getDatos = () => {
    setIsLoading(true);
    fetch("https://slogan.com.bo/vulcano/products/all")
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          console.log(data.data);
          setData(data.data);
        } else {
          console.error(data.error);
        }
        setIsLoading(false);
      });
  };
  useEffect(() => {
    getDatos();
  }, []);

  return isLoading ? (
    <div className="flex justify-center items-center self-center mt-[30px]">
      <Spinner
        color="#3682F7"
        size={17}
        speed={1}
        animating={true}
        style={{ marginLeft: "auto", marginRight: "auto" }}
      />
    </div>
  ) : (
    <div>
      <Table headlist={headlist} data={data} />
      <TableResponsive headlist={headlist} data={data} />
    </div>
  );
}

const Table = (props) => {
  const { headlist, data } = props;

  return (
    <div className="hidden lg:block md:block mt-[20px]">
      <table className="w-full bg-[#fff] rounded-[20px] shadow-[25px]">
        <thead className="">
          <tr>
            {headlist.map((header) => (
              <th
                key={headlist.id}
                className="text-[14px] border-b-2 font-medium text-start text-[#A5A5A5] py-2"
              >
                {header}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {data.map((row) => {
            let spliteado = row.created.split("T");
            let fecha = spliteado[0];
            let hora = spliteado[1].split("+")[0];

            return (
              <tr key={row.id}>
                <td className="self-center">
                  <div className="w-[54px] h-[54px] bg-[#F6F6FA] rounded-full m-[5px] self-center " />
                </td>
                <td className="text-[12px] md:text-[14px] lg:text-[16px] text-[#000000]">
                  {row.name}
                </td>
                <td className="self-center items-center text-[16px] text-[#000000] font-medium whitespace-nowrap ">
                  {" "}
                  {fecha} <span className="ml-[10px]">{hora}</span>{" "}
                </td>
                <td className="text-[16px] text-[#000000] font-medium self-center">
                  {row.name}
                </td>
                <td className="text-[30px] text-[#71AD46] font-semibold self-center">
                  {row.avg_cost}
                </td>
                <td className="text-[30px] text-[#000000] font-semibold self-center">
                  {row.min_stock}
                </td>
                <td className="text-center">
                  {" "}
                  <TooltipAmortiguador id={row.id} />
                  {" "}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

const TableResponsive = (props) => {
  const { data } = props;

  return (
    <div className="grid grid-cols-12 mt-[20px] ">
      <div className="col-span-12 md:col-span-12 lg:col-span-12 rounded-[10px] md:hidden">
        {data.map((row) => {
          let spliteado = row.created.split("T");
          let fecha = spliteado[0];
          let hora = spliteado[1].split("+")[0];

          return (
            <div
              key={row.id}
              className="grid grid-cols-12 bg-[#fff] rounded-[15px] shadow-md py-3 px-3 mb-[15px]"
            >
              <div className="col-span-11 md:col-span-12">
                <div className="grid grid-cols-12 gap-3">
                  <div className="col-span-6 md:col-span-12 flex justify-between self-center">
                    <div className="text-[16px] font-medium text-[#000000] ">
                      {fecha}
                    </div>
                    <div className="text-[16px] font-medium  text-[#000000] ">
                      {hora}
                    </div>
                  </div>
                  <div className="col-span-6 md:col-span-12 flex justify-between">
                    <div></div>
                    <div className="flex ">
                      <div className="text-[14px] text-[#000000] self-center m-2">
                        INGRESO
                      </div>
                      <div className="w-[30px] h-[30px] bg-[#F6F6FA] rounded-full ">
                        <Image
                          className="rounded-full "
                          src={imagenIngreso}
                          alt="media"
                          layout="responsive"
                          width={30}
                          height={30}
                        />
                      </div>
                    </div>
                  </div>
                </div>

                <div className="grid grid-cols-12 gap-3">
                  <div className="col-span-6 md:col-span-12 self-center">
                    <div className="text-[16px] text-[#000000] font-medium ">
                      {row.name}
                    </div>
                  </div>
                  <div className="col-span-6 md:col-span-12 self-center flex justify-between">
                    <div className="text-[24px] text-[#71AD46] font-semibold self-center">
                      {row.avg_cost}
                    </div>
                    <div className="text-[24px] text-[#000000] font-semibold self-center">
                      {row.min_stock}
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-span-1 md:col-span-12 ">
                <TooltipAmortiguador id={row.id} />
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

import React, { useEffect, useState } from "react";
import Image from "next/image";

import ImgEdit from "../../../public/Desktop/editar.svg";
import Spinner from "react-activity/dist/Spinner";
import "react-activity/dist/Spinner.css";

export default function ModalNewEntry(props) {
  const [showModal, setShowModal] = useState(false);

  const [description, setDescription] = useState("");

  const { id, body } = props;

  return (
    <>
      <div>
        <button
          className="w-[100px] h-[28px] md:w-[115px] md:h-[30px] lg:w-[120px] lg:h-[30px] bg-[#3682F7] text-[#FFFFFF] text-[12px] md:text-[15px] lg:text-[15px] rounded-[8px] text-center items-center hover:bg-[#F6F6FA] hover:border-[#3682F7] hover:border-[1px] hover:text-[#3682F7] justify-center"
          type="button"
          onClick={() => setShowModal(true)}
        >
          Nuevo Ingreso
        </button>
      </div>
      {showModal ? (
        <>
          <div className="fixed inset-0 z-10 overflow-y-auto">
            <div
              className="fixed inset-0 w-full h-full bg-[#000000] opacity-40"
              onClick={() => setShowModal(false)}
            ></div>
            <div className="flex items-center min-h-screen px-4 py-8">
              <div className="relative w-full max-w-[600px] p-4 mx-auto bg-[#FFFF] rounded-md shadow-lg">
                <div>
                  <h4 className="text-lg font-medium text-gray-800">
                    Nuevo Ingreso
                  </h4>
                </div>

                <div className="mt-[20px] grid grid-cols-12 gap-4">
                  <div className="col-span-6 md:col-span-6 lg:col-span-6 ">
                    <p className="text-[12px] text-[#C1C1C1] mb-[2px]">
                      Cantidad
                    </p>
                    <input
                      className="w-full h-[30px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                      type="number"
                      // value={body}
                      // onChange={(e) => body(e)}
                    />
                  </div>

                  <div className="col-span-6 md:col-span-6 lg:col-span-6">
                    <p className="text-[12px] text-[#C1C1C1] mb-[2px]">Costo</p>
                    <input
                      className="w-full h-[30px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                      type="number"
                      // value={description}
                      // onChange={(e) => setDescription(e)}
                    />
                  </div>

                  <div className="col-span-6 md:col-span-6 lg:col-span-3 self-center">
                    <div className="flex flex-row">
                      <input
                        name="facebook"
                        type="checkbox"
                        id=""
                        className="mr-[5px]"
                        // {...register('facebook')}
                        // value={facebook}
                        // onChange={(e) => {
                        //     setFacebook(e.target.value)
                        // }}
                      />

                      <p className="text-[12px] text-[#C1C1C1] mb-[2px]">
                        Con Factura
                      </p>
                    </div>
                  </div>
                  <div className="col-span-6 md:col-span-6 lg:col-span-3 self-center">
                    <div className="flex flex-row">
                      <input
                        name="facebook"
                        type="checkbox"
                        id=""
                        className="mr-[5px]"
                        // {...register('facebook')}
                        // value={facebook}
                        // onChange={(e) => {
                        //     setFacebook(e.target.value)
                        // }}
                      />

                      <p className="text-[12px] text-[#C1C1C1] mb-[2px]">
                        Sin Factura
                      </p>
                    </div>
                  </div>
                  <div className="col-span-6 md:col-span-6 lg:col-span-6 ">
                    <p className="text-[12px] text-[#C1C1C1] mb-[2px]">
                      Factura
                    </p>
                    <input
                      className="w-full h-[30px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] pt-[3px] text-[10px] self-center items-center"
                      type="file"
                      value={description}
                      onChange={(e) => setDescription(e)}
                    />
                  </div>
                  <div className="col-span-12 md:col-span-12 lg:col-span-12">
                    <p className="text-[12px] text-[#C1C1C1] mb-[2px]">
                      Referencia
                    </p>
                    <input
                      className="w-full h-[30px] bg-[#FFFFFF] border-[1px] border-[#E4E7EB] rounded-[10px] pl-[5px] text-[12px]"
                      type="text"
                      value={description}
                      onChange={(e) => setDescription(e)}
                    />
                  </div>
                </div>
                <div className="flex flex-row justify-between mt-[20px]">
                  <div>
                    <h1 className="text-[12px] mt-[10px]">
                      * This field is mandatory
                    </h1>
                  </div>

                  <div>
                    <button
                      className="w-[75px] h-[35px] border-[1px] border-[#3682F7] rounded-[30px] text-[#3682F7] hover:bg-[#3682F7] hover:text-[#FFFF] text-[14px] mt-[3px] mr-[10px]"
                      onClick={() => setShowModal(false)}
                    >
                      Cancel
                    </button>
                    <button
                      className="w-[100px] h-[35px] border-[1px] bg-[#3682F7] rounded-[30px] text-[#FFFFFF] hover:border-[#3682F7] hover:bg-[#FFFFFF] hover:b-[#FFFFFF]  hover:text-[#3682F7] text-[14px] mt-[3px]"
                      onClick={() => setShowModal(false)}
                    >
                      Create
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : null}
    </>
  );
}

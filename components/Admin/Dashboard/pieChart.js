import React, { useEffect, useState } from 'react';
import Chart from "chart.js";

export default function PieChart() {
  const [isLoading, setIsLoading] = useState(true)
  const [orders, setOrders] = useState([]);

  useEffect(() => {

      setIsLoading(true)

      fetch('https://slogan.com.bo/vulcano/orders/adminDashboard')
          .then(response => response.json())
          .then(data => {
              if (data.status) {
                  var temp = [];
                  Object.values(data.data).map((result) =>{
                      temp.push(result);
                  })
  
                  setOrders(temp)
                  
              } else {
                  console.error(data.error)
              }
              setIsLoading(false)
          })

  }, [])

    React.useEffect(() => {
        let config = {
          type: "pie",
          data: {
            labels: [
              "Open", "Assigned", "In Progress", "Paused", "Finalized", "Closed", "Delivered", "Nulled"
             
    
            ],
            datasets: [
              {
                label: 'Projets',
                backgroundColor: ["#0066ff", "#9966ff", "#ff6600", "#009900", "#0066ff", "#9966ff", "#ff6600", "#009900"],
                //borderColor: "#4a5568",
                data: [
                  orders[0],
                  orders[1],
                  orders[2],
                  orders[3],
                  orders[4],
                  orders[5],
                  orders[6],
                  orders[7],
                  //orders[8],

                ],
                fill: false,
                barThickness: 12,
              },
            ],
          },
          options: {
            maintainAspectRatio: false,
            responsive: true,
            title: {
              display: false,
              text: "Orders Chart",
            },
            tooltips: {
              mode: "index",
              intersect: false,
            },
            hover: {
              mode: "nearest",
              intersect: true,
            },
            legend: {
              labels: {
                fontColor: "rgba(0,0,0,.4)",
                color: "#3682F7"
              },
              align: "end",
              position: "bottom",
            },
            scales: {
              xAxes: [
                {
                  ticks: {
                    fontColor: "#000000",
                  },
                  display: false,
                  scaleLabel: {
                    display: true,
                    labelString: "Month",
                  },
                  gridLines: {
                    borderDash: [2],
                    borderDashOffset: [2],
                    color: "rgba(33, 37, 41, 0.3)",
                    zeroLineColor: "rgba(33, 37, 41, 0.3)",
                    zeroLineBorderDash: [2],
                    zeroLineBorderDashOffset: [2],
                  },
                },
              ],
              yAxes: [
                {
                  display: true,
                  scaleLabel: {
                    display: false,
                    labelString: "Value",
                  },
                  gridLines: {
                    borderDash: [2],
                    drawBorder: false,
                    borderDashOffset: [2],
                    color: "rgba(33, 37, 41, 0.2)",
                    zeroLineColor: "rgba(33, 37, 41, 0.15)",
                    zeroLineBorderDash: [2],
                    zeroLineBorderDashOffset: [2],
                  },
                },
              ],
            },
          },
        };
    
        let ctx = document.getElementById("pie-chartOrders").getContext("2d");
        window.myBar = new Chart(ctx, config);
      }, []);
      
      return (
        <>
          <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded bg-[#fff]">
            <div className="p-4 flex-auto">
              <div className="relative h-[230px]">
                <canvas id="pie-chartOrders"></canvas>
              </div>
            </div>
          </div>
        </>
      );
    }
    
    
    
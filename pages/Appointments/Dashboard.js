import React from "react";
import Image from "next/image";

import Layout from "../../components/layout";

import imagenCalendario from "../../public/imagenCalendario.svg";
import ImgSmile from "../../public/citas/smile.svg";
import ImgSad from "../../public/citas/sad.svg";
import BarChartCitasYear from "../../components/Appointments/Dashboard/BarChartCitasYear";
import BarChartWeek from "../../components/Appointments/Dashboard/BarChartWeek";
import TableCitas from "../../components/Appointments/Dashboard/tableCitas";

//////////////////////// Contadores ////////////////////////////
// ESTE MES
import CounterEsteMesRealizadas from "../../components/Appointments/Dashboard/counterEsteMesRealizadas";
import CounterEsteMesConfirmadas from "../../components/Appointments/Dashboard/counterEsteMesConfirmadas";
import CounterEsteMesCanceladas from "../../components/Appointments/Dashboard/counterEsteMesCanceladas";

// MES ANTERIOR
import CounterMesAnteriorRealizadas from "../../components/Appointments/Dashboard/counterMesAnteriorRealizadas";
import CounterMesAnteriorConfirmadas from "../../components/Appointments/Dashboard/counterMesAnteriorConfirmadas";
import CounterMesAnteriorCanceladas from "../../components/Appointments/Dashboard/counterMesAnteriorCanceladas";

// ESTE AÑO
import CounterAñoReservas from "../../components/Appointments/Dashboard/counterAñoReservas";
import CounterAñoConfirmadas from "../../components/Appointments/Dashboard/counterAñoConfirmadas";
import CounterAñoCanceladas from "../../components/Appointments/Dashboard/counterAñoCanceladas";

function pantallaCitas() {
  return (
    <div className="grid grid-cols-12 mt-[20px] ml-[20px] mr-[20px]">
      <div className="col-span-12 md:col-span-12 lg:col-span-8">
        <div className=" pt-8 grid grid-cols-12 gap-6">
          <div className="col-span-12 md:col-span-6 lg:col-span-6">
            <div className="flex flex-row">
              <h2 className="text-[24px] font-semibold">Citas</h2>
              <h2 className="text-[14px] pt-[10px]">/Este mes</h2>
            </div>
            <div className="pt-[20px] grid grid-cols-12 gap-3">
              <div className="col-span-12 md:col-span-12 lg:col-span-12 flex flex-row bg-[#FFFF] rounded-[16px] shadow-md  justify-between py-2 px-2">
                <div>
                  <div className="text-[14px] font-semibold text-[#000000] ">
                    #Reservas realizadas
                  </div>
                  <div className="text-[40px] font-semibold text-[#3682F7]">
                    <CounterEsteMesRealizadas />
                  </div>
                  <div className="text-[10px] text-[#A5A5A5]">Este mes</div>
                </div>
                <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full self-center pt-[10px] text-center">
                  <Image
                    src={imagenCalendario}
                    layout="fixed"
                    alt="media"
                    width={30}
                    height={30}
                  />
                </div>
              </div>

              <div className="col-span-6 md:col-span-6 lg:col-span-6  bg-[#FFFF] rounded-[16px] shadow-md py-2 px-2">
                <div className="w-[30px] h-[30px] bg-[#F6F6FA] rounded-full ">
                  <Image
                    src={ImgSmile}
                    layout="responsive"
                    alt="media"
                    width={30}
                    height={30}
                  />
                </div>
                <p className="text-[14px] text-[#000000] font-semibold">
                  Confirmadas
                </p>
                <div className="text-[40px] font-semibold text-[#3682F7]">
                  <CounterEsteMesConfirmadas />
                </div>
                <p className="text-[10px] text-[#A5A5A5]">Este mes</p>
              </div>

              <div className="col-span-6 md:col-span-6 lg:col-span-6  bg-[#FFFF] rounded-[16px] shadow-md py-2 px-2">
                <div className="w-[30px] h-[30px] bg-[#F6F6FA] rounded-full ">
                  <Image
                    src={ImgSad}
                    layout="responsive"
                    alt="media"
                    width={30}
                    height={30}
                  />
                </div>
                <p className="text-[14px] text-[#000000] font-semibold">
                  Canceladas
                </p>
                <div className="text-[40px] font-semibold text-[#3682F7]">
                  <CounterEsteMesCanceladas />
                </div>
                <p className="text-[10px] text-[#A5A5A5]">Este mes</p>
              </div>
            </div>
          </div>

          <div className="col-span-12 md:col-span-6 lg:col-span-6">
            <div className="flex flex-row">
              <h2 className="text-[24px] font-semibold">Citas</h2>
              <h2 className="text-[14px] pt-[10px]">/Mes anterior</h2>
            </div>
            <div className=" pt-[20px] grid grid-cols-12 gap-3">
              <div className="col-span-12 md:col-span-12 lg:col-span-12 flex flex-row bg-[#FFFF] rounded-[16px] shadow-md  justify-between py-2 px-2">
                <div>
                  <div className="text-[14px] font-semibold text-[#000000] ">
                    #Reservas realizadas
                  </div>
                  <div className="text-[40px] font-semibold text-[#3682F7]">
                    <CounterMesAnteriorRealizadas />
                  </div>
                  <div className="text-[10px] text-[#A5A5A5]">Mes anterior</div>
                </div>
                <div className="w-[50px] h-[50px] bg-[#F6F6FA] rounded-full self-center pt-[10px] text-center">
                  <Image
                    src={imagenCalendario}
                    layout="fixed"
                    alt="media"
                    width={30}
                    height={30}
                  />
                </div>
              </div>

              <div className="col-span-6 md:col-span-6 lg:col-span-6  bg-[#FFFF] rounded-[16px] shadow-md py-2 px-2">
                <div className="w-[30px] h-[30px] bg-[#F6F6FA] rounded-full ">
                  <Image
                    src={ImgSmile}
                    layout="responsive"
                    alt="media"
                    width={30}
                    height={30}
                  />
                </div>
                <p className="text-[14px] text-[#000000] font-semibold">
                  Confirmadas
                </p>
                <div className="text-[40px] font-semibold text-[#3682F7]">
                  <CounterMesAnteriorConfirmadas />
                </div>
                <p className="text-[10px] text-[#A5A5A5]">Mes anterior</p>
              </div>

              <div className="col-span-6 md:col-span-6 lg:col-span-6  bg-[#FFFF] rounded-[16px] shadow-md py-2 px-2">
                <div className="w-[30px] h-[30px] bg-[#F6F6FA] rounded-full ">
                  <Image
                    src={ImgSad}
                    layout="responsive"
                    alt="media"
                    width={30}
                    height={30}
                  />
                </div>
                <p className="text-[14px] text-[#000000] font-semibold">
                  Canceladas
                </p>
                <div className="text-[40px] font-semibold text-[#3682F7]">
                  <CounterMesAnteriorCanceladas />
                </div>
                <p className="text-[10px] text-[#A5A5A5]">Este mes</p>
              </div>
            </div>
          </div>
        </div>

        <div className="flex flex-row mt-[20px]">
          <h2 className="text-[24px] font-semibold">Citas</h2>
          <h2 className="text-[14px] pt-[10px]">/Este Año</h2>
        </div>

        <div className="flex grid grid-cols-12 gap-6">
          <div className="col-span-6 md:col-span-4 lg:col-span-4 bg-[#FFFF] rounded-2xl mt-[20px] pb-[5px] flex flex-col justify-center items-center py-2 px-2 shadow-md">
            <div className="bg-[#3682F7] w-[50px] h-[25px] rounded mt-2 flex justify-center shadow-md">
              <div className="bg-[#FFFF] w-[18px] h-[12px] rounded flex justify-center mt-2 ">
                <p className="text-[#3682F7] text-[12px] font-semibold mt-[-4px]">
                  ↑
                </p>
              </div>
              <p className="text-[14px] text-[#FFFF] mt-[3px] ml-[4px]">15%</p>
            </div>

            <div className="flex flex-col justify-center items-center">
              <p className="text-[#000000] text-[40px] font-medium ">
                <CounterAñoReservas />
              </p>
              <p className="text-[#000000] text-[15px] font-light mt-[-12px]">
                Reservas
              </p>
            </div>
          </div>

          <div className="col-span-6 md:col-span-4 lg:col-span-4 bg-[#FFFF] rounded-2xl mt-[20px] pl-[15px] flex flex-col justify-center items-center pr-[15px]pb-[5px] shadow-md">
            <div className="bg-[#3682F7] w-[50px] h-[25px] rounded mt-2 flex justify-center shadow-md">
              <div className="bg-[#FFFF] w-[18px] h-[12px] rounded flex justify-center mt-2 ">
                <p className="text-[#3682F7] text-[12px] font-semibold mt-[-4px]">
                  ↓
                </p>
              </div>
              <p className="text-[14px] text-[#FFFF] mt-[3px] ml-[4px]">15%</p>
            </div>
            <div className="flex flex-col justify-center items-center">
              <p className="text-[#000000] text-[40px] font-medium ">
                <CounterAñoConfirmadas />
              </p>
              <p className="text-[#000000] text-[15px] font-light mt-[-12px]">
                Confirmadas
              </p>
            </div>
          </div>

          <div className="col-span-12 md:col-span-4 lg:col-span-4 bg-[#FFFF] rounded-2xl mt-[20px] pl-[15px] flex flex-col justify-center items-center pr-[15px] pb-[5px] shadow-md">
            <div className="bg-[#EE002B] w-[50px] h-[25px] rounded mt-2 flex justify-center  shadow-md">
              <div className="bg-[#FFFF] w-[18px] h-[12px] rounded flex justify-center mt-2 ">
                <p className="text-[#EE002B] text-[12px] font-semibold mt-[-4px]">
                  ↓
                </p>
              </div>
              <p className="text-[14px] text-[#FFFF] mt-[3px] ml-[4px]">15%</p>
            </div>
            <div className="flex flex-col justify-center items-center">
              <p className="text-[#000000] text-[40px] font-medium ">
                <CounterAñoCanceladas />
              </p>
              <p className="text-[#000000] text-[15px] font-light mt-[-12px] ">
                Canceladas
              </p>
            </div>
          </div>

          <div className="col-span-12 md:col-span-12 lg:col-span-12 mt-[20px]">
            <BarChartCitasYear />
          </div>
        </div>
      </div>

      <div className="col-span-12 md:col-span-12 lg:col-span-4">
        <div className="grid grid-cols-12 mt-[20px] ml-[20px]">
          <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
            <div className="flex flex-row">
              <h2 className="text-[24px] font-semibold">Citas</h2>
              <h2 className="text-[14px] pt-[10px]">/Esta Semana</h2>
            </div>
            <div className=" mt-[30px]">
              <BarChartWeek />
            </div>
          </div>

          <div className="col-span-12 md:col-span-12 lg:col-span-12">
            <div className="flex flex-row">
              <h2 className="text-[24px] font-semibold">Citas</h2>
              <h2 className="text-[14px] pt-[10px]">/Hoy</h2>
            </div>
            <div className=" grid grid-cols-12">
              <div className="col-span-12 md:col-span-12 lg:col-span-12 mt-[20px]">
                <TableCitas />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
pantallaCitas.auth = true;
pantallaCitas.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};

export default pantallaCitas;

// import Head from 'next/head'
// import Image from 'next/image'
// import React, { useEffect, useState } from 'react'
// import { useForm } from 'react-hook-form';
// import { yupResolver } from '@hookform/resolvers/yup';
// import * as Yup from 'yup'


// import Link from 'next/link'
// import { useRouter } from 'next/router'
// import { signIn } from 'next-auth/react'
// import imagenInicio from '../public/fondo.png'
// import imagenLogo from '../assets/img/logo_azul.png'
// import imagenToyota from '../assets/img/toyota.png'


// export default function Login() {

//   const router = useRouter()
//   const [authState, setAuthState] = useState({
//     username: '',
//     password: ''
//   })
//   const [pageState, setPageState] = useState({
//     error: '',
//     processing: false
//   })
//   const handleFielChange = (e) => {
//     setAuthState(old => ({ ...old, [e.target.id]: e.target.value }))
//   }

//   const handleAuth = async () => {
//     setPageState(old => ({ ...old, processing: true, error: '' }))
//     signIn('credentials', {
//       ...authState,
//       redirect: false
//     }).then(response => {
//       console.log(response)
//       if (response.ok) {
//         router.push("Desktop/nuevaOrden")
//       } else {
//         setPageState(old => ({ ...old, processing: false, error: response.error }))
//       }
//     }).catch(error => {
//       console.log(error)
//       setPageState(old => ({ ...old, processing: false, error: error.message ?? "Something went wrong!" }))
//     })
//   }


//   //////////////// VALIDATE USERS ////////////////

//   const sentData = async () => {
//     var data = new FormData();
//     data.append("username", "want");
//     data.append("password", "123");

//     fetch("https://slogan.com.bo/vulcano/users/loginMobile", {
//       method: 'POST',
//       body: data,
//     })
//       .then(response => response.json())
//       .then(data => {
//         console.log(data)
//       },
//         (error) => {
//           console.log(error)
//         }
//       )

//   }



//   ///////////////// VALIDATIONS INPUT ///////////////

//   const validationSchema = Yup.object().shape({
//     username: Yup.string()
//       .required('Username is required'),
//     password: Yup.string()
//       .min(3, "Passaword must be at least 3 characters")
//       .required("Password is required")
//   })

//   const formOptions = { resolver: yupResolver(validationSchema) };
//   const { register, handleSubmit, reset, formState } = useForm(formOptions);
//   const { errors } = formState;

//   function onSubmit(data) {
//     alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
//     return false;
//   }

//   return (
//     // <div className="w-screen h-screen flex flex-row">
//       <div className="flex grid grid-cols-12 h-screen overflow-auto">

//         <div className="col-span-12 md:col-span-12 lg:col-span-8 bg-[#3682F7]-500 hidden md:hidden lg:flex  relative">
//           <Image
//             src={imagenInicio}
//             layout="fill" 
//             objectFit="cover"
//             className=" opacity-10"
//             alt='imageninicio'
//           />
//         </div>

//         <div className="col-span-12 md:col-span-12 lg:col-span-4 justify-center">
//           <div className="w-full h-2/5 flex justify-center items-center md:items-center lg:items-end pb-16" >
//             <Image
//               src={imagenLogo}
//               layout='fixed'
//               alt='imagenlogo'
//             />
//           </div>
//           <div className="w-full h-2/5 flex flex-col justify-center items-center">
//             <form onSubmit={handleSubmit(onSubmit)}>
//               <div className="w-2/3 h-full">
//                 <form className="w-full h-full flex flex-col justify-start items-start">
//                   <div className="form-group ml-[14px] ">
//                     <label form="first" className="text-[14px] mb-1 text-[#3A4567]">Usuario</label>
//                     <input 
//                     id="username" 
//                     name="username" 
//                     type='text' 
//                     className={`form-control ${errors.username ? 'is-invalid' : ''} w-[340px] h-[48px] rounded-[7px] bg-[#F6F6FA] border-[1px] border-[#E4E7EB] mb-3 px-3`}
//                       {...register('username')}
//                       //keyboardType="string"
//                       //placeholder='Descripción'
//                       value={authState.username}
//                       onChange={handleFielChange} />
//                     <div className="invalid-feedback">{errors.username?.message}</div>
//                   </div>

//                   <div className="form-group ml-[14px] ">
//                     <label form="last" className="text-[14px] mb-1 text-[#3A4567]">Password</label>
//                     <input id="password" name="password" className={`form-control ${errors.password ? 'is-invalid' : ''} w-[340px] h-[48px] rounded-[7px] bg-[#F6F6FA] border-[1px] border-[#E4E7EB] mb-3 px-3`}
//                       {...register('password')}
//                       //keyboardType="number"
//                       //placeholder='Descripción'
//                       value={authState.password}
//                       onChange={handleFielChange} />
//                     <div className="invalid-feedback">{errors.password?.message}</div>
//                   </div>
//                   <button className="w-[340px] h-[48px] ml-[14px] mt-[15px] rounded-[7px] bg-[#3682F7] border-0 text-[#FFFFFF] text-[14PX]"
//                     //onClick={sentData}
//                     disabled={pageState.processing}
//                     onClick={handleAuth}
//                     type="submit">
//                     Ingresar
//                   </button>
//                 </form>
//               </div>
//             </form>
//           </div>
//           <div className="w-full h-1/5 flex flex-row justify-center items-end">
//             <div className="w-full h-2/3 flex flex-row justify-center items-center">
//               <p className="text-xs text-[#3A4567]">
//                 Desarrollado por&nbsp;
//               </p>
//               <Link href='http://www.wantpublicidad.com/'>
//                 <a className="text-xs text-[#3682F7]-700" target="_blank">
//                   WANT Digital Agency
//                 </a>
//               </Link>
//             </div>
//           </div>
//         </div>
//       </div>
//     // </div>
//   )
// }


import Head from 'next/head'
import Image from 'next/image'
import React, { useState} from 'react'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as Yup from 'yup'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { signIn} from 'next-auth/react'
import imagenInicio from '../public/fondo.png'
import imagenLogo from '../assets/img/logo_azul.png'
import imagenToyota from '../assets/img/toyota.png'

import Spinner from 'react-activity/dist/Spinner'
import 'react-activity/dist/Spinner.css'


export default function Login() {

  const router = useRouter()
  
  const [authState, setAuthState] = useState({
    username: '',
    password: ''
  })
  const [pageState, setPageState] = useState({
    error: '',
    processing: false
  })
  const handleFielChange = (e) => {
    setAuthState(old => ({ ...old, [e.target.id]: e.target.value}))
  }

  const handleAuth = async () => {
    setPageState(old => ({...old, processing: true, error: ''}))
    signIn('credentials', {
      ...authState,
      redirect: false
    })
    .then(response => {
      console.log(response)
      if (response.ok) {
        // router.push("Desktop/nuevaOrden")
        //router.push('/')
        router.push(response.url)
      } else {
        setPageState(old => ({ ...old, processing: false, error: "⚠️" + response.error}))
      }
    })
    .catch(error => {
      console.log(error)
      setPageState(old => ({...old, processing: false, error: error.message ?? "⚠️" + "Something went wrong!"}))
    })
  }


  ///////////////// VALIDATIONS INPUT ///////////////
  
  const validationSchema = Yup.object().shape({
    username: Yup.string()
    .required('Username is required'),
    password: Yup.string()
    // .min(3, "Passaword must be at least 3 characters")
    .required("Password is required")
})

const formOptions = { resolver: yupResolver(validationSchema)};
const {register, handleSubmit, reset, formState } = useForm(formOptions);
const { errors } = formState;

function onSubmit(data) {
    //alert('SUCCESS!! :-)\n\n' + JSON.stringify(data, null, 4));
    handleAuth();
    return false;
}



  return (
    <div className="w-screen h-screen flex flex-row">
      <div className="w-3/5 bg-[#3682F7]-500  relative">
        <Image
          src={imagenInicio}
          layout="fill" objectFit="cover"
          className=" opacity-10"
          alt='imageninicio'
        />

      </div>
    <div className="w-2/5 flex flex-col justify-center">

      <div className="w-full h-2/5 flex flex-row justify-center items-end pb-16" >
        <Image
          src={imagenLogo}
          layout='fixed'
          alt='imagenlogo'
        />
      </div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="w-full h-2/5 flex flex-col justify-center items-center">
          <div className="w-2/3 h-full">
              {
                pageState.processing ? <Spinner color="#bebebe" size={17} speed={1} animating={true} style={{marginLeft: 'auto', marginRight: 'auto'}} /> : 
                (
                  <div>
                    <div className='text-[#ff0000] text-center pb-4 font-bold'>
                      {
                        pageState.error 
                      }
                    </div>
                    <form className="w-full h-full flex flex-col justify-start items-start">
                      <label htmlFor="first" className="text-xs mb-1 text-[#3A4567]">Username</label>
                      <input id="username" name="username" type='text' className={`form-control w-full h-10 rounded-lg bg-slate-200 border-1 border-slate-400 mb-3 px-3`}
                        //keyboardType="string"
                        //placeholder='Descripcion'
                        {...register('username')} 
                        value={authState.username}
                        onChange={handleFielChange} />
                      <div className=" w-full flex flex-row justify-center items-center">

                      </div>
                      <label htmlFor="last" className="text-xs mb-1 text-[#3A4567]">Password</label>
                      <input id="password" name="password" className={`form-control w-full h-10 rounded-lg bg-slate-200 border-1 border-slate-400 mb-3 px-3`} 
                        //keyboardType="number"
                        //placeholder='Descripcion'
                      {...register('password')} 
                        value={authState.password}
                        onChange={handleFielChange} />
                      <button disabled={pageState.processing} type="button" className="w-full h-10 rounded-lg bg-[#3682F7] border-0 text-[#FFFF] text-xs" onClick={handleAuth}>
                              Ingresar
                      </button>
                    </form>
                  </div>
                )
            }
          
          </div>
        </div>
      </form>
      <div
        className="w-full h-1/5 flex flex-row justify-center items-end"
      >
        <div
          className="w-full h-2/3 flex flex-row justify-center items-center "
        >
          <p
            className="text-xs text-[#3A4567]"
          >
            Desarrollado por&nbsp;
          </p>
          <Link
            href='http://www.wantpublicidad.com/'
          >
            <a
              className="text-xs text-[#3682F7]-700" target="_blank"
            >
              WANT Digital Agency
            </a>
          </Link>
        </div>
      </div>
    </div>

  </div>
  )
}



import React, { useEffect, useState } from "react";
import Spinner from "react-activity/dist/Spinner";
import "react-activity/dist/Spinner.css";

import Layout from "../../components/layout";
import ModalNewUsers from "../../components/users/modal";
import TableDatosUsers from "../../components/users/table";


function DeskUsers() {


  return (
    <div className="mt-[52px] ml-[20px] mr-[20px] ">
      <div className="grid grid-cols-12">
        <div className="col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between">
          <div className="text-[24px] md:text-[32px] lg:text-[32px] text-[#000000] font-semibold self-center">
            Users
          </div>
          <div className="self-center">
            <ModalNewUsers/>
            {/* <ModalAñadir getDatos={getDatos} /> */}
          </div>
        </div>
      </div>

      <div className="grid grid-cols-12 mt-[22px]">
        <div className=" md:col-span-4 lg:col-span-4"></div>
        <div className="col-span-12 md:col-span-8 lg:col-span-8 h-[40px] flex flex-row">
          <input className="w-full h-full rounded-lg bg-[#F6F6FA] border pl-[10px] pr-[10px] " />
          <div className="flex justify-center items-center">
            <div className="flex justify-center items-center w-[40px] h-[40px] hover:bg-[#3682F7] flex flex-col rounded-lg border-[#3682F7] border-[1px] ml-[10px] ">
              <button>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 24 24"
                  strokeWidth={1.5}
                  stroke="currentColor"
                  className="w-6 h-6  text-[#3682F7]  hover:text-[#fff]"
                >
                  <path
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
                  />
                </svg>
              </button>
            </div>
          </div>
        </div>
      </div>
      <div className="grid grid-cols-12 mt-[22px]">
        <div className="col-span-12 md:col-span-12 ">
          {/* <TableDatosClients reloadClients={reloadClients} setReloadClients={setReloadClients}/> */}
          <TableDatosUsers/>
        </div>
      </div>
    </div>
  );
}
DeskUsers.auth = true;
DeskUsers.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};

export default DeskUsers;

import NextAuth from "next-auth"
import CredentialsProvider from "next-auth/providers/credentials"

export const authOptions = {
    pages: {
      signIn: "/login"
    },
  secret: process.env.NEXT_PUBLIC_AUTH_SECRET,
  // site: process.env.NEXTAUTH_URL,
  // @link https://next-auth.js.org/configuration/providers
  providers: [
    CredentialsProvider({
      // The name to display on the sign in form (e.g. 'Sign in with...')
      name: 'Credentials',
      // The credentials is used to generate a suitable form on the sign in page.
      // You can specify whatever fields you are expecting to be submitted.
      // e.g. domain, username, password, 2FA token, etc.
      // You can pass any HTML attribute to the <input> tag through the object.
      credentials: {
        username: { label: "Username", type: "text", placeholder: "Username" },
        password: {  label: "Password", type: "password", placeholder: "Password" }
      },
      async authorize(credentials, req) {
        // You need to provide your own logic here that takes the credentials
        // submitted and returns either a object representing a user or value
        // that is false/null if the credentials are invalid.
        // e.g. return { id: 1, name: 'J Smith', email: 'jsmith@example.com' }
        // You can also use the `req` object to obtain additional parameters
        // (i.e., the request IP address)

        const res = await fetch("https://slogan.com.bo/vulcano/users/loginMobile", {
          method: 'POST',
          body: JSON.stringify(credentials),
          headers: { "Content-Type": "application/json" }
        })
        // const user = await res.json()
  
        const response = await res.json()

        // if (!response.status) {
        //   throw new Error(response.error);
        // }

        if(response.status){
          return response.data
        } else {
          return null
        }

      },
      // session: { jwt: true },
      // callbacks: {
      //   jwt: async ({ token, user }) => {
      //     if (user) {
      //       token.role = user.role
      //     }
      
      //     return token
      //   },
      //   session: async ({ session, token }) => {
      //     session.user.role = token.role
      //     return session
      //   },
      // }
    }),
  ],
  session: { jwt: true },
  callbacks: {
    jwt: async ({ token, user }) => {
      if (user) {
        token.role = user.role
      }
  
      return token
    },
    session: async ({ session, token }) => {
      session.user.role = token.role
      return session
    },
    async redirect({ url, baseUrl }) {
      // return url
      return baseUrl
    }
  },
}
export default NextAuth(authOptions)
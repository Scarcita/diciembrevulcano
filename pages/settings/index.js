import React, { useState, useEffect } from "react";
import Image from "next/image";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import Layout from "../../components/layout";
import TableCompanyInf from "../../components/settings/tableCompanyInf";


export default function DeskSettings() {


  return (
    <div className="mt-[52px] ml-[20px] mr-[20px]">
      <div className="grid grid-cols-12 justify-left mt-10">
        <div className="col-span-12 md:col-span-12 lg:col-span-12">
          <Tabs>
            <TabList className="flex flex-row w-full  md:w-[300px] lg:w-[320px] bg-[#E4E7EB] focus:outline-none rounded-t-lg h-8">
              <Tab className="flex justify-center items-center text-[12px] focus:text-[#fff] focus:bg-[#3682F7] rounded-tl-lg bg-[#E4E7EB] px-2">
                Company Inf.
              </Tab>
              <Tab className="text-gray-500 text-[12px] flex justify-center items-center focus:bg-[#3682F7] focus:text-[#fff]  px-2 py-2">
                Cars Catalog
              </Tab>
              <Tab className="text-gray-500 text-[12px] flex justify-center items-center focus:bg-[#3682F7] focus:text-[#fff]  px-2 py-2">
                Colors Catalog
              </Tab>
            </TabList>
            <TabPanel>
              <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                  <TableCompanyInf/>
                </div>
              </div>
            </TabPanel>

            <TabPanel>
              <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                  {/* <TablaIngresos /> */}
                </div>
              </div>
            </TabPanel>

            <TabPanel>
              <div className="grid grid-cols-12">
                <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
                  {/* <TablaSalidasOt /> */}
                </div>
              </div>
            </TabPanel>
          </Tabs>
        </div>
      </div>
    </div>
  );
}
DeskSettings.auth = true;
DeskSettings.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};
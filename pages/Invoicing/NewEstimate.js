import React, { useState, useEffect } from "react";
import Layout from "../../components/layout";

import Cliente from "../../components/Invoicing/NewEstimate/DatosClientes";
import Vehiculo from "../../components/Invoicing/NewEstimate/DatosVehiculos";
import Servicios from "../../components/Invoicing/NewEstimate/Servicios";
import ListaServicios from "../../components/Invoicing/NewEstimate/lado derecho/ListaServicios";
import TableProductos from "../../components/Invoicing/NewEstimate/tableProductos";
import PresupuestoProducto from "../../components/Invoicing/NewEstimate/presupuestoProducto";

function PantallaPresupuesto() {
  const [servicesList, setServicesList] = useState([]);
  const [productsList, setProductsList] = useState([]);
  const [selectedItem, setSelectedItem] = useState(null);
  const [busqueda, setBusqueda] = useState("");
  const [dataColumnaDerecha, setDataColumnaDerecha] = useState([]);
  const [precioSugerido, setPrecioSugerido] = useState(0);

  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState([]);

  const getDatos = () => {
    setIsLoading(true);
    fetch("https://slogan.com.bo/vulcano/products/all")
      .then((response) => response.json())
      .then((data) => {
        if (data.status) {
          console.log(data.data);
          setData(data.data);
        } else {
          console.error(data.error);
        }
        setIsLoading(false);
      });
  };
  useEffect(() => {
    getDatos();
  }, []);

  const removeFromCart = (indextoBeremoved) => {
    setDataColumnaDerecha((products) =>
      products.filter((_, index) => index !== indextoBeremoved)
    );
    //setDataColumnaDerecha();
  };

  return (
    <div className="mt-[20px] ml-[20px] mr-[20px]">
      <div className="grid grid-cols-12 gap-4 ">
        <div className="col-span-12 md:col-span-12 lg:col-span-8">
          <div className="grid grid-cols-12 gap-5 ">
            <div className="col-span-12 md:col-span-12 lg:col-span-12 ">
              <div className="text-[24px] md:text-[32px] lg:text-[32px] text-[#000000]">
                Presupuesto de productos y servicios
              </div>
            </div>
            <div className="col-span-12 md:col-span-6 lg:col-span-6 justify-center items-center">
              <Vehiculo />
            </div>
            <div className="col-span-12 md:col-span-6 lg:col-span-6 justify-center items-center">
              <Cliente />
            </div>
          </div>
          <div className="col-span-12 md:col-span-12 lg:col-span-12 mt-[20px]">
            <Servicios
              precioSugerido={precioSugerido}
              setPrecioSugerido={setPrecioSugerido}
              selectedItem={selectedItem}
              setSelectedItem={setSelectedItem}
              servicesList={servicesList}
              setServicesList={setServicesList}
            />
          </div>
          <div className="col-span-12 md:col-span-12 lg:col-span-12  mt-[20px]">
            {/* <Productos
                            precioSugerido={precioSugerido}
                            setPrecioSugerido={setPrecioSugerido}
                            setDataColumnaDerecha={setDataColumnaDerecha}
                            busqueda={busqueda}
                            setBusqueda={setBusqueda}
                            productsList={productsList}
                            setProductsList={setProductsList}
                        /> */}

            <TableProductos
              isLoading={isLoading}
              setIsLoading={setIsLoading}
              getDatos={getDatos}
              data={data}
              setDataColumnaDerecha={setDataColumnaDerecha}
            />
          </div>
        </div>

        <div className="col-span-12 md:col-span-12 lg:col-span-4">
          <div className="grid grid-cols-12">
            <div className="col-span-12 md:col-span-12 lg:col-span-12 mb-[20px]">
              <div className="text-[24px] md:text-[32px] lg:text-[32px] text-[#000000]">
                Presupuesto
              </div>
            </div>
            <div className="col-span-12 md:col-span-12 lg:col-span-12">
              <p className="text-[18px] text-[#3682F7] mb-[10px]">Servicios</p>
              <ListaServicios
                precioSugerido={precioSugerido}
                setPrecioSugerido={setPrecioSugerido}
                className="mt-2"
                selectedItem={selectedItem}
                setSelectedItem={setSelectedItem}
                servicesList={servicesList}
                setServicesList={setServicesList}
              />
            </div>

            <div className="col-span-12 md:col-span-12 lg:col-span-12">
              {/* <ListaProductos
                                precioSugerido={precioSugerido}
                                setPrecioSugerido={setPrecioSugerido}
                                dataColumnaDerecha={dataColumnaDerecha}
                                setDataColumnaDerecha={setDataColumnaDerecha}
                                removeFromCart={removeFromCart}
                                busqueda={busqueda}
                                setBusqueda={setBusqueda}
                            /> */}
              <p className="text-[18px] text-[#3682F7] mb-[10px]">Productos</p>

              <PresupuestoProducto
                getDatos={getDatos}
                data={data}
                dataColumnaDerecha={dataColumnaDerecha}
                precioSugerido={precioSugerido}
                setPrecioSugerido={setPrecioSugerido}
                setDataColumnaDerecha={setDataColumnaDerecha}
                removeFromCart={removeFromCart}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
PantallaPresupuesto.auth = true;
PantallaPresupuesto.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};

export default PantallaPresupuesto;

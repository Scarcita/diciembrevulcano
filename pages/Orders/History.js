import Layout from "../../components/layout";

import Datos from "../../components/Orders/History/Datos";

function pantallaHistorial() {
  return (
    <div className="mt-[10px] ml-[20px] mr-[20px] bg-[#F6F6FA]">
      <p className=" text-[23.57px]">
        <span className="font-bold text-[23.57px]">Historial de vehículo</span>/
        5557828DH32738SHSB
      </p>
      <div className="flex grid grid-cols-12 gap-6 justify-center items-center mt-[20px]">
        <div className="col-span-12 md:col-span-12 lg:col-span-3">
          {/*    ESPACIO PARA LA BARRA IZQUIERDA   */}
        </div>
        <div className="col-span-12 md:col-span-12 lg:col-span-1 flex flex-col ">
          <div className="flex justify-center items-center">
            <p>
              <span className="font-bold text-[15px]">OT</span>
              #7717
            </p>
          </div>
          <div className="flex bg-[#FFFF] rounded-2xl flex-col justify-center shadow-md justify-center items-center">
            <div className="flex bg-[#FF0000] w-full h-[26px] rounded-t-2xl  justify-center ">
              <div className="flex-col flex justify-center items-center">
                <p className="text-[#FFFF] text-[13px]">LUN</p>
              </div>
            </div>
            <div className="flex-col flex justify-center items-center ">
              <p className="font-bold text-[37px] mt-[-8px]">15</p>
              <p className="font-bold text-[16px] mt-[-12px]">AGO</p>
            </div>
          </div>
          <div className="flex justify-center items-center">
            <p className="mt-[8px] text-[15px] font-medium">09:55</p>
          </div>
        </div>
        <div className="col-span-12 md:col-span-12 lg:col-span-6 bg-[#FFFF] lg:h-[120px] md:h-[120px] h-[190px] rounded-xl shadow-md">
          <Datos />
        </div>
      </div>
      <div className="flex grid grid-cols-12 gap-6 justify-center items-center mt-[20px]">
        <div className="col-span-12 md:col-span-12 lg:col-span-3">
          {/*    ESPACIO PARA LA BARRA IZQUIERDA   */}
        </div>
        <div className="col-span-12 md:col-span-12 lg:col-span-1 flex flex-col ">
          <div className="flex justify-center items-center">
            <p>
              <span className="font-bold text-[15px]">OT</span>
              #7717
            </p>
          </div>
          <div className="flex bg-[#FFFF] rounded-2xl flex-col justify-center shadow-md justify-center items-center">
            <div className="flex bg-[#FF0000] w-full h-[26px] rounded-t-2xl  justify-center ">
              <div className="flex-col flex justify-center items-center">
                <p className="text-[#FFFF] text-[13px]">LUN</p>
              </div>
            </div>
            <div className="flex-col flex justify-center items-center ">
              <p className="font-bold text-[37px] mt-[-8px]">15</p>
              <p className="font-bold text-[16px] mt-[-12px]">AGO</p>
            </div>
          </div>
          <div className="flex justify-center items-center">
            <p className="mt-[8px] text-[15px] font-medium">09:55</p>
          </div>
        </div>
        <div className="col-span-12 md:col-span-12 lg:col-span-6 bg-[#FFFF] lg:h-[120px] md:h-[120px] h-[190px] rounded-xl shadow-md">
          <Datos />
        </div>
      </div>
      <div className="flex grid grid-cols-12 gap-6 justify-center items-center mt-[20px]">
        <div className="col-span-12 md:col-span-12 lg:col-span-3">
          {/*    ESPACIO PARA LA BARRA IZQUIERDA   */}
        </div>
        <div className="col-span-12 md:col-span-12 lg:col-span-1 flex flex-col ">
          <div className="flex justify-center items-center">
            <p>
              <span className="font-bold text-[15px]">OT</span>
              #7717
            </p>
          </div>
          <div className="flex bg-[#FFFF] rounded-2xl flex-col justify-center shadow-md justify-center items-center">
            <div className="flex bg-[#FF0000] w-full h-[26px] rounded-t-2xl  justify-center ">
              <div className="flex-col flex justify-center items-center">
                <p className="text-[#FFFF] text-[13px]">LUN</p>
              </div>
            </div>
            <div className="flex-col flex justify-center items-center ">
              <p className="font-bold text-[37px] mt-[-8px]">15</p>
              <p className="font-bold text-[16px] mt-[-12px]">AGO</p>
            </div>
          </div>
          <div className="flex justify-center items-center">
            <p className="mt-[8px] text-[15px] font-medium">09:55</p>
          </div>
        </div>
        <div className="col-span-12 md:col-span-12 lg:col-span-6 bg-[#FFFF] lg:h-[120px] md:h-[120px] h-[190px] rounded-xl shadow-md">
          <Datos />
        </div>
      </div>
      <div className="flex grid grid-cols-12 gap-6 justify-center items-center mt-[20px]">
        <div className="col-span-12 md:col-span-12 lg:col-span-3">
          {/*    ESPACIO PARA LA BARRA IZQUIERDA   */}
        </div>
        <div className="col-span-12 md:col-span-12 lg:col-span-1 flex flex-col ">
          <div className="flex justify-center items-center">
            <p>
              <span className="font-bold text-[15px]">OT</span>
              #7717
            </p>
          </div>
          <div className="flex bg-[#FFFF] rounded-2xl flex-col justify-center shadow-md justify-center items-center">
            <div className="flex bg-[#FF0000] w-full h-[26px] rounded-t-2xl  justify-center ">
              <div className="flex-col flex justify-center items-center">
                <p className="text-[#FFFF] text-[13px]">LUN</p>
              </div>
            </div>
            <div className="flex-col flex justify-center items-center ">
              <p className="font-bold text-[37px] mt-[-8px]">15</p>
              <p className="font-bold text-[16px] mt-[-12px]">AGO</p>
            </div>
          </div>
          <div className="flex justify-center items-center">
            <p className="mt-[8px] text-[15px] font-medium">09:55</p>
          </div>
        </div>
        <div className="col-span-12 md:col-span-12 lg:col-span-6 bg-[#FFFF] lg:h-[120px] md:h-[120px] h-[190px] rounded-xl shadow-md">
          <Datos />
        </div>
      </div>
      <div className="flex grid grid-cols-12 gap-6 justify-center items-center mt-[20px]">
        <div className="col-span-12 md:col-span-12 lg:col-span-3">
          {/*    ESPACIO PARA LA BARRA IZQUIERDA   */}
        </div>
        <div className="col-span-12 md:col-span-12 lg:col-span-1 flex flex-col ">
          <div className="flex justify-center items-center">
            <p>
              <span className="font-bold text-[15px]">OT</span>
              #7717
            </p>
          </div>
          <div className="flex bg-[#FFFF] rounded-2xl flex-col justify-center shadow-md justify-center items-center">
            <div className="flex bg-[#FF0000] w-full h-[26px] rounded-t-2xl  justify-center ">
              <div className="flex-col flex justify-center items-center">
                <p className="text-[#FFFF] text-[13px]">LUN</p>
              </div>
            </div>
            <div className="flex-col flex justify-center items-center ">
              <p className="font-bold text-[37px] mt-[-8px]">15</p>
              <p className="font-bold text-[16px] mt-[-12px]">AGO</p>
            </div>
          </div>
          <div className="flex justify-center items-center">
            <p className="mt-[8px] text-[15px] font-medium">09:55</p>
          </div>
        </div>
        <div className="col-span-12 md:col-span-12 lg:col-span-6 bg-[#FFFF] lg:h-[120px] md:h-[120px] h-[190px] rounded-xl shadow-md">
          <Datos />
        </div>
      </div>
      <div className="flex grid grid-cols-12 gap-6 justify-center items-center mt-[20px]">
        <div className="col-span-12 md:col-span-12 lg:col-span-3">
          {/*    ESPACIO PARA LA BARRA IZQUIERDA   */}
        </div>
        <div className="col-span-12 md:col-span-12 lg:col-span-1 flex flex-col ">
          <div className="flex justify-center items-center">
            <p>
              <span className="font-bold text-[15px]">OT</span>
              #7717
            </p>
          </div>
          <div className="flex bg-[#FFFF] rounded-2xl flex-col justify-center shadow-md justify-center items-center">
            <div className="flex bg-[#FF0000] w-full h-[26px] rounded-t-2xl  justify-center ">
              <div className="flex-col flex justify-center items-center">
                <p className="text-[#FFFF] text-[13px]">LUN</p>
              </div>
            </div>
            <div className="flex-col flex justify-center items-center ">
              <p className="font-bold text-[37px] mt-[-8px]">15</p>
              <p className="font-bold text-[16px] mt-[-12px]">AGO</p>
            </div>
          </div>

          <div className="flex justify-center items-center">
            <p className="mt-[8px] text-[15px] font-medium">09:55</p>
          </div>
        </div>
        <div className="col-span-12 md:col-span-12 lg:col-span-6 bg-[#FFFF] lg:h-[120px] md:h-[120px] h-[190px] rounded-xl shadow-md">
          <Datos />
        </div>
      </div>
    </div>
  );
}
pantallaHistorial.auth = true;

pantallaHistorial.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};
export default pantallaHistorial;

import Image from "next/image";

import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import Layout from "../../components/layout";

// import imagenIndicadorDrecha from "../../../public/derecha.svg";
import imagenIndicadorDrecha from "../../public/derecha.svg";

import BarraAzul from "../../components/Orders/Detail/BarraAzul";
import TablaDatosCliente from "../../components/Orders/Detail/TablaDatosClientes";
import TablaRespuestosRequeridos from "../../components/Orders/Detail/TablaRespuestosRequeridos";
import TablaServiciosRequeridos from "../../components/Orders/Detail/TablaServiciosRequeridos";
import DatosDelAuto from "../../components/Orders/Detail/DatosDelAuto";

function PantallaCinco() {
  return (
    <div className="w-full h-screen flex flex-col justify-start items-center bg-[#F6F6F6] overflow-auto">
      <div className="w-full flex flex-row justify-center items-center px-4 my-5">
        <div className="w-1/2 h-full flex flex-row justify-start items-center">
          <div className="w-8 h-full flex flex-row justify-start items-center">
            <Image
              src={imagenIndicadorDrecha}
              layout="fixed"
              alt="imagenIndicadorDrecha"
            />
          </div>

          <div className="w-full h-full flex flex-row justify-start items-center text-[#000000]">
            <p className="text-2xl font-Dosis text-left">Orden # 44427</p>
          </div>
        </div>

        <div className="w-1/2 h-full flex flex-row justify-end items-center">
          <div className="w-full h-full flex flex-col justify-center items-end">
            <div className="w-full h-1/2 flex flex-row justify-end items-end text-[#000000]">
              <p className="text-xm font-Dosis"> Juan Camacho A.</p>
            </div>
            <div className="w-full h-1/2 flex flex-row justify-end items-start ">
              <p className="text-[#C7CDD4]">ASESOR DE SERVICIO</p>
            </div>
          </div>
          <div className="w-32 h-full flex flex-row justify-center items-center">
            <div className="w-16 h-16 rounded-full bg-[#E4E7EB]"></div>
          </div>
        </div>
      </div>

      <div className="w-full grid grid-cols-12 gap-4">
        <div className="lg:h-full mr-5 ml-5 col-span-12 md:col-span-12 lg:col-span-8 m-8 bg-[#F6F6FA]">
          <div className="flex mb-10">
            <BarraAzul />
          </div>

          <div className="w-full lg:h-[130px]">
            <TablaDatosCliente />
          </div>

          <div className="grid grid-cols-12 ">
            <div className="col-span-12 md:col-span-12">
              <Tabs className="col-span-12 md:col-span-12">
                <TabList className="flex flex-row w-full focus:outline-none">
                  <Tab className="w-1/4 h-8 flex justify-center items-center text-[12px] text-gray-500 focus:text-[#3682F7] border-white-500 border-2 px-2 focus:bg-transparent bg-[#E4E7EB]">
                    {" "}
                    Requeridos
                  </Tab>
                  <Tab className="text-gray-500 w-1/4 h-8 text-[12px] flex justify-center items-center px-2">
                    Observaciones de Cliente
                  </Tab>
                </TabList>
                <TabPanel className="bg-[#FFFF] col-span-12 md:col-span-12 overflow-auto">
                  <TablaServiciosRequeridos />
                </TabPanel>
                <TabPanel>
                  <h2>Any content 2</h2>
                </TabPanel>
              </Tabs>
            </div>
          </div>

          <div className="flex justify-end px-5 ">
            <button className=" w-[100px] h-8 bg-[#3682F7] border-[#3682F7] border-2 rounded-lg text-[13px] text-[#fff] mt-2 mb-2">
              Añadir
            </button>
          </div>

          <div className="grid grid-cols-12  ">
            <div className="col-span-12 md:col-span-12">
              <Tabs>
                <TabList className="flex flex-row w-full focus:outline-none">
                  <Tab className="w-1/4 h-8 flex justify-center items-center text-[12px] text-gray-500 focus:text-[#3682F7] border-white-500 border-2 px-2 focus:bg-transparent bg-[#E4E7EB]">
                    Servicios Requeridos
                  </Tab>
                  <Tab className="text-gray-500 w-1/4 h-8 text-[12px] flex justify-center items-center px-2">
                    Observaciones de Cliente
                  </Tab>
                </TabList>
                <TabPanel className="bg-[#FFFF] col-span-12 md:col-span-12 overflow-auto">
                  <TablaRespuestosRequeridos />
                </TabPanel>
                <TabPanel>
                  <h2>Any content 2</h2>
                </TabPanel>
              </Tabs>
            </div>
          </div>
        </div>

        <div className="mr-5 ml-5 col-span-12 md:col-span-12 lg:col-span-4 bg-[#3682F7]">
          <DatosDelAuto />
        </div>
      </div>
    </div>
  );
}

PantallaCinco.auth = true;
PantallaCinco.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};
export default PantallaCinco;

import React, { useEffect, useState } from "react";
import { Dots } from "react-activity";

import Layout from "../../components/layout";

import DatosVehiculos from "../../components/Orders/New/DatosVehiculos";
import DatosClientes from "../../components/Orders/New/DatosClientes";

import Observaciones from "../../components/Orders/New/Observaciones";
import Servicios from "../../components/Orders/New/Servicios";

import DetallesContacto from "../../components/Orders/New/DetallesContacto";
import DetallesEntrega from "../../components/Orders/New/DetallesEntrega";
import Kilometraje from "../../components/Orders/New/Kilometraje";
import Comentarios from "../../components/Orders/New/Comentarios";
import { comment } from "postcss";

// import ListObservacionesCli from "../../components/Orders/New/listObservaciones";
// import ListServiceReq from "../../components/Orders/New/listServiciosReq";
// import SearchPlate from "../../components/Orders/New/searchPlate";
// import SearchName from "../../components/Orders/New/searchName";
// import SearchVin from "../../components/Orders/New/searchVin";
// import ListServices from "../../components/Orders/New/listServices";

function NuevaOrden() {
  // const [value, onChange] = useState(new Date());

  //const [valueTime, onChangeTime] = useState(new Date());

  // const [marca, setMarca] = useState("");
  // const [modelo, setModelo] = useState("");
  // const [version, setVersion] = useState("");
  // const [color, setColor] = useState("");
  // const [kilometraje, setKilometraje] = useState("");
  // const [transmision, setTransmision] = useState("");
  // const [email, setEmail] = useState("");
  // const [phone, setPhone] = useState("");
  // const [nit, setNit] = useState("");
  // const [nameTwo, setNameTwo] = useState("");
  // const [date, setDate] = useState("");
  // const [time, setTime] = useState("");

  // const [placa, setPlaca] = useState("");
  // const [name, setName] = useState("");
  // const [vin, setVin] = useState("");

  /////////////// LLAMADA SERVIDOR POST ///////////

  // const [showDots, setShowDots] = useState(true);
  // const [isLocked, setIsLocked] = useState(false);

  // const [dataAdd, setAddData] = useState("");
  // const [carId, setCarId] = useState();
  // const [km, setKm] = useState();
  // const [clientId, setClientId] = useState();
  // const [createdBy, setCreatedBy] = useState(1);
  // const [contactName, setContactName] = useState("");
  // const [contactPhone, setContactPhone] = useState("");
  // const [observations, setObservations] = useState("");
  // const [deliveryPromise, setDeliveryPromise] = useState("");

  // function onSubmit(data) {
  //   if (typeof placa === "string" && placa.length === 0) {
  //     // console.log('string is empty');
  //     return false;
  //   }

  //   if (typeof name === "string" && name.length === 0) {
  //     // console.log('string is empty');
  //     return false;
  //   }

  //   if (typeof vin === "string" && vin.length === 0) {
  //     // console.log('string is empty');
  //     return false;
  //   }

  //   alert("SUCCESS!! :-)\n\n" + JSON.stringify(data, null, 4));
  //   // console.log('string is NOT empty')
  //   getDataOrden();
  //   return false;
  // }

  ////////////////// POST ////////////////////////

  // const getDataOrden = async () => {
  //   var data = new FormData();
  //   console.warn(carId);
  //   console.warn(contactName);
  //   console.warn(contactPhone);

  //   data.append("car_id", carId);
  //   data.append("km", km);
  //   data.append("client_id", clientId);
  //   data.append("created_by", createdBy);
  //   data.append("contact_name", contactName);
  //   data.append("contact_phone", contactPhone);
  //   data.append("observations", observations);
  //   data.append("delivery_promise", deliveryPromise);

  //   fetch("https://slogan.com.bo/vulcano/orders/addMobile", {
  //     method: "POST",
  //     body: data,
  //   })
  //     .then((response) => response.json())
  //     .then(
  //       (data) => {
  //         // console.log('VALOR ENDPOINTS: ', data);
  //         setAddData(data);
  //       },
  //       (error) => {
  //         // console.log(error)
  //       }
  //     );
  // };

  // useEffect(() => {
  //   getDataOrden();
  // }, [getDataOrden]);

  // useEffect(() => {
  //   console.warn(placa);
  // }, [placa]);

  // useEffect(() => {
  //   console.warn("carId: " + carId);
  // }, [carId]);

  // useEffect(() => {
  //   console.warn("ahora name: " + name);
  // }, [name]);
  // useEffect(() => {
  //   console.warn("ahora clientId: " + clientId);
  // }, [clientId]);

  // useEffect(() => {
  //   console.warn("ahora VIIIIIN: " + vin);
  // }, [vin]);
  ///////////// VERIFICACION DE ESPACIOS VACIOSS ///////////////

  const [diaNumeralActual, setDiaActual] = useState(0);
  const [diaTextualActual, setDiaTextualActual] = useState("");
  const [mesNumeralActual, setMesActual] = useState(0);
  const [mesTextualActual, setMesTextualActual] = useState("");
  const [anioActual, setAnioActual] = useState(0);

  const [isDataFilled, setIsDataFilled] = useState(false);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [success, setSuccess] = useState(false);

  const [datosVehiculo, setDatosVehiculo] = useState({
    id: "",
    placa: "",
    vin: "",
    marca: "",
    modelo: "",
    version: "",
    color: "",
    anio: "",
    transmision: "",
  });
  const [datosCliente, setDatosCliente] = useState({
    id: "",
    telefono: "",
    email: "",
    ciNit: "",
    nombre: "",
  });
  const [observaciones, setObservaciones] = useState(null);
  const [servicios, setServicios] = useState(null);
  const [detallesContacto, setDetallesContacto] = useState({
    nombre: "",
    telefono: "",
  });
  const [detallesEntrega, setDetallesEntrega] = useState("");
  const [kilometraje, setKilometraje] = useState("");
  const [comentarios, setComentarios] = useState("");

  useEffect(() => {
    console.log("Datos del vehiculo: ", datosVehiculo);
    console.log("Datos del cliente: ", datosCliente);
    console.log("Observaciones: ", observaciones);
    console.log("Servicios: ", servicios);
    console.log("Detalles de contacto: ", detallesContacto);
    console.log("Detalles de entrega: ", detallesEntrega);
    console.log("Kilometraje: ", kilometraje);
    console.log("Comentarios: ", comentarios);

    if (
      datosVehiculo.id != "" &&
      datosCliente.id != "" &&
      detallesContacto.nombre != "" &&
      detallesContacto.telefono != "" &&
      detallesEntrega != "" &&
      kilometraje != "" &&
      servicios != null
    ) {
      setIsDataFilled(true);
    } else {
      setIsDataFilled(false);
    }
  }, [
    datosVehiculo,
    datosCliente,
    observaciones,
    servicios,
    detallesContacto,
    detallesEntrega,
    kilometraje,
    comentarios,
  ]);

  const sendData = () => {
    setLoading(true);
    const data = new FormData();
    data.append("car_id", datosVehiculo.id);
    data.append("client_id", datosCliente.id);
    data.append("created_by", "1");
    data.append("contact_name", detallesContacto.nombre);
    data.append("contact_phone", detallesContacto.telefono);
    observaciones != null
      ? data.append("client_observations", observaciones)
      : null;
    data.append("delivery_promise", detallesEntrega);
    data.append("km", kilometraje);
    comentarios != "" ? data.append("comments", comentarios) : null;
    data.append("srs", servicios);

    fetch("https://slogan.com.bo/vulcano/orders/addMobile", {
      method: "POST",
      body: data,
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.status == true) {
          console.log("Orden creada con exito: ", data);
          setLoading(false);
          setSuccess(true);
          window.location.reload();

          setTimeout(() => {
            setSuccess(false);
          }, 2000);
        } else {
          console.log("Error al crear orden: ", data);
          setLoading(false);
          setError(true);

          setTimeout(() => {
            setError(false);
          }, 2000);
        }
      })
      .catch((error) => {
        console.error("sendData: ", error);
      });
  };

  const getFechaActual = () => {
    const fechaActual = new Date();
    const diaNumeral = fechaActual.getDate();
    const diaTextual = fechaActual.toLocaleString("es-ES", { weekday: "long" });
    const mesNumeral = fechaActual.getMonth() + 1;
    const mesTextual = fechaActual.toLocaleString("es-ES", { month: "long" });
    const anio = fechaActual.getFullYear();

    setDiaActual(diaNumeral);
    setDiaTextualActual(
      diaTextual.charAt(0).toUpperCase() + diaTextual.slice(1)
    );
    setMesActual(mesNumeral);
    setMesTextualActual(
      mesTextual.charAt(0).toUpperCase() + mesTextual.slice(1)
    );
    setAnioActual(anio);
  };

  useEffect(() => {
    getFechaActual();
  }, []);

  return (
    // <div className="mt-[20px] ml-[20px] mr-[20px]">
    //   <div className="grid grid-cols-12">
    //     <div className="col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between">
    //       <div className="text-[24px] md:text-[32px] lg:text-[32px] text-[#000000] ">
    //         Nueva Orden
    //       </div>
    //       <div className="flex flex-row">
    //         <div className="text-right">
    //           <span className="text-[14px] md:text-[16px] lg:text-[16px] text-[#000000]">
    //             {" "}
    //             Scarleth Guzman
    //           </span>
    //           <h1 className="text-[14px] md:text-[16px] lg:text-[16px] text-[#ACB5BD]">
    //             ASESOR DE SERVICIO
    //           </h1>
    //         </div>
    //         <div className="w-[52px] h-[52px] md:w-[56px] md:h-[56px] lg:w-[62px] lg:h-[62px] rounded-full bg-[#E4E7EB] ml-[5px]"></div>
    //       </div>
    //     </div>
    //   </div>

    //   <div>
    //     <form onSubmit={handleSubmit(onSubmit)}>
    //       <div
    //         className={`grid grid-cols-8 gap-3 bg-[#3682F7] rounded-[10px] shadow-md mt-[36px] p-[15px]`}
    //       >
    //         <div className="col-span-4 md:col-span-4 lg:col-span-1">
    //           <label className="text-[14px] text-[#FFFFFF]">Marca</label>
    //           <input
    //             name="marca"
    //             type="text"
    //             {...register("marca")}
    //             className={`form-control ${
    //               errors.marca ? "is-invalid" : ""
    //             } w-full h-[25px]  bg-[#FFFFFF] border-[#E4E7EB] rounded-[4px] text-[14px] pl-[8px]`}
    //             placeholder="Descripción"
    //             value={marca}
    //             onChange={(e) => {
    //               setMarca(e.target.value);
    //             }}
    //           />
    //           <div className="text-[14px] text-[#FF0000]">
    //             {errors.marca?.message}
    //           </div>
    //         </div>

    //         <div className="col-span-4 md:col-span-4 lg:col-span-1">
    //           <label className="text-[14px] text-[#FFFFFF]">Modelo</label>
    //           <input
    //             name="modelo"
    //             type="text"
    //             {...register("modelo")}
    //             className={`form-control ${
    //               errors.modelo ? "is-invalid" : ""
    //             } w-full h-[25px]  bg-[#FFFFFF] border-[#E4E7EB] rounded-[4px] text-[14px] pl-[8px]`}
    //             placeholder="Descripción"
    //             value={modelo}
    //             onChange={(e) => {
    //               setModelo(e.target.value);
    //             }}
    //           />
    //           <div className="text-[14px] text-[#FF0000]">
    //             {errors.modelo?.message}
    //           </div>
    //         </div>

    //         <div className="col-span-4 md:col-span-4 lg:col-span-1">
    //           <label className="text-[14px] text-[#FFFFFF]">Versión</label>
    //           <input
    //             name="version"
    //             type="text"
    //             {...register("version")}
    //             className={`form-control ${
    //               errors.version ? "is-invalid" : ""
    //             } w-full h-[25px] bg-[#FFFFFF] border-[#E4E7EB] rounded-[4px] text-[14px] pl-[8px]`}
    //             placeholder="Descripción"
    //             value={version}
    //             onChange={(e) => {
    //               setVersion(e.target.value);
    //             }}
    //           />
    //           <div className="text-[14px] text-[#FF0000]">
    //             {errors.version?.message}
    //           </div>
    //         </div>

    //         <div className="col-span-4 md:col-span-4 lg:col-span-1 ">
    //           <label className="text-[14px] text-[#FFFFFF]">Color</label>
    //           <input
    //             name="color"
    //             type="text"
    //             {...register("color")}
    //             className={`form-control ${
    //               errors.color ? "is-invalid" : ""
    //             } w-full h-[25px]  bg-[#FFFFFF] border-[#E4E7EB] rounded-[4px] text-[14px] pl-[8px]`}
    //             placeholder="Descripción"
    //             value={color}
    //             onChange={(e) => {
    //               setColor(e.target.value);
    //             }}
    //           />
    //           <div className="text-[14px] text-[#FF0000]">
    //             {errors.color?.message}
    //           </div>
    //         </div>

    //         <div className="col-span-4 md:col-span-4 lg:col-span-1">
    //           <label className="text-[14px] text-[#FFFFFF]">Placa</label>
    //           <SearchPlate
    //             {...register("placa")}
    //             setCarId={setCarId}
    //             setPlaca={setPlaca}
    //           />

    //           {/* <div className="text-[14px] text-[#FF0000]">{errors.placa?.message}</div> */}
    //         </div>

    //         <div className="col-span-4 md:col-span-4 lg:col-span-1">
    //           <label className="text-[14px] text-[#FFFFFF]">VIN</label>

    //           <SearchVin setVin={setVin} />

    //           <div className="text-[14px] text-[#FF0000]">
    //             {errors.vin?.message}
    //           </div>
    //         </div>

    //         <div className="col-span-4 md:col-span-4 lg:col-span-1">
    //           <label className="text-[14px] text-[#FFFFFF]">Kilometraje</label>
    //           <input
    //             name="kilometraje"
    //             type="text"
    //             {...register("kilometraje")}
    //             className={`form-control ${
    //               errors.kilometraje ? "is-invalid" : ""
    //             } w-full h-[25px]  bg-[#FFFFFF] border-[#E4E7EB] rounded-[4px] text-[14px] pl-[8px]`}
    //             placeholder="Descripción"
    //             value={km}
    //             onChange={(e) => {
    //               setKm(e.target.value);
    //             }}
    //           />
    //           <div className="text-[14px] text-[#FF0000]">
    //             {errors.kilometraje?.message}
    //           </div>
    //         </div>

    //         <div className="col-span-4 md:col-span-4 lg:col-span-1">
    //           <label className="text-[14px] text-[#FFFFFF]">Transmisión</label>
    //           <input
    //             name="transmision"
    //             type="text"
    //             {...register("transmision")}
    //             className={`form-control ${
    //               errors.transmision ? "is-invalid" : ""
    //             } w-full h-[25px]  bg-[#FFFFFF] border-[#E4E7EB] rounded-[4px] text-[14px] pl-[8px]`}
    //             placeholder="Descripción"
    //             value={transmision}
    //             onChange={(e) => {
    //               setTransmision(e.target.value);
    //             }}
    //           />
    //           <div className="text-[14px] text-[#FF0000]">
    //             {errors.transmision?.message}
    //           </div>
    //         </div>
    //       </div>
    //       <div className="mt-[20px] grid grid-cols-12 gap-3">
    //         <div className="col-span-12 md:col-span-12 lg:col-span-6 p-[15px] bg-[#FFFF] rounded-[10px] shadow-md justify-evenly">
    //           <div className="grid grid-cols-12 gap-3">
    //             <div className="col-span-6 md:col-span-4 lg:col-span-4">
    //               <label className="text-[14px] text-[#000000]">Nombre</label>
    //               <div>
    //                 <SearchName setClientId={setClientId} setName={setName} />
    //               </div>

    //               <div className="text-[14px] text-[#FF0000]">
    //                 {errors.nombre?.message}
    //               </div>
    //             </div>

    //             <div className="col-span-6 md:col-span-4 lg:col-span-4">
    //               <label className="text-[14px] text-[#000000]">Teléfono</label>
    //               <input
    //                 name="telefono"
    //                 type="text"
    //                 {...register("telefono")}
    //                 className={`form-control ${
    //                   errors.telefono ? "is-invalid" : ""
    //                 } w-full lg:col-span-4 h-[25px]  bg-[#F6F6FA] border-[#E4E7EB] rounded-[4px] text-[14px] pl-[8px]`}
    //                 placeholder="Descripción"
    //                 value={phone}
    //                 onChange={(e) => {
    //                   setPhone(e.target.value);
    //                 }}
    //               />
    //               <div className="text-[14px] text-[#FF0000]">
    //                 {errors.telefono?.message}
    //               </div>
    //             </div>

    //             <div className="col-span-12 md:col-span-4 lg:col-span-4 ">
    //               <label className="text-[14px] text-[#000000]">E-Mail</label>
    //               <input
    //                 name="email"
    //                 type="text"
    //                 {...register("email")}
    //                 className={`form-control ${
    //                   errors.email ? "is-invalid" : ""
    //                 } w-full h-[25px]  bg-[#F6F6FA] border-[#E4E7EB] rounded-[4px] text-[14px] pl-[8px]`}
    //                 placeholder="Descripción"
    //                 value={email}
    //                 onChange={(e) => {
    //                   setEmail(e.target.value);
    //                 }}
    //               />

    //               <div className="text-[14px] text-[#FF0000]">
    //                 {errors.email?.message}
    //               </div>
    //             </div>
    //           </div>
    //         </div>

    //         <div className="col-span-12 md:col-span-12 lg:col-span-3  p-[15px]  bg-[#FFFF] shadow-md rounded-[10px] justify-evenly">
    //           <div className="grid grid-cols-12 gap-3">
    //             <div className="col-span-6 md:col-span-6 lg:col-span-6">
    //               <label className="text-[14px] text-[#000000]">Nombre</label>
    //               <input
    //                 name="nameTwo"
    //                 type="text"
    //                 {...register("nameTwo")}
    //                 className={`form-control ${
    //                   errors.nombre2 ? "is-invalid" : ""
    //                 } w-full h-[25px]  bg-[#F6F6FA] border-[#E4E7EB] rounded-[4px] text-[14px] pl-[8px]`}
    //                 placeholder="Descripción"
    //                 value={nameTwo}
    //                 onChange={(e) => {
    //                   setNameTwo(e.target.value);
    //                 }}
    //               />
    //               <div className="text-[14px] text-[#FF0000]">
    //                 {errors.nameTwo?.message}
    //               </div>
    //             </div>

    //             <div className="col-span-6 md:col-span-6 lg:col-span-6">
    //               <label className="text-[14px] text-[#000000]">NIT/C.I.</label>
    //               <input
    //                 name="nit"
    //                 type="text"
    //                 {...register("nit")}
    //                 className={`form-control ${
    //                   errors.nit ? "is-invalid" : ""
    //                 } w-full h-[25px]  bg-[#F6F6FA] border-[#E4E7EB] rounded-[4px] text-[14px] pl-[8px]`}
    //                 placeholder="Descripción"
    //                 value={nit}
    //                 onChange={(e) => {
    //                   setNit(e.target.value);
    //                 }}
    //               />
    //               <div className="text-[14px] text-[#FF0000]">
    //                 {errors.nit?.message}
    //               </div>
    //             </div>
    //           </div>
    //         </div>

    //         <div className="col-span-12 md:col-span-12 lg:col-span-3  p-[15px]  bg-[#FFFF] rounded-[10px]  shadow-md justify-evenly">
    //           <div className="grid grid-cols-12 gap-3">
    //             <div className="col-span-6 md:col-span-6 lg:col-span-6">
    //               <label className="text-[14px] text-[#000000]">Nombre</label>
    //               <input
    //                 name="contactName"
    //                 type="text"
    //                 {...register("contactName")}
    //                 className={`form-control ${
    //                   errors.contactName ? "is-invalid" : ""
    //                 } w-full h-[25px]  bg-[#F6F6FA] border-[#E4E7EB] rounded-[4px] text-[14px] pl-[8px]`}
    //                 placeholder="Descripción"
    //                 value={contactName}
    //                 onChange={(e) => {
    //                   setContactName(e.target.value);
    //                 }}
    //               />
    //               <div className="text-[14px] text-[#FF0000]">
    //                 {errors.contactName?.message}
    //               </div>
    //             </div>

    //             <div className="col-span-6 md:col-span-6 lg:col-span-6">
    //               <label className="text-[14px] text-[#000000]">Celular</label>
    //               <input
    //                 name="contactPhone"
    //                 type="text"
    //                 {...register("contactPhone")}
    //                 className={`form-control ${
    //                   errors.contactPhone ? "is-invalid" : ""
    //                 } w-full h-[25px]  bg-[#F6F6FA] border-[#E4E7EB] rounded-[4px] text-[14px] pl-[8px]`}
    //                 placeholder="Descripción"
    //                 value={contactPhone}
    //                 onChange={(e) => {
    //                   setContactPhone(e.target.value);
    //                 }}
    //               />
    //               <div className="text-[14px] text-[#FF0000]">
    //                 {errors.contactPhone?.message}
    //               </div>
    //             </div>
    //           </div>
    //         </div>
    //       </div>

    //       <div className="grid grid-cols-12 gap-6">
    //         <div className="col-span-12 md:col-span-12 lg:col-span-8">
    //           <div className="grid grid-cols-12 gap-3">
    //             <div className="col-span-12 md:col-span-12 lg:col-span-12">
    //               <div>
    //                 <ListObservacionesCli />
    //               </div>
    //             </div>
    //           </div>

    //           <div className='grid grid-cols-12 gap-3"'>
    //             <div className="col-span-12 md:col-span-12 lg:col-span-12">
    //               <div>
    //                 <ListServiceReq />
    //               </div>
    //             </div>
    //           </div>
    //         </div>

    //         <div className="col-span-12 md:col-span-12 lg:col-span-4">
    //           <div className="text-[17px] text-[#000000] mt-[32px]">
    //             Promesa de entrega
    //           </div>

    //           <div className="grid grid-cols-12 gap-6">
    //             {/* <div className="col-span-12 md:col-span-6 lg:col-span-12 mt-[18px]">
    //                 <Calendar
    //                   onChange={onChange}
    //                   value={value}
    //                 />
    //               </div> */}

    //             <div className="col-span-12 md:col-span-6 lg:col-span-12 mt-[18px] p-[15px]  bg-[#FFFF] rounded-[10px]  shadow-md">
    //               <label>Fecha de entrega</label>
    //               <input
    //                 name="date"
    //                 type="date"
    //                 {...register("date")}
    //                 className={`form-control ${
    //                   errors.date ? "is-invalid" : ""
    //                 } w-full h-[35px]  bg-[#F6F6FA] border-[1px] border-[#000000] rounded-[4px] text-[14px] pl-[8px]`}
    //                 value={date}
    //                 onChange={(e) => {
    //                   setDate(e.target.value);
    //                 }}
    //               />
    //               <div className="text-[14px] text-[#FF0000]">
    //                 {errors.date?.message}
    //               </div>

    //               <div className="col-span-12 md:col-span-6 lg:col-span-12 mt-[18px]">
    //                 <label>Hora de entrega</label>
    //                 <input
    //                   name="time"
    //                   type={"time"}
    //                   className="w-full h-[35px]  bg-[#F6F6FA] border-[1px] border-[#000000] rounded-[4px] text-[14px] pl-[8px]"
    //                   {...register("time")}
    //                   value={time}
    //                   onChange={(e) => {
    //                     setTime(e.target.value);
    //                   }}
    //                 />
    //                 <div className="text-[14px] text-[#FF0000]">
    //                   {errors.time?.message}
    //                 </div>
    //               </div>
    //             </div>

    //             <div className="col-span-12 md:col-span-6 lg:col-span-12">
    //               <div className=" text-m mt-[20px]">
    //                 Comentarios adicionales
    //               </div>

    //               <div>
    //                 <input
    //                   name="observations"
    //                   type="text"
    //                   {...register("observations")}
    //                   className="w-full h-[100px] mt-[18px] h-[80px]  border-[1px] border-[#E4E7EB] rounded-[10px] "
    //                   value={observations}
    //                   onChange={(e) => {
    //                     setObservations(e.target.value);
    //                   }}
    //                 />
    //                 <div className="text-[14px] text-[#FF0000]">
    //                   {errors.observations?.message}
    //                 </div>
    //               </div>

    //               <button
    //                 className="w-full  mt-[18px] h-[57px] rounded-[7px] text-[#FFFF] bg-[#3682F7] mt-[21px"
    //                 type="submit"
    //               >
    //                 {" "}
    //                 Guardar
    //               </button>
    //             </div>
    //           </div>
    //         </div>
    //       </div>
    //     </form>
    //   </div>

    //   {/* <div className="flex flex-row mt-[40px] ml-[40px]">
    //     <div style={{ maxHeight: '180px', overflowY: 'auto' }}>
    //       <div >
    //         <ListServices />
    //       </div>
    //     </div>
    //   </div> */}
    // </div>

    // CONTENERDOR PRINCIPAL
    <div
      className="
    w-full h-screen flex flex-col justify-start items-center bg-[#F6F6FA]
    "
    >
      {/*HEADER*/}
      <div
        className="
      w-full h-24 flex flex-row justifify-center items-center px-8
      {/*DESKTOP*/}
      lg:h-20  
      "
      >
        {/*TITLE*/}
        <div
          className="
        w-3/4 h-full flex flex-col justify-center items-start
        {/*DESKTOP*/}
        lg:flex-row lg:justify-start lg:items-center
        "
        >
          <p
            className="
          text-2x1 font-bold font-Dosis text-[#000000]
          {/*DESKTOP*/}
          lg:text-3xl
          "
          >
            Nueva orden
          </p>
          <p
            className="
          text-2x1 font-Dosis hidden
          {/*DESKTOP*/}
          lg:text-3xl lg:flex
          "
          >
            &nbsp;/&nbsp;
          </p>
          <p
            className="
          text-2x1 font-Dosis
          {/*DESKTOP*/}
          lg:text-3xl
          "
          >
            {diaTextualActual} {diaNumeralActual} de {mesTextualActual} del{" "}
            {anioActual}
          </p>
        </div>
        {/*BUTTON*/}
        <div
          className="
        w-1/4 flex flex-row justify-end items-center 
        "
        >
          <button
            type={"button"}
            className={`
            w-28 h-10 border-[#3682F7] border-2 rounded-full text-sm text-center font-Dosis font-medium bg-[#F6F6FA] text-[#3682F7] focus:
             outline-none z-100
            ${isDataFilled ? "bg-[#3682F7] text-[#fff]" : "cursor-not-allowed"}
            ${loading ? "cursor-not-allowed" : ""}
            ${success ? "cursor-not-allowed bg-green-500 border-green-500" : ""}
            ${error ? "cursor-not-allowed bg-red-500 border-red-500" : ""}
            {/*DESKTOP*/}
            lg:w-32 lg:h-12 lg:text-lg
            {/*HOVER*/}
            transform hover:scale-105
            `}
            onClick={sendData}
            disabled={!isDataFilled || loading || success || error}
          >
            {loading ? (
              <Dots color={"#fff"} size={20} className="mb-1" />
            ) : success ? (
              "Guardado"
            ) : error ? (
              "Error"
            ) : (
              "Guardar"
            )}
          </button>
        </div>
      </div>
      {/*BODY*/}
      <div
        className="
      w-full h-full flex flex-col justfiy-start items-center overflow-scroll
      {/*DESKTOP*/}
      lg:justify-between
      "
      >
        {/*VEHICLE AND CLIENT*/}
        <div
          className="
          w-full h-fit flex flex-col justify-start items-center
          {/*DESKTOP*/}
          lg:h-2/4 lg:flex-row lg:justify-center lg:px-4
          "
        >
          {/* DATOS DE VEHÍCULOS */}
          <div
            className="
            w-5/6 h-[700px] flex flex-row justify-center items-center mx-4 mb-4
            {/*DESKTOP*/}
            lg:w-1/2 lg:h-full lg:mb-0 lg:py-4
            "
          >
            <DatosVehiculos setDatosVehiculo={setDatosVehiculo} />
          </div>
          {/* DATOS DE CLIENTES */}
          <div
            className="
            w-5/6 h-[600px] flex flex-row justify-center items-center mx-4 mb-4
            {/*DESKTOP*/}
            lg:w-1/2 lg:h-full lg:mb-0 lg:py-4
            "
          >
            <DatosClientes setDatosCliente={setDatosCliente} />
          </div>
        </div>
        {/*OBSERVATIONS AND SERVICES*/}
        <div
          className="
        w-full h-fit flex flex-col justify-start items-center
        {/*DESKTOP*/}
        lg:h-1/3 lg:flex-row lg:justify-center lg:px-4
        "
        >
          {/*OBSEVATIONS*/}
          <div
            className="
          w-5/6 flex flex-row justify-center items-center mx-4 mb-4
          {/*DESKTOP*/}
            lg:w-1/2 lg:h-full lg:mb-0 lg:py-4
          "
          >
            <Observaciones
              observaciones={observaciones}
              setObservaciones={setObservaciones}
            />
          </div>
          {/*SERVICES*/}
          <div
            className="
          w-5/6 flex flex-row justify-center items-center mx-4 mb-4
          {/*DESKTOP*/}
            lg:w-1/2 lg:h-full lg:mb-0 lg:py-4
          "
          >
            <Servicios servicios={servicios} setServicios={setServicios} />
          </div>
        </div>
        {/*CONTACT, PROMISE, KM AND COMMENTS*/}
        <div
          className="
        w-full h-fit flex flex-col justify-start items-center
        {/*DESKTOP*/}
        lg:h-1/5 lg:flex-row lg:justify-center lg:px-4
        "
        >
          {/* CONTACT */}
          <div
            className="
          w-5/6 flex flex-row justify-center items-center mx-4 mb-4
          {/*DESKTOP*/}
          lg:w-[30%] lg:h-full lg:mb-0 lg:py-4
          "
          >
            <DetallesContacto setDetallesContacto={setDetallesContacto} />
          </div>
          {/* PROMISE */}
          <div
            className="
          w-5/6 flex flex-row justify-center items-center mx-4 mb-4
          {/*DESKTOP*/}
          lg:w-[20%] lg:h-full lg:mb-0 lg:py-4
          "
          >
            <DetallesEntrega
              detallesEntrega={detallesEntrega}
              setDetallesEntrega={setDetallesEntrega}
            />
          </div>
          {/* KM */}
          <div
            className="
          w-5/6 flex flex-row justify-center items-center mx-4 mb-4
          {/*DESKTOP*/}
          lg:w-[20%] lg:h-full lg:mb-0 lg:py-4
          "
          >
            <Kilometraje
              kilometraje={kilometraje}
              setKilometraje={setKilometraje}
            />
          </div>
          {/* COMMENTS */}
          <div
            className="
          w-5/6 flex flex-row justify-center items-center mx-4 mb-4
          {/*DESKTOP*/}
          lg:w-[30%] lg:h-full lg:mb-0 lg:py-4
          "
          >
            <Comentarios
              comentarios={comentarios}
              setComentarios={setComentarios}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
NuevaOrden.auth = true;
NuevaOrden.getLayout = function getLayout(page) {
  return <Layout>{page}</Layout>;
};

export default NuevaOrden;
